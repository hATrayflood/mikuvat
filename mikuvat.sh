#!/bin/sh

JAVA_HOME_MACOSX=/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/Home
JAVA_HOME_FEDORA=/usr/lib/jvm/jre-1.6.0
JAVA_HOME_UBUNTU=/usr/lib/jvm/java-6-openjdk/jre

if [ -z "${JAVA_HOME}" ] ; then
	if [ -e ${JAVA_HOME_MACOSX} ] ; then
		JAVA_HOME=${JAVA_HOME_MACOSX}
	elif [ -e ${JAVA_HOME_FEDORA} ] ; then
		JAVA_HOME=${JAVA_HOME_FEDORA}
	elif [ -e ${JAVA_HOME_UBUNTU} ] ; then
		JAVA_HOME=${JAVA_HOME_UBUNTU}
	else
		JAVA_HOME=`which java`
		JAVA_HOME=`dirname ${JAVA_HOME}`/..
	fi
fi

cd `dirname ${0}`
${JAVA_HOME}/bin/java -jar mikuvat.jar $@
