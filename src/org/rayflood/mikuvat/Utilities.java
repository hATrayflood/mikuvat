package org.rayflood.mikuvat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.rayflood.mikuvat.io.vsq.VibratoBP;

public class Utilities{
	public static final Pattern SPLITEQUAL = Pattern.compile("=");
	public static final Pattern SPLITCOMMA = Pattern.compile(",");
	public static final Pattern SPLITDQUOTE = Pattern.compile("\\\"");

	public static String[] splitEqual(String s){
		return SPLITEQUAL.split(s, 2);
	}

	public static String setSectionBracket(String s){
		return "[" + s + "]";
	}

	public static Integer adjustMaxMin(Integer value, Integer max, Integer min){
		if(value != null){
			if(value > max){
				value = max;
			}
			else if(value < min){
				value = min;
			}
		}
		return value;
	}

	public static BigDecimal adjustMaxMin(BigDecimal value, BigDecimal max, BigDecimal min){
		if(value != null){
			if(value.floatValue() > max.floatValue()){
				value = max;
			}
			else if(value.floatValue() < min.floatValue()){
				value = min;
			}
		}
		return value;
	}

	public static String getStringValue(Boolean value){
		if(value == null || !value){
			return "0";
		}
		else{
			return "1";
		}
	}

	public static boolean getBooleanValue(String value){
		if(value == null || value.isEmpty() || "0".equals(value)){
			return false;
		}
		else{
			return true;
		}
	}

	public static String getBigDecimalString(BigDecimal bd){
		return getBigDecimalString(bd, "%.6f");
	}

	public static String getBigDecimalString(BigDecimal bd, String format){
		return String.format(format, bd);
	}

	public static String getByteArrayString(byte[] data){
		StringBuilder sb = new StringBuilder();
		if(data != null && data.length > 0){
			sb.append(String.format("%X", data[0]));
			for(int i = 1; i < data.length; i++){
				sb.append(" " + String.format("%X", data[i]));
			}
		}
		return sb.toString();
	}

	public static List<VibratoBP> getVibratoBPList(SortedMap<BigDecimal, Integer> bpm){
		List<VibratoBP> bpl = new ArrayList<VibratoBP>();
		BigDecimal xi[] = bpm.keySet().toArray(new BigDecimal[0]);
		for(int i = 0; i < xi.length; i++){
			BigDecimal x = xi[i];
			int y = bpm.get(x);
			bpl.add(new VibratoBP(x, y));
		}
		return bpl;
	}

	public static TreeMap<BigDecimal, Integer> getVibratoBPMap(List<VibratoBP> bpl){
		TreeMap<BigDecimal, Integer> bpm = new TreeMap<BigDecimal, Integer>();
		for(int i = 0; i < bpl.size(); i++){
			VibratoBP bp = bpl.get(i);
			bpm.put(bp.getX(), bp.getY());
		}
		return bpm;
	}
}
