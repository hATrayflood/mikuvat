package org.rayflood.mikuvat.gui;

import javax.swing.JCheckBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class OnOffBox extends JCheckBox{
	private static final long serialVersionUID = -7708456617741170758L;

	public OnOffBox(){
		super();
		this.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(isSelected()){
					setText("On");
				}
				else{
					setText("Off");
				}
			}
		});
		setSelected(true);
	}
}
