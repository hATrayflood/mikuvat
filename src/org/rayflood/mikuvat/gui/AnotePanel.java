package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.AnoteEvent.ACCENT_MAX;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.ACCENT_MIN;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.BENDDEPTH_MAX;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.BENDDEPTH_MIN;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.BENDLENGTH_MAX;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.BENDLENGTH_MIN;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.DECAY_MAX;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.DECAY_MIN;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.NOTE_PARAMLIST;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.PORTAMENTOUSE_PARAMLIST;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.VELOCITY_MAX;
import static org.rayflood.mikuvat.io.vsq.AnoteEvent.VELOCITY_MIN;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.rayflood.mikuvat.io.vsq.AnoteEvent;
import org.rayflood.mikuvat.io.vsq.LyricHandle;
import org.rayflood.mikuvat.io.vsq.LyricNote;
import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class AnotePanel extends PropertyPanel{
	private static final long serialVersionUID = -4240325455386126638L;
	public static final String PANEL_NAME = "音符プロパティ";
	private CheckAndInput lyricPanel;
	private CheckAndInput phoneticPanel;
	private CheckAndInput protectPanel;
	private CheckAndInput velocityPanel;
	private CheckAndInput notePanel;
	private CheckAndInput lengthPanel;
	private CheckAndInput bendDepthPanel;
	private CheckAndInput bendLengthPanel;
	private CheckAndInput portamentoUsePanel;
	private CheckAndInput decayPanel;
	private CheckAndInput accentPanel;
	private CheckAndInput vibratoHandlePanel;
	private SpinnerNumberModel lengthspin;
	private VibratoHandleInput vibratoHandleInput;

	public AnotePanel(){
		this(new VibratoHandleLabel());
	}

	public AnotePanel(VibratoHandleInput vibratoHandleInput){
		lengthspin = new SpinnerNumberModel(15, 1, 65535, 15);
		lengthspin.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				changeNoteLength(getLength());
			}
		});
		lyricPanel = new CheckAndInput("歌詞", new JTextField());
		phoneticPanel = new CheckAndInput("発音記号", new JTextField());
		protectPanel = new CheckAndInput("プロテクト", new OnOffBox());
		velocityPanel = new CheckAndInput("ベロシティ", new JSpinner(new SpinnerNumberModel(0, VELOCITY_MIN, VELOCITY_MAX, 1)));
		notePanel = new CheckAndInput("高さ", new JSpinner(new SpinnerListModel(NOTE_PARAMLIST)));
		lengthPanel = new CheckAndInput("長さ", new JSpinner(lengthspin));
		bendDepthPanel = new CheckAndInput("ベンドの深さ", new JSpinner(new SpinnerNumberModel(0, BENDDEPTH_MIN, BENDDEPTH_MAX, 1)));
		bendLengthPanel = new CheckAndInput("ベンドの長さ", new JSpinner(new SpinnerNumberModel(0, BENDLENGTH_MIN, BENDLENGTH_MAX, 1)));
		portamentoUsePanel = new CheckAndInput("ポルタメント", new JComboBox(PORTAMENTOUSE_PARAMLIST.toArray(new String[0])));
		decayPanel = new CheckAndInput("ディケイ", new JSpinner(new SpinnerNumberModel(0, DECAY_MIN, DECAY_MAX, 1)));
		accentPanel = new CheckAndInput("アクセント", new JSpinner(new SpinnerNumberModel(0, ACCENT_MIN, ACCENT_MAX, 1)));
		vibratoHandlePanel = new CheckAndInput("ビブラート", vibratoHandleInput.getVibratoHandlePanel());
		setVibratoHandleInput(vibratoHandleInput);

		Component components[] = new Component[]{
				lyricPanel.getCheck(), lyricPanel.getInput()
				, phoneticPanel.getCheck(), phoneticPanel.getInput()
				, protectPanel.getCheck(), protectPanel.getInput()
				, velocityPanel.getCheck(), velocityPanel.getInput()
				, notePanel.getCheck(), notePanel.getInput()
				, lengthPanel.getCheck(), lengthPanel.getInput()
				, bendDepthPanel.getCheck(), bendDepthPanel.getInput()
				, bendLengthPanel.getCheck(), bendLengthPanel.getInput()
				, portamentoUsePanel.getCheck(), portamentoUsePanel.getInput()
				, decayPanel.getCheck(), decayPanel.getInput()
				, accentPanel.getCheck(), accentPanel.getInput()
				, vibratoHandlePanel.getCheck(), vibratoHandlePanel.getInput()
		};

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(components, 6), BorderLayout.CENTER);
		setSelected(true);
		changeNoteLength(getLength());
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setLyricSelected(selected);
		setPhoneticSelected(selected);
		setProtectSelected(selected);
		setVelocitySelected(selected);
		setNoteSelected(selected);
		setLengthSelected(selected);
		setBendDepthSelected(selected);
		setBendLengthSelected(selected);
		setPortamentoUseSelected(selected);
		setDecaySelected(selected);
		setAccentSelected(selected);
		setVibratoHandleSelected(selected);
	}

	public void changeNoteLength(int noteLength){
		vibratoHandleInput.changeNoteLength(noteLength);
	}

	public AnoteEvent getAnoteEvent(){
		AnoteEvent anote = new AnoteEvent();
		if(isLengthSelected()){
			anote.setLength(getLength());
		}
		if(isNoteSelected()){
			anote.setNote(getNote());
		}
		if(isVelocitySelected()){
			anote.setVelocity(getVelocity());
		}
		if(isBendDepthSelected()){
			anote.setBendDepth(getBendDepth());
		}
		if(isBendLengthSelected()){
			anote.setBendLength(getBendLength());
		}
		if(isPortamentoUseSelected()){
			anote.setPortamentoUse(getPortamentoUse());
		}
		if(isDecaySelected()){
			anote.setDecay(getDecay());
		}
		if(isAccentSelected()){
			anote.setAccent(getAccent());
		}
		if(isLyricSelected() || isPhoneticSelected() || isProtectSelected()){
			anote.setLyricHandle(getLyricHandle());
		}
		if(isVibratoHandleSelected()){
			anote.setVibratoHandle(getVibratoHandle());
		}
		return anote;
	}

	public void setAnoteEvent(AnoteEvent anote){
		if(anote.hasLength()){
			setLength(anote.getLength());
		}
		else{
			setLengthSelected(false);
		}
		if(anote.hasNote()){
			setNote(anote.getNote());
		}
		else{
			setNoteSelected(false);
		}
		if(anote.hasVelocity()){
			setVelocity(anote.getVelocity());
		}
		else{
			setVelocitySelected(false);
		}
		if(anote.hasBendDepth()){
			setBendDepth(anote.getBendDepth());
		}
		else{
			setBendDepthSelected(false);
		}
		if(anote.hasBendLength()){
			setBendLength(anote.getBendLength());
		}
		else{
			setBendLengthSelected(false);
		}
		if(anote.hasPortamentoUse()){
			setPortamentoUse(anote.getPortamentoUse());
		}
		else{
			setPortamentoUseSelected(false);
		}
		if(anote.hasDecay()){
			setDecay(anote.getDecay());
		}
		else{
			setDecaySelected(false);
		}
		if(anote.hasAccent()){
			setAccent(anote.getAccent());
		}
		else{
			setAccentSelected(false);
		}
		if(anote.hasLyricHandle()){
			setLyricHandle(anote.getLyricHandle());
		}
		else{
			setLyricSelected(false);
			setPhoneticSelected(false);
			setProtectSelected(false);
		}
		if(anote.hasVibratoHandle()){
			setVibratoHandle(anote.getVibratoHandle());
		}
		else{
			setVibratoHandleSelected(false);
		}
	}

	public VibratoHandleInput getVibratoHandleInput(){
		return vibratoHandleInput;
	}

	public void setVibratoHandleInput(VibratoHandleInput vibratoHandleInput){
		this.vibratoHandleInput = vibratoHandleInput;
	}

	public boolean isLyricSelected(){
		return lyricPanel.isSelected();
	}

	public void setLyricSelected(boolean selected){
		lyricPanel.setSelected(selected);
	}

	public String getLyric(){
		JTextField input = (JTextField)lyricPanel.getInput();
		return input.getText();
	}

	public void setLyric(String lyric){
		JTextField input = (JTextField)lyricPanel.getInput();
		input.setText(lyric);
	}

	public boolean isPhoneticSelected(){
		return phoneticPanel.isSelected();
	}

	public void setPhoneticSelected(boolean selected){
		phoneticPanel.setSelected(selected);
	}

	public String getPhonetic(){
		JTextField input = (JTextField)phoneticPanel.getInput();
		return input.getText();
	}

	public void setPhonetic(String phonetic){
		JTextField input = (JTextField)phoneticPanel.getInput();
		input.setText(phonetic);
	}

	public boolean isProtectSelected(){
		return protectPanel.isSelected();
	}

	public void setProtectSelected(boolean selected){
		protectPanel.setSelected(selected);
	}

	public boolean getProtect(){
		JCheckBox input = (JCheckBox)protectPanel.getInput();
		return input.isSelected();
	}

	public void setProtect(boolean protect){
		JCheckBox input = (JCheckBox)protectPanel.getInput();
		input.setSelected(protect);
	}

	public boolean isVelocitySelected(){
		return velocityPanel.isSelected();
	}

	public void setVelocitySelected(boolean selected){
		velocityPanel.setSelected(selected);
	}

	public int getVelocity(){
		JSpinner input = (JSpinner)velocityPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setVelocity(int velocity){
		JSpinner input = (JSpinner)velocityPanel.getInput();
		input.setValue(velocity);
	}

	public boolean isNoteSelected(){
		return notePanel.isSelected();
	}

	public void setNoteSelected(boolean selected){
		notePanel.setSelected(selected);
	}

	public int getNote(){
		JSpinner input = (JSpinner)notePanel.getInput();
		return NOTE_PARAMLIST.indexOf(input.getValue());
	}

	public void setNote(int note){
		JSpinner input = (JSpinner)notePanel.getInput();
		input.setValue(NOTE_PARAMLIST.get(note));
	}

	public boolean isLengthSelected(){
		return lengthPanel.isSelected();
	}

	public void setLengthSelected(boolean selected){
		lengthPanel.setSelected(selected);
	}

	public int getLength(){
		JSpinner input = (JSpinner)lengthPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setLength(int length){
		JSpinner input = (JSpinner)lengthPanel.getInput();
		input.setValue(length);
	}

	public boolean isBendDepthSelected(){
		return bendDepthPanel.isSelected();
	}

	public void setBendDepthSelected(boolean selected){
		bendDepthPanel.setSelected(selected);
	}

	public int getBendDepth(){
		JSpinner input = (JSpinner)bendDepthPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setBendDepth(int bendDepth){
		JSpinner input = (JSpinner)bendDepthPanel.getInput();
		input.setValue(bendDepth);
	}

	public boolean isBendLengthSelected(){
		return bendLengthPanel.isSelected();
	}

	public void setBendLengthSelected(boolean selected){
		bendLengthPanel.setSelected(selected);
	}

	public int getBendLength(){
		JSpinner input = (JSpinner)bendLengthPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setBendLength(int bendLength){
		JSpinner input = (JSpinner)bendLengthPanel.getInput();
		input.setValue(bendLength);
	}

	public boolean isPortamentoUseSelected(){
		return portamentoUsePanel.isSelected();
	}

	public void setPortamentoUseSelected(boolean selected){
		portamentoUsePanel.setSelected(selected);
	}

	public int getPortamentoUse(){
		JComboBox input = (JComboBox)portamentoUsePanel.getInput();
		return input.getSelectedIndex();
	}

	public void setPortamentoUse(int portamentoUse){
		JComboBox input = (JComboBox)portamentoUsePanel.getInput();
		input.setSelectedIndex(portamentoUse);
	}

	public boolean isDecaySelected(){
		return decayPanel.isSelected();
	}

	public void setDecaySelected(boolean selected){
		decayPanel.setSelected(selected);
	}

	public int getDecay(){
		JSpinner input = (JSpinner)decayPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setDecay(int decay){
		JSpinner input = (JSpinner)decayPanel.getInput();
		input.setValue(decay);
	}

	public boolean isAccentSelected(){
		return accentPanel.isSelected();
	}

	public void setAccentSelected(boolean selected){
		accentPanel.setSelected(selected);
	}

	public int getAccent(){
		JSpinner input = (JSpinner)accentPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setAccent(int accent){
		JSpinner input = (JSpinner)accentPanel.getInput();
		input.setValue(accent);
	}

	public LyricHandle getLyricHandle(){
		LyricNote lyricNote = new LyricNote();
		if(isLyricSelected()){
			lyricNote.setLyric(getLyric());
		}
		if(isPhoneticSelected()){
			lyricNote.setPhonetic(getPhonetic());
		}
		if(isProtectSelected()){
			lyricNote.setProtect(getProtect());
		}
		return new LyricHandle(lyricNote);
	}

	public void setLyricHandle(LyricHandle lyricHandle){
		LyricNote lyricNote = lyricHandle.getLyricNote();
		if(lyricNote.hasCaption()){
			setLyric(lyricNote.getCaption());
		}
		else{
			setLyricSelected(false);
		}
		if(lyricNote.hasPhonetic()){
			setPhonetic(lyricNote.getPhonetic());
		}
		else{
			setPhoneticSelected(false);
		}
		if(lyricNote.hasProtect()){
			setProtect(lyricNote.isProtect());
		}
		else{
			setProtectSelected(false);
		}
	}

	public boolean isVibratoHandleSelected(){
		return vibratoHandlePanel.isSelected();
	}

	public void setVibratoHandleSelected(boolean selected){
		vibratoHandlePanel.setSelected(selected);
	}

	public VibratoHandle getVibratoHandle(){
		return vibratoHandleInput.getVibratoHandle();
	}

	public void setVibratoHandle(VibratoHandle vibratoHandle){
		vibratoHandleInput.setVibratoHandle(getLength(), vibratoHandle);
	}
}
