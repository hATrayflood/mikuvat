package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.BPLinePanel.PANEL_NAME;

import java.awt.Window;

import org.rayflood.mikuvat.io.vsq.EventBPTime;

public class BPLineDialog extends PropertyDialog{
	private static final long serialVersionUID = 8348156968037169299L;
	private BPLinePanel bpLinePanel;

	public BPLineDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public BPLineDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		bpLinePanel = new BPLinePanel();
		setComponent(bpLinePanel);
		pack();
	}

	public void setSelected(boolean selected){
		bpLinePanel.setSelected(selected);
	}

	public BPLinePanel getBPLinePanel(){
		return bpLinePanel;
	}

	public void setBPLinePanel(BPLinePanel bpLinePanel){
		this.bpLinePanel = bpLinePanel;
	}

	public EventBPTime getEventBPTime(){
		return bpLinePanel.getEventBPTime();
	}

	public void setEventBPTime(EventBPTime bp){
		bpLinePanel.setEventBPTime(bp);
	}

	public boolean isDynamicsSelected(){
		return bpLinePanel.isDynamicsSelected();
	}

	public void setDynamicsSelected(boolean selected){
		bpLinePanel.setDynamicsSelected(selected);
	}

	public int getDynamics(){
		return bpLinePanel.getDynamics();
	}

	public void setDynamics(int dynamics){
		bpLinePanel.setDynamics(dynamics);
	}

	public boolean isBreathinessSelected(){
		return bpLinePanel.isBreathinessSelected();
	}

	public void setBreathinessSelected(boolean selected){
		bpLinePanel.setBreathinessSelected(selected);
	}

	public int getBreathiness(){
		return bpLinePanel.getBreathiness();
	}

	public void setBreathiness(int breathiness){
		bpLinePanel.setBreathiness(breathiness);
	}

	public boolean isBrightnessSelected(){
		return bpLinePanel.isBrightnessSelected();
	}

	public void setBrightnessSelected(boolean selected){
		bpLinePanel.setBrightnessSelected(selected);
	}

	public int getBrightness(){
		return bpLinePanel.getBrightness();
	}

	public void setBrightness(int brightness){
		bpLinePanel.setBrightness(brightness);
	}

	public boolean isClearnessSelected(){
		return bpLinePanel.isClearnessSelected();
	}

	public void setClearnessSelected(boolean selected){
		bpLinePanel.setClearnessSelected(selected);
	}

	public int getClearness(){
		return bpLinePanel.getClearness();
	}

	public void setClearness(int clearness){
		bpLinePanel.setClearness(clearness);
	}

	public boolean isOpeningSelected(){
		return bpLinePanel.isOpeningSelected();
	}

	public void setOpeningSelected(boolean selected){
		bpLinePanel.setOpeningSelected(selected);
	}

	public int getOpening(){
		return bpLinePanel.getOpening();
	}

	public void setOpening(int opening){
		bpLinePanel.setOpening(opening);
	}

	public boolean isGenderFactorSelected(){
		return bpLinePanel.isGenderFactorSelected();
	}

	public void setGenderFactorSelected(boolean selected){
		bpLinePanel.setGenderFactorSelected(selected);
	}

	public int getGenderFactor(){
		return bpLinePanel.getGenderFactor();
	}

	public void setGenderFactor(int genderFactor){
		bpLinePanel.setGenderFactor(genderFactor);
	}

	public boolean isPortamentoTimingSelected(){
		return bpLinePanel.isPortamentoTimingSelected();
	}

	public void setPortamentoTimingSelected(boolean selected){
		bpLinePanel.setPortamentoTimingSelected(selected);
	}

	public int getPortamentoTiming(){
		return bpLinePanel.getPortamentoTiming();
	}

	public void setPortamentoTiming(int portamentoTiming){
		bpLinePanel.setPortamentoTiming(portamentoTiming);
	}

	public boolean isPitchBendSelected(){
		return bpLinePanel.isPitchBendSelected();
	}

	public void setPitchBendSelected(boolean selected){
		bpLinePanel.setPitchBendSelected(selected);
	}

	public int getPitchBend(){
		return bpLinePanel.getPitchBend();
	}

	public void setPitchBend(int pitchBend){
		bpLinePanel.setPitchBend(pitchBend);
	}

	public boolean isPitchBendSensSelected(){
		return bpLinePanel.isPitchBendSensSelected();
	}

	public void setPitchBendSensSelected(boolean selected){
		bpLinePanel.setPitchBendSensSelected(selected);
	}

	public int getPitchBendSens(){
		return bpLinePanel.getPitchBendSens();
	}

	public void setPitchBendSens(int pitchBendSens){
		bpLinePanel.setPitchBendSens(pitchBendSens);
	}
}
