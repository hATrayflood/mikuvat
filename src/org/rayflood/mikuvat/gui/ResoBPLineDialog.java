package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.ResoBPLinePanel.PANEL_NAME;

import java.awt.Window;

import org.rayflood.mikuvat.io.vsq.EventBPTime;

public class ResoBPLineDialog extends PropertyDialog{
	private static final long serialVersionUID = -1422814968641806160L;
	private ResoBPLinePanel resoBPLinePanel;

	public ResoBPLineDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public ResoBPLineDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		resoBPLinePanel = new ResoBPLinePanel();
		setComponent(resoBPLinePanel);
		pack();
	}

	public void setSelected(boolean selected){
		resoBPLinePanel.setSelected(selected);
	}

	public ResoBPLinePanel getResoBPLinePanel(){
		return resoBPLinePanel;
	}

	public void setResoBPLinePanel(ResoBPLinePanel resoBPLinePanel){
		this.resoBPLinePanel = resoBPLinePanel;
	}

	public EventBPTime getEventBPTime(){
		return resoBPLinePanel.getEventBPTime();
	}

	public void setEventBPTime(EventBPTime bp){
		resoBPLinePanel.setEventBPTime(bp);
	}

	public boolean isReso1FreqSelected(){
		return resoBPLinePanel.isReso1FreqSelected();
	}

	public void setReso1FreqSelected(boolean selected){
		resoBPLinePanel.setReso1FreqSelected(selected);
	}

	public int getReso1Freq(){
		return resoBPLinePanel.getReso1Freq();
	}

	public void setReso1Freq(int reso1Freq){
		resoBPLinePanel.setReso1Freq(reso1Freq);
	}

	public boolean isReso2FreqSelected(){
		return resoBPLinePanel.isReso2FreqSelected();
	}

	public void setReso2FreqSelected(boolean selected){
		resoBPLinePanel.setReso2FreqSelected(selected);
	}

	public int getReso2Freq(){
		return resoBPLinePanel.getReso2Freq();
	}

	public void setReso2Freq(int reso2Freq){
		resoBPLinePanel.setReso2Freq(reso2Freq);
	}

	public boolean isReso3FreqSelected(){
		return resoBPLinePanel.isReso3FreqSelected();
	}

	public void setReso3FreqSelected(boolean selected){
		resoBPLinePanel.setReso3FreqSelected(selected);
	}

	public int getReso3Freq(){
		return resoBPLinePanel.getReso3Freq();
	}

	public void setReso3Freq(int reso3Freq){
		resoBPLinePanel.setReso3Freq(reso3Freq);
	}

	public boolean isReso4FreqSelected(){
		return resoBPLinePanel.isReso4FreqSelected();
	}

	public void setReso4FreqSelected(boolean selected){
		resoBPLinePanel.setReso4FreqSelected(selected);
	}

	public int getReso4Freq(){
		return resoBPLinePanel.getReso4Freq();
	}

	public void setReso4Freq(int reso4Freq){
		resoBPLinePanel.setReso4Freq(reso4Freq);
	}

	public boolean isReso1BWSelected(){
		return resoBPLinePanel.isReso1BWSelected();
	}

	public void setReso1BWSelected(boolean selected){
		resoBPLinePanel.setReso1BWSelected(selected);
	}

	public int getReso1BW(){
		return resoBPLinePanel.getReso1BW();
	}

	public void setReso1BW(int reso1BW){
		resoBPLinePanel.setReso1BW(reso1BW);
	}

	public boolean isReso2BWSelected(){
		return resoBPLinePanel.isReso2BWSelected();
	}

	public void setReso2BWSelected(boolean selected){
		resoBPLinePanel.setReso2BWSelected(selected);
	}

	public int getReso2BW(){
		return resoBPLinePanel.getReso2BW();
	}

	public void setReso2BW(int reso2BW){
		resoBPLinePanel.setReso2BW(reso2BW);
	}

	public boolean isReso3BWSelected(){
		return resoBPLinePanel.isReso3BWSelected();
	}

	public void setReso3BWSelected(boolean selected){
		resoBPLinePanel.setReso3BWSelected(selected);
	}

	public int getReso3BW(){
		return resoBPLinePanel.getReso3BW();
	}

	public void setReso3BW(int reso3BW){
		resoBPLinePanel.setReso3BW(reso3BW);
	}

	public boolean isReso4BWSelected(){
		return resoBPLinePanel.isReso4BWSelected();
	}

	public void setReso4BWSelected(boolean selected){
		resoBPLinePanel.setReso4BWSelected(selected);
	}

	public int getReso4BW(){
		return resoBPLinePanel.getReso4BW();
	}

	public void setReso4BW(int reso4BW){
		resoBPLinePanel.setReso4BW(reso4BW);
	}

	public boolean isReso1AmpSelected(){
		return resoBPLinePanel.isReso1AmpSelected();
	}

	public void setReso1AmpSelected(boolean selected){
		resoBPLinePanel.setReso1AmpSelected(selected);
	}

	public int getReso1Amp(){
		return resoBPLinePanel.getReso1Amp();
	}

	public void setReso1Amp(int reso1Amp){
		resoBPLinePanel.setReso1Amp(reso1Amp);
	}

	public boolean isReso2AmpSelected(){
		return resoBPLinePanel.isReso2AmpSelected();
	}

	public void setReso2AmpSelected(boolean selected){
		resoBPLinePanel.setReso2AmpSelected(selected);
	}

	public int getReso2Amp(){
		return resoBPLinePanel.getReso2Amp();
	}

	public void setReso2Amp(int reso2Amp){
		resoBPLinePanel.setReso2Amp(reso2Amp);
	}

	public boolean isReso3AmpSelected(){
		return resoBPLinePanel.isReso3AmpSelected();
	}

	public void setReso3AmpSelected(boolean selected){
		resoBPLinePanel.setReso3AmpSelected(selected);
	}

	public int getReso3Amp(){
		return resoBPLinePanel.getReso3Amp();
	}

	public void setReso3Amp(int reso3Amp){
		resoBPLinePanel.setReso3Amp(reso3Amp);
	}

	public boolean isReso4AmpSelected(){
		return resoBPLinePanel.isReso4AmpSelected();
	}

	public void setReso4AmpSelected(boolean selected){
		resoBPLinePanel.setReso4AmpSelected(selected);
	}

	public int getReso4Amp(){
		return resoBPLinePanel.getReso4Amp();
	}

	public void setReso4Amp(int reso4Amp){
		resoBPLinePanel.setReso4Amp(reso4Amp);
	}
}
