package org.rayflood.mikuvat.gui;

import java.awt.Component;

import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public interface VibratoHandleInput{
	public Component getVibratoHandlePanel();

	public VibratoHandle getVibratoHandle();

	public void setVibratoHandle(int noteLength, VibratoHandle vibratoHandle);

	public void changeNoteLength(int noteLength);
}
