package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.Mikuvat.ICON;
import static org.rayflood.mikuvat.Mikuvat.MIKUVAT_CLASSLOADER;
import static org.rayflood.mikuvat.Mikuvat.MIKUVAT_TITLE;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import org.rayflood.mikuvat.MikuvatProperties;
import org.rayflood.mikuvat.io.vsq.VSQFile;
import org.rayflood.mikuvat.plugin.PluginInstance;
import org.rayflood.mikuvat.plugin.PluginInterface;

public class MikuvatFrame extends JFrame{
	private static final long serialVersionUID = -2300970621405006357L;
	private JTextField infile;
	private JList pluginlist;
	private String pluginSet[];
	private Map<String, PluginInstance> pluginInstance;
	private MikuvatProperties properties;
	private VSQFile vsq;

	public MikuvatFrame(Map<String, PluginInstance> pluginInstance, MikuvatProperties properties){
		super(MIKUVAT_TITLE);
		setPluginInstance(pluginInstance);
		String pluginSet[] = pluginInstance.keySet().toArray(new String[0]);
		setPluginSet(pluginSet);
		setProperties(properties);
		try{
			UIManager.getInstalledLookAndFeels();
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			setLocationByPlatform(true);
			setResizable(false);
			ImageIcon bgimg = new ImageIcon(MIKUVAT_CLASSLOADER.getResource("org/rayflood/mikuvat/gui/mikuvat.png"));
			setIconImage(ICON.getImage());
			setSize(bgimg.getIconWidth(), bgimg.getIconHeight());
			JLabel mainpanel = new JLabel(bgimg);
			mainpanel.setLayout(new BorderLayout());
			Color back = mainpanel.getBackground();
			Color half = new Color(back.getRed(), back.getGreen(), back.getBlue(), 170);

			JPanel inpanel = new JPanel();
			inpanel.setBackground(half);
			inpanel.setLayout(new FlowLayout());
			JLabel inlabel = new JLabel("VSQファイル");
			infile = new JTextField(16);
			JButton inbutton = new JButton("参照...");
			inbutton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					select();
				}
			});
			inpanel.add(inlabel);
			inpanel.add(infile);
			inpanel.add(inbutton);

			JPanel listpanel = new JPanel();
			listpanel.setBackground(half);
			listpanel.setLayout(new BorderLayout());
			pluginlist = new JList();
			pluginlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			pluginlist.setBackground(new Color(0, 0, 0, 0));
			pluginlist.setOpaque(false);
			pluginlist.setBorder(new LineBorder(back));
			List<String> list = new ArrayList<String>();
			for(int i = 0; i < pluginSet.length; i++){
				PluginInstance plugin = pluginInstance.get(pluginSet[i]);
				list.add(plugin.getPluginName() + " - " + plugin.getPluginDescription());
			}
			pluginlist.setListData(list.toArray(new String[0]));
			pluginlist.setSelectedIndex(0);
			listpanel.add(pluginlist, BorderLayout.CENTER);
			listpanel.add(new JLabel("使用可能プラグイン"), BorderLayout.NORTH);

			JPanel outpanel = new JPanel();
			outpanel.setBackground(half);
			outpanel.setLayout(new FlowLayout());
			JButton gobutton = new JButton("プラグイン起動");
			JButton savebutton = new JButton("上書き保存");
			JButton saveasbutton = new JButton("別名で保存");
			gobutton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					pluginGo();
				}
			});
			savebutton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					save();
				}
			});
			saveasbutton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					saveas();
				}
			});
			outpanel.add(gobutton);
			outpanel.add(savebutton);
			outpanel.add(saveasbutton);

			JPanel westpanel = new JPanel();
			westpanel.setBackground(half);
			JPanel eastpanel = new JPanel();
			eastpanel.setBackground(half);
			mainpanel.add(inpanel, BorderLayout.NORTH);
			mainpanel.add(westpanel, BorderLayout.WEST);
			mainpanel.add(eastpanel, BorderLayout.EAST);
			mainpanel.add(listpanel, BorderLayout.CENTER);
			mainpanel.add(outpanel, BorderLayout.SOUTH);

			add(mainpanel);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			addWindowListener(new WindowListener(){
				public void windowActivated(WindowEvent e){
				}
				public void windowClosed(WindowEvent e){
					onClosed();
				}
				public void windowClosing(WindowEvent e){
				}
				public void windowDeactivated(WindowEvent e){
				}
				public void windowDeiconified(WindowEvent e){
				}
				public void windowIconified(WindowEvent e){
				}
				public void windowOpened(WindowEvent e){
				}
			});
			setVisible(true);
		}
		catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ミクバット起動失敗！", "ミクバット", JOptionPane.ERROR_MESSAGE);
		}
	}

	protected void select(){
		File file = null;
		try{
			file = new File(properties.getOpenProperty());
		}
		catch(Exception e){
			// nop
		}
		try{
			JFileChooser opendialog = new JFileChooser();
			opendialog.setFileFilter(new VSQFileFilter());
			opendialog.setSelectedFile(file);
			if(JFileChooser.APPROVE_OPTION == opendialog.showOpenDialog(this)){
				infile.setText(opendialog.getSelectedFile().getPath());
				open();
			}
		}
		catch(Exception e1){
			e1.printStackTrace();
			errorDialog("読み込み失敗！");
		}
	}

	protected void open() throws Exception{
		try{
			if(vsq == null){
				vsq = new VSQFile(infile.getText());
			}
			else if(!infile.getText().equals(vsq.getVsqFile().getPath())){
				if(JOptionPane.OK_OPTION == confirmDialog("現在のVSQを破棄しますか？")){
					vsq = new VSQFile(infile.getText());
				}
				else{
					infoDialog("元のVSQを対象にプラグインを起動します。");
					infile.setText(vsq.getVsqFile().getPath());
				}
			}
			properties.setOpenProperty(infile.getText());
		}
		catch(Exception e1){
			e1.printStackTrace();
			errorDialog("読み込み失敗！");
			throw e1;
		}
	}

	protected void pluginGo(){
		try{
			open();
			try{
				String pluginID = pluginSet[pluginlist.getSelectedIndex()];
				PluginInterface plugin = pluginInstance.get(pluginID).getPlugin();
				plugin.start(this, properties.getProperties());
			}
			catch(Exception e1){
				e1.printStackTrace();
				errorDialog("プラグイン起動失敗！");
			}
		}
		catch(Exception e1){
			// 読み込み失敗！
		}
	}

	protected void save(){
		try{
			vsq.writeVSQFile();
			String path = vsq.getVsqFile().getPath();
			properties.setSaveProperty(path);
			infoDialog("以下のファイルに上書き保存しました。\n" + path);
		}
		catch(Exception e1){
			e1.printStackTrace();
			errorDialog("保存失敗！");
		}
	}

	protected void saveas(){
		File file = null;
		try{
			file = new File(properties.getSaveProperty());
		}
		catch(Exception e){
			// nop
		}
		try{
			JFileChooser savedialog = new JFileChooser(infile.getText());
			savedialog.setFileFilter(new VSQFileFilter());
			savedialog.setSelectedFile(file);
			if(JFileChooser.APPROVE_OPTION == savedialog.showSaveDialog(this)){
				file = savedialog.getSelectedFile();
				vsq.writeVSQFile(file);
				properties.setSaveProperty(file.getPath());
			}
		}
		catch(Exception e1){
			e1.printStackTrace();
			errorDialog("保存失敗！");
		}
	}

	protected void onClosed(){
		properties.storeToXML();
	}

	public int confirmDialog(String message){
		return JOptionPane.showConfirmDialog(this, message, getTitle(), JOptionPane.OK_CANCEL_OPTION);
	}

	public void infoDialog(String message){
		JOptionPane.showMessageDialog(this, message, getTitle(), JOptionPane.INFORMATION_MESSAGE);
	}

	public void errorDialog(String message){
		JOptionPane.showMessageDialog(this, message, getTitle(), JOptionPane.ERROR_MESSAGE);
	}

	public Map<String, PluginInstance> getPluginInstance(){
		return pluginInstance;
	}

	public void setPluginInstance(Map<String, PluginInstance> pluginInstance){
		this.pluginInstance = pluginInstance;
	}

	public MikuvatProperties getProperties(){
		return properties;
	}

	public void setProperties(MikuvatProperties properties){
		this.properties = properties;
	}

	public VSQFile getVsq(){
		return vsq;
	}

	public void setVsq(VSQFile vsq){
		this.vsq = vsq;
	}

	public String[] getPluginSet(){
		return pluginSet;
	}

	public void setPluginSet(String[] pluginSet){
		this.pluginSet = pluginSet;
	}
}
