package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1AMP_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1AMP_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1BW_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1BW_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1FREQ_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO1FREQ_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2AMP_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2AMP_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2BW_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2BW_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2FREQ_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO2FREQ_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3AMP_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3AMP_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3BW_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3BW_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3FREQ_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO3FREQ_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4AMP_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4AMP_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4BW_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4BW_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4FREQ_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.RESO4FREQ_MIN;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.rayflood.mikuvat.io.vsq.EventBPTime;

public class ResoBPLinePanel extends PropertyPanel{
	private static final long serialVersionUID = 1760855856059546490L;
	public static final String PANEL_NAME = "レゾナンスパラメータ";
	private CheckAndInput r1f;
	private CheckAndInput r2f;
	private CheckAndInput r3f;
	private CheckAndInput r4f;
	private CheckAndInput r1b;
	private CheckAndInput r2b;
	private CheckAndInput r3b;
	private CheckAndInput r4b;
	private CheckAndInput r1a;
	private CheckAndInput r2a;
	private CheckAndInput r3a;
	private CheckAndInput r4a;

	public ResoBPLinePanel(){
		r1f = new CheckAndInput("周波数1", new JSpinner(new SpinnerNumberModel(0, RESO1FREQ_MIN, RESO1FREQ_MAX, 1)));
		r2f = new CheckAndInput("周波数2", new JSpinner(new SpinnerNumberModel(0, RESO2FREQ_MIN, RESO2FREQ_MAX, 1)));
		r3f = new CheckAndInput("周波数3", new JSpinner(new SpinnerNumberModel(0, RESO3FREQ_MIN, RESO3FREQ_MAX, 1)));
		r4f = new CheckAndInput("周波数4", new JSpinner(new SpinnerNumberModel(0, RESO4FREQ_MIN, RESO4FREQ_MAX, 1)));
		r1b = new CheckAndInput("帯域幅1", new JSpinner(new SpinnerNumberModel(0, RESO1BW_MIN, RESO1BW_MAX, 1)));
		r2b = new CheckAndInput("帯域幅2", new JSpinner(new SpinnerNumberModel(0, RESO2BW_MIN, RESO2BW_MAX, 1)));
		r3b = new CheckAndInput("帯域幅3", new JSpinner(new SpinnerNumberModel(0, RESO3BW_MIN, RESO3BW_MAX, 1)));
		r4b = new CheckAndInput("帯域幅4", new JSpinner(new SpinnerNumberModel(0, RESO4BW_MIN, RESO4BW_MAX, 1)));
		r1a = new CheckAndInput("振幅1", new JSpinner(new SpinnerNumberModel(0, RESO1AMP_MIN, RESO1AMP_MAX, 1)));
		r2a = new CheckAndInput("振幅2", new JSpinner(new SpinnerNumberModel(0, RESO2AMP_MIN, RESO2AMP_MAX, 1)));
		r3a = new CheckAndInput("振幅3", new JSpinner(new SpinnerNumberModel(0, RESO3AMP_MIN, RESO3AMP_MAX, 1)));
		r4a = new CheckAndInput("振幅4", new JSpinner(new SpinnerNumberModel(0, RESO4AMP_MIN, RESO4AMP_MAX, 1)));

		Component components[] = new Component[]{
				r1f.getCheck(), r1f.getInput()
				, r1b.getCheck(), r1b.getInput()
				, r1a.getCheck(), r1a.getInput()
				, r2f.getCheck(), r2f.getInput()
				, r2b.getCheck(), r2b.getInput()
				, r2a.getCheck(), r2a.getInput()
				, r3f.getCheck(), r3f.getInput()
				, r3b.getCheck(), r3b.getInput()
				, r3a.getCheck(), r3a.getInput()
				, r4f.getCheck(), r4f.getInput()
				, r4b.getCheck(), r4b.getInput()
				, r4a.getCheck(), r4a.getInput()
		};

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(components, 6), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setReso1FreqSelected(selected);
		setReso2FreqSelected(selected);
		setReso3FreqSelected(selected);
		setReso4FreqSelected(selected);
		setReso1BWSelected(selected);
		setReso2BWSelected(selected);
		setReso3BWSelected(selected);
		setReso4BWSelected(selected);
		setReso1AmpSelected(selected);
		setReso2AmpSelected(selected);
		setReso3AmpSelected(selected);
		setReso4AmpSelected(selected);
	}

	public EventBPTime getEventBPTime(){
		EventBPTime bp = new EventBPTime();
		if(isReso1FreqSelected()){
			bp.setReso1Freq(getReso1Freq());
		}
		if(isReso2FreqSelected()){
			bp.setReso2Freq(getReso2Freq());
		}
		if(isReso3FreqSelected()){
			bp.setReso3Freq(getReso3Freq());
		}
		if(isReso4FreqSelected()){
			bp.setReso4Freq(getReso4Freq());
		}
		if(isReso1BWSelected()){
			bp.setReso1BW(getReso1BW());
		}
		if(isReso2BWSelected()){
			bp.setReso2BW(getReso2BW());
		}
		if(isReso3BWSelected()){
			bp.setReso3BW(getReso3BW());
		}
		if(isReso4BWSelected()){
			bp.setReso4BW(getReso4BW());
		}
		if(isReso1AmpSelected()){
			bp.setReso1Amp(getReso1Amp());
		}
		if(isReso2AmpSelected()){
			bp.setReso2Amp(getReso2Amp());
		}
		if(isReso3AmpSelected()){
			bp.setReso3Amp(getReso3Amp());
		}
		if(isReso4AmpSelected()){
			bp.setReso4Amp(getReso4Amp());
		}
		return bp;
	}

	public void setEventBPTime(EventBPTime bp){
		if(bp.hasReso1Freq()){
			setReso1Freq(bp.getReso1Freq());
		}
		else{
			setReso1FreqSelected(false);
		}
		if(bp.hasReso2Freq()){
			setReso2Freq(bp.getReso2Freq());
		}
		else{
			setReso2FreqSelected(false);
		}
		if(bp.hasReso3Freq()){
			setReso3Freq(bp.getReso3Freq());
		}
		else{
			setReso3FreqSelected(false);
		}
		if(bp.hasReso4Freq()){
			setReso4Freq(bp.getReso4Freq());
		}
		else{
			setReso4FreqSelected(false);
		}
		if(bp.hasReso1BW()){
			setReso1BW(bp.getReso1BW());
		}
		else{
			setReso1BWSelected(false);
		}
		if(bp.hasReso2BW()){
			setReso2BW(bp.getReso2BW());
		}
		else{
			setReso2BWSelected(false);
		}
		if(bp.hasReso3BW()){
			setReso3BW(bp.getReso3BW());
		}
		else{
			setReso3BWSelected(false);
		}
		if(bp.hasReso4BW()){
			setReso4BW(bp.getReso4BW());
		}
		else{
			setReso4BWSelected(false);
		}
		if(bp.hasReso1Amp()){
			setReso1Amp(bp.getReso1Amp());
		}
		else{
			setReso1AmpSelected(false);
		}
		if(bp.hasReso2Amp()){
			setReso2Amp(bp.getReso2Amp());
		}
		else{
			setReso2AmpSelected(false);
		}
		if(bp.hasReso3Amp()){
			setReso3Amp(bp.getReso3Amp());
		}
		else{
			setReso3AmpSelected(false);
		}
		if(bp.hasReso4Amp()){
			setReso4Amp(bp.getReso4Amp());
		}
		else{
			setReso4AmpSelected(false);
		}
	}

	public boolean isReso1FreqSelected(){
		return r1f.isSelected();
	}

	public void setReso1FreqSelected(boolean selected){
		r1f.setSelected(selected);
	}

	public int getReso1Freq(){
		JSpinner input = (JSpinner)r1f.getInput();
		return (Integer)input.getValue();
	}

	public void setReso1Freq(int reso1Freq){
		JSpinner input = (JSpinner)r1f.getInput();
		input.setValue(reso1Freq);
	}

	public boolean isReso2FreqSelected(){
		return r2f.isSelected();
	}

	public void setReso2FreqSelected(boolean selected){
		r2f.setSelected(selected);
	}

	public int getReso2Freq(){
		JSpinner input = (JSpinner)r2f.getInput();
		return (Integer)input.getValue();
	}

	public void setReso2Freq(int reso2Freq){
		JSpinner input = (JSpinner)r2f.getInput();
		input.setValue(reso2Freq);
	}

	public boolean isReso3FreqSelected(){
		return r3f.isSelected();
	}

	public void setReso3FreqSelected(boolean selected){
		r3f.setSelected(selected);
	}

	public int getReso3Freq(){
		JSpinner input = (JSpinner)r3f.getInput();
		return (Integer)input.getValue();
	}

	public void setReso3Freq(int reso3Freq){
		JSpinner input = (JSpinner)r3f.getInput();
		input.setValue(reso3Freq);
	}

	public boolean isReso4FreqSelected(){
		return r4f.isSelected();
	}

	public void setReso4FreqSelected(boolean selected){
		r4f.setSelected(selected);
	}

	public int getReso4Freq(){
		JSpinner input = (JSpinner)r4f.getInput();
		return (Integer)input.getValue();
	}

	public void setReso4Freq(int reso4Freq){
		JSpinner input = (JSpinner)r4f.getInput();
		input.setValue(reso4Freq);
	}

	public boolean isReso1BWSelected(){
		return r1b.isSelected();
	}

	public void setReso1BWSelected(boolean selected){
		r1b.setSelected(selected);
	}

	public int getReso1BW(){
		JSpinner input = (JSpinner)r1b.getInput();
		return (Integer)input.getValue();
	}

	public void setReso1BW(int reso1BW){
		JSpinner input = (JSpinner)r1b.getInput();
		input.setValue(reso1BW);
	}

	public boolean isReso2BWSelected(){
		return r2b.isSelected();
	}

	public void setReso2BWSelected(boolean selected){
		r2b.setSelected(selected);
	}

	public int getReso2BW(){
		JSpinner input = (JSpinner)r2b.getInput();
		return (Integer)input.getValue();
	}

	public void setReso2BW(int reso2BW){
		JSpinner input = (JSpinner)r2b.getInput();
		input.setValue(reso2BW);
	}

	public boolean isReso3BWSelected(){
		return r3b.isSelected();
	}

	public void setReso3BWSelected(boolean selected){
		r3b.setSelected(selected);
	}

	public int getReso3BW(){
		JSpinner input = (JSpinner)r3b.getInput();
		return (Integer)input.getValue();
	}

	public void setReso3BW(int reso3BW){
		JSpinner input = (JSpinner)r3b.getInput();
		input.setValue(reso3BW);
	}

	public boolean isReso4BWSelected(){
		return r4b.isSelected();
	}

	public void setReso4BWSelected(boolean selected){
		r4b.setSelected(selected);
	}

	public int getReso4BW(){
		JSpinner input = (JSpinner)r4b.getInput();
		return (Integer)input.getValue();
	}

	public void setReso4BW(int reso4BW){
		JSpinner input = (JSpinner)r4b.getInput();
		input.setValue(reso4BW);
	}

	public boolean isReso1AmpSelected(){
		return r1a.isSelected();
	}

	public void setReso1AmpSelected(boolean selected){
		r1a.setSelected(selected);
	}

	public int getReso1Amp(){
		JSpinner input = (JSpinner)r1a.getInput();
		return (Integer)input.getValue();
	}

	public void setReso1Amp(int reso1Amp){
		JSpinner input = (JSpinner)r1a.getInput();
		input.setValue(reso1Amp);
	}

	public boolean isReso2AmpSelected(){
		return r2a.isSelected();
	}

	public void setReso2AmpSelected(boolean selected){
		r2a.setSelected(selected);
	}

	public int getReso2Amp(){
		JSpinner input = (JSpinner)r2a.getInput();
		return (Integer)input.getValue();
	}

	public void setReso2Amp(int reso2Amp){
		JSpinner input = (JSpinner)r2a.getInput();
		input.setValue(reso2Amp);
	}

	public boolean isReso3AmpSelected(){
		return r3a.isSelected();
	}

	public void setReso3AmpSelected(boolean selected){
		r3a.setSelected(selected);
	}

	public int getReso3Amp(){
		JSpinner input = (JSpinner)r3a.getInput();
		return (Integer)input.getValue();
	}

	public void setReso3Amp(int reso3Amp){
		JSpinner input = (JSpinner)r3a.getInput();
		input.setValue(reso3Amp);
	}

	public boolean isReso4AmpSelected(){
		return r4a.isSelected();
	}

	public void setReso4AmpSelected(boolean selected){
		r4a.setSelected(selected);
	}

	public int getReso4Amp(){
		JSpinner input = (JSpinner)r4a.getInput();
		return (Integer)input.getValue();
	}

	public void setReso4Amp(int reso4Amp){
		JSpinner input = (JSpinner)r4a.getInput();
		input.setValue(reso4Amp);
	}
}
