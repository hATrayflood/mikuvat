package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CheckAndInput extends JPanel{
	private static final long serialVersionUID = -3729450735450290186L;
	private JCheckBox check;
	private Component input;

	public CheckAndInput(JCheckBox c, Component i){
		check = c;
		input = i;
		setLayout(new FlowLayout());
		add(check);
		add(input);
	
		check.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				input.setEnabled(check.isSelected());
			}
		});
	}

	public CheckAndInput(String c, Component i){
		this(new JCheckBox(c), i);
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		check.setEnabled(enabled);
		if(enabled){
			enabled = check.isSelected();
		}
		input.setEnabled(enabled);
	}

	public JCheckBox getCheck(){
		return check;
	}

	public void setCheck(JCheckBox check){
		this.check = check;
	}

	public Component getInput(){
		return input;
	}

	public void setInput(Component input){
		this.input = input;
	}

	public boolean isSelected(){
		return check.isSelected();
	}

	public void setSelected(boolean enabled){
		check.setSelected(enabled);
	}
}
