package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.TrackPanel.PANEL_NAME;

import java.awt.Color;
import java.awt.Window;

import org.rayflood.mikuvat.io.vsq.CommonSection;
import org.rayflood.mikuvat.io.vsq.MixerSetting;

public class TrackDialog extends PropertyDialog{
	private static final long serialVersionUID = -8238539482647404916L;
	private TrackPanel trackPanel;

	public TrackDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public TrackDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		trackPanel = new TrackPanel();
		setComponent(trackPanel);
		pack();
	}

	public void setSelected(boolean selected){
		trackPanel.setSelected(selected);
	}

	public TrackPanel getTrackPanel(){
		return trackPanel;
	}

	public void setTrackPanel(TrackPanel trackPanel){
		this.trackPanel = trackPanel;
	}

	public CommonSection getCommonSection(){
		return trackPanel.getCommonSection();
	}

	public void setCommonSection(CommonSection common){
		trackPanel.setCommonSection(common);
	}

	public MixerSetting getMixerSetting(){
		return trackPanel.getMixerSetting();
	}

	public void setMixerSetting(MixerSetting mixer){
		trackPanel.setMixerSetting(mixer);
	}

	public boolean isTrackNameSelected(){
		return trackPanel.isTrackNameSelected();
	}

	public void setTrackNameSelected(boolean selected){
		trackPanel.setTrackNameSelected(selected);
	}

	public String getTrackName(){
		return trackPanel.getTrackName();
	}

	public void setTrackName(String name){
		trackPanel.setTrackName(name);
	}

	public boolean isColorSelected(){
		return trackPanel.isColorSelected();
	}

	public void setColorSelected(boolean selected){
		trackPanel.setColorSelected(selected);
	}

	public Color getColor(){
		return trackPanel.getColor();
	}

	public void setColor(Color color){
		trackPanel.setColor(color);
	}

	public boolean isFederSelected(){
		return trackPanel.isFederSelected();
	}

	public void setFederSelected(boolean selected){
		trackPanel.setFederSelected(selected);
	}

	public int getFeder(){
		return trackPanel.getFeder();
	}

	public void setFeder(int feder){
		trackPanel.setFeder(feder);
	}

	public boolean isPanpotSelected(){
		return trackPanel.isPanpotSelected();
	}

	public void setPanpotSelected(boolean selected){
		trackPanel.setPanpotSelected(selected);
	}

	public int getPanpot(){
		return trackPanel.getPanpot();
	}

	public void setPanpot(int panpot){
		trackPanel.setPanpot(panpot);
	}

	public boolean isMuteSelected(){
		return trackPanel.isMuteSelected();
	}

	public void setMuteSelected(boolean selected){
		trackPanel.setMuteSelected(selected);
	}

	public boolean getMute(){
		return trackPanel.getMute();
	}

	public void setMute(boolean mute){
		trackPanel.setMute(mute);
	}

	public boolean isSoloSelected(){
		return trackPanel.isSoloSelected();
	}

	public void setSoloSelected(boolean selected){
		trackPanel.setSoloSelected(selected);
	}

	public boolean getSolo(){
		return trackPanel.getSolo();
	}

	public void setSolo(boolean solo){
		trackPanel.setSolo(solo);
	}
}
