package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.Utilities.getVibratoBPList;
import static org.rayflood.mikuvat.Utilities.getVibratoBPMap;
import static org.rayflood.mikuvat.io.vsq.VibratoBP.Y_MAX;
import static org.rayflood.mikuvat.io.vsq.VibratoBP.Y_MIN;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import org.rayflood.mikuvat.io.vsq.VibratoBP;

public class VibratoBPTable extends JPanel{
	private static final long serialVersionUID = -5339648092376060378L;
	private SpinnerNumberModel lengthspin;
	private JCheckBox check;
	private DefaultTableModel vibratoBPList;
	private JTable table;
	private JButton rev;
	private JButton add;
	private JButton del;

	public VibratoBPTable(String label){
		vibratoBPList = new DefaultTableModel();
		vibratoBPList.addColumn("時間");
		vibratoBPList.addColumn("強弱");
		table = new JTable(vibratoBPList);
		table.changeSelection(0, 0, false, false);
		lengthspin = new SpinnerNumberModel(1, 1, 1, 1);
		JSpinner xe = new JSpinner(lengthspin);
		JSpinner ye = new JSpinner(new SpinnerNumberModel(0, Y_MIN, Y_MAX, 1));
		table.getColumnModel().getColumn(0).setCellEditor(new JSpinnerCellEditor(xe));
		table.getColumnModel().getColumn(1).setCellEditor(new JSpinnerCellEditor(ye));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scroll = new JScrollPane(table
				, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS
				, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		check = new JCheckBox(label);
		rev = new JButton("補正");
		add = new JButton("追加");
		del = new JButton("削除");
		check.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				setEnabledFields(check.isSelected());
			}
		});
		rev.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				revise();
			}
		});
		add.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				addRow();
			}
		});
		del.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				removeRow();
			}
		});

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(check);
		panel.add(rev);
		panel.add(add);
		panel.add(del);

		setMaximumSize(new Dimension(128, 133));
		setLayout(new BorderLayout());
		add(panel, BorderLayout.NORTH);
		add(scroll, BorderLayout.CENTER);
	}

	public void setVibratoBP(int vibratoLength, List<VibratoBP> vibratoBP){
		setVibratoLength(vibratoLength);
		setVibratoBP(vibratoBP);
	}

	public void changeVibratoLength(int vibratoLength){
		if(vibratoLength > 0){
			List<VibratoBP> vibratoBP = getVibratoBP();
			setVibratoLength(vibratoLength);
			setVibratoBP(vibratoBP);
		}
	}

	public int getVibratoLength(){
		return (Integer)lengthspin.getMaximum();
	}

	public void setVibratoLength(int vibratoLength){
		if(vibratoLength > 0){
			lengthspin.setMaximum(vibratoLength);
		}
	}

	public List<VibratoBP> getVibratoBP(){
		List<VibratoBP> vibratoBP = new ArrayList<VibratoBP>();
		for(int i = 0; i < vibratoBPList.getRowCount(); i++){
			int xi = (Integer)vibratoBPList.getValueAt(i, 0);
			double x = (double)xi / (double)getVibratoLength();
			int y = (Integer)vibratoBPList.getValueAt(i, 1);
			vibratoBP.add(new VibratoBP(new BigDecimal(x, MathContext.DECIMAL32), y));
		}
		return vibratoBP;
	}

	public void setVibratoBP(List<VibratoBP> vibratoBP){
		removeVibratoBP();
		addVibratoBP(vibratoBP);
	}

	public void addVibratoBP(List<VibratoBP> vibratoBP){
		for(int i = 0; i < vibratoBP.size(); i++){
			VibratoBP vbp = vibratoBP.get(i);
			Integer bp[] = new Integer[2];
			bp[0] = (int)StrictMath.ceil((vbp.getX().doubleValue() * (double)getVibratoLength()));
			bp[1] = vbp.getY();
			vibratoBPList.addRow(bp);
		}
	}

	public void removeVibratoBP(){
		while(vibratoBPList.getRowCount() > 0){
			vibratoBPList.removeRow(0);
		}
	}

	public void revise(){
		SortedMap<BigDecimal, Integer> bpm = getVibratoBPMap(getVibratoBP());
		setVibratoBP(getVibratoBPList(bpm));
	}

	public void addRow(){
		try{
			int row = table.getSelectedRow();
			vibratoBPList.insertRow(row, new Integer[]{1, 0});
			table.changeSelection(row, 0, false, false);
		}
		catch(Exception e){
			vibratoBPList.addRow(new Integer[]{1, 0});
			table.changeSelection(table.getRowCount() - 1, 0, false, false);
		}
	}

	public void removeRow(){
		try{
			int row = table.getSelectedRow();
			vibratoBPList.removeRow(row);
			if(row > table.getRowCount() - 1){
				row = table.getRowCount() - 1;
			}
			table.changeSelection(row, 0, false, false);
		}
		catch(Exception ex){
			// なにもしない
		}
	}

	public void setEnabledFields(boolean enabled){
		add.setEnabled(enabled);
		del.setEnabled(enabled);
		rev.setEnabled(enabled);
		table.setEnabled(enabled);
		table.clearSelection();
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		check.setEnabled(enabled);
		if(enabled){
			enabled = check.isSelected();
		}
		setEnabledFields(enabled);
	}

	public boolean isSelected(){
		return check.isSelected();
	}

	public void setSelected(boolean selected){
		check.setSelected(selected);
	}
}
