package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.AnotePanel.PANEL_NAME;

import java.awt.Window;

import org.rayflood.mikuvat.io.vsq.AnoteEvent;
import org.rayflood.mikuvat.io.vsq.LyricHandle;
import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class AnoteDialog extends PropertyDialog{
	private static final long serialVersionUID = 3086336439858028854L;
	private AnotePanel anotePanel;

	public AnoteDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public AnoteDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		anotePanel = new AnotePanel(new VibratoHandleButton());
		setComponent(anotePanel);
		pack();
	}

	public void setSelected(boolean selected){
		anotePanel.setSelected(selected);
	}

	public AnotePanel getAnotePanel(){
		return anotePanel;
	}

	public void setAnotePanel(AnotePanel anotePanel){
		this.anotePanel = anotePanel;
	}

	public void changeNoteLength(int noteLength){
		anotePanel.changeNoteLength(noteLength);
	}

	public AnoteEvent getAnoteEvent(){
		return anotePanel.getAnoteEvent();
	}

	public void setAnoteEvent(AnoteEvent anote){
		anotePanel.setAnoteEvent(anote);
	}

	public VibratoHandleInput getVibratoHandleInput(){
		return anotePanel.getVibratoHandleInput();
	}

	public void setVibratoHandleInput(VibratoHandleInput vibratoHandleInput){
		anotePanel.setVibratoHandleInput(vibratoHandleInput);
	}

	public boolean isLyricSelected(){
		return anotePanel.isLyricSelected();
	}

	public void setLyricSelected(boolean selected){
		anotePanel.setLyricSelected(selected);
	}

	public String getLyric(){
		return anotePanel.getLyric();
	}

	public void setLyric(String lyric){
		anotePanel.setLyric(lyric);
	}

	public boolean isPhoneticSelected(){
		return anotePanel.isPhoneticSelected();
	}

	public void setPhoneticSelected(boolean selected){
		anotePanel.setPhoneticSelected(selected);
	}

	public String getPhonetic(){
		return anotePanel.getPhonetic();
	}

	public void setPhonetic(String phonetic){
		anotePanel.setPhonetic(phonetic);
	}

	public boolean isProtectSelected(){
		return anotePanel.isProtectSelected();
	}

	public void setProtectSelected(boolean selected){
		anotePanel.setProtectSelected(selected);
	}

	public boolean getProtect(){
		return anotePanel.getProtect();
	}

	public void setProtect(boolean protect){
		anotePanel.setProtect(protect);
	}

	public boolean isVelocitySelected(){
		return anotePanel.isVelocitySelected();
	}

	public void setVelocitySelected(boolean selected){
		anotePanel.setVelocitySelected(selected);
	}

	public int getVelocity(){
		return anotePanel.getVelocity();
	}

	public void setVelocity(int velocity){
		anotePanel.setVelocity(velocity);
	}

	public boolean isNoteSelected(){
		return anotePanel.isNoteSelected();
	}

	public void setNoteSelected(boolean selected){
		anotePanel.setNoteSelected(selected);
	}

	public int getNote(){
		return anotePanel.getNote();
	}

	public void setNote(int note){
		anotePanel.setNote(note);
	}

	public boolean isLengthSelected(){
		return anotePanel.isLengthSelected();
	}

	public void setLengthSelected(boolean selected){
		anotePanel.setLengthSelected(selected);
	}

	public int getLength(){
		return anotePanel.getLength();
	}

	public void setLength(int length){
		anotePanel.setLength(length);
	}

	public boolean isBendDepthSelected(){
		return anotePanel.isBendDepthSelected();
	}

	public void setBendDepthSelected(boolean selected){
		anotePanel.setBendDepthSelected(selected);
	}

	public int getBendDepth(){
		return anotePanel.getBendDepth();
	}

	public void setBendDepth(int bendDepth){
		anotePanel.setBendDepth(bendDepth);
	}

	public boolean isBendLengthSelected(){
		return anotePanel.isBendLengthSelected();
	}

	public void setBendLengthSelected(boolean selected){
		anotePanel.setBendLengthSelected(selected);
	}

	public int getBendLength(){
		return anotePanel.getBendLength();
	}

	public void setBendLength(int bendLength){
		anotePanel.setBendLength(bendLength);
	}

	public boolean isPortamentoUseSelected(){
		return anotePanel.isPortamentoUseSelected();
	}

	public void setPortamentoUseSelected(boolean selected){
		anotePanel.setPortamentoUseSelected(selected);
	}

	public int getPortamentoUse(){
		return anotePanel.getPortamentoUse();
	}

	public void setPortamentoUse(int portamentoUse){
		anotePanel.setPortamentoUse(portamentoUse);
	}

	public boolean isDecaySelected(){
		return anotePanel.isDecaySelected();
	}

	public void setDecaySelected(boolean selected){
		anotePanel.setDecaySelected(selected);
	}

	public int getDecay(){
		return anotePanel.getDecay();
	}

	public void setDecay(int decay){
		anotePanel.setDecay(decay);
	}

	public boolean isAccentSelected(){
		return anotePanel.isAccentSelected();
	}

	public void setAccentSelected(boolean selected){
		anotePanel.setAccentSelected(selected);
	}

	public int getAccent(){
		return anotePanel.getAccent();
	}

	public void setAccent(int accent){
		anotePanel.setAccent(accent);
	}

	public LyricHandle getLyricHandle(){
		return anotePanel.getLyricHandle();
	}

	public void setLyricHandle(LyricHandle lyricHandle){
		anotePanel.setLyricHandle(lyricHandle);
	}

	public boolean isVibratoHandleSelected(){
		return anotePanel.isVibratoHandleSelected();
	}

	public void setVibratoHandleSelected(boolean selected){
		anotePanel.setVibratoHandleSelected(selected);
	}

	public VibratoHandle getVibratoHandle(){
		return anotePanel.getVibratoHandle();
	}

	public void setVibratoHandle(VibratoHandle vibratoHandle){
		anotePanel.setVibratoHandle(vibratoHandle);
	}
}
