package org.rayflood.mikuvat.gui;

import java.awt.Component;

import javax.swing.JLabel;

import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class VibratoHandleLabel extends JLabel implements VibratoHandleInput{
	private static final long serialVersionUID = 7092324251698933313L;
	private VibratoPanel vibratoPanel = new VibratoPanel();

	public Component getVibratoHandlePanel(){
		return this;
	}

	public VibratoHandle getVibratoHandle(){
		return vibratoPanel.getVibratoHandle();
	}

	public void setVibratoHandle(int noteLength, VibratoHandle vibratoHandle){
		vibratoPanel.setVibratoHandle(noteLength, vibratoHandle);
	}

	public void changeNoteLength(int noteLength){
		vibratoPanel.changeNoteLength(noteLength);
	}

	public VibratoPanel getVibratoPanel(){
		return vibratoPanel;
	}

	public void setVibratoPanel(VibratoPanel vibratoPanel){
		this.vibratoPanel = vibratoPanel;
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		if(enabled){
			setText("On");
		}
		else{
			setText("Off");
		}
		vibratoPanel.setEnabled(enabled);
	}
}
