package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.TrackSelectPanel.PANEL_NAME;

import java.awt.Window;

public class TrackSelectDialog extends PropertyDialog{
	private static final long serialVersionUID = 3440171188924061123L;
	private TrackSelectPanel trackSelectPanel;

	public TrackSelectDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public TrackSelectDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		trackSelectPanel = new TrackSelectPanel();
		setComponent(trackSelectPanel);
		pack();
	}

	public void setSelected(boolean enabled){
		trackSelectPanel.setSelected(enabled);
	}

	public TrackSelectPanel getTrackSelectPanel(){
		return trackSelectPanel;
	}

	public void setTrackSelectPanel(TrackSelectPanel trackSelectPanel){
		this.trackSelectPanel = trackSelectPanel;
	}
}
