package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;

import javax.swing.AbstractCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class JSpinnerCellEditor extends AbstractCellEditor implements TableCellEditor{
	private static final long serialVersionUID = 8377526209984378751L;
	private JSpinner spinner;

	public JSpinnerCellEditor(JSpinner s){
		spinner = s;
		spinner.addFocusListener(new FocusListener(){
			public void focusGained(FocusEvent e){
			}
			public void focusLost(FocusEvent e){
				try{
					spinner.commitEdit();
				}
				catch(ParseException e1){
					// なにもしない
				}
			}
		});
	}

	public void commitEdit(){
		try{
			spinner.commitEdit();
		}
		catch(ParseException e){
			// なにもしない
		}
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column){
		spinner.setValue(value);
		return spinner;
	}

	public Object getCellEditorValue(){
		return spinner.getValue();
	}
}
