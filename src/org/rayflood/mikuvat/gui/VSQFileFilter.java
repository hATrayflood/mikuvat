package org.rayflood.mikuvat.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class VSQFileFilter extends FileFilter{
	public boolean accept(File f){
		if(f != null){
			if(f.isDirectory()){
				return true;
			}
			else if(f.isFile()){
				String name = f.getName();
				int per = name.lastIndexOf(".");
				if(per > 0){
					String suffix = name.substring(per);
					if(".vsq".equalsIgnoreCase(suffix)){
						return true;
					}
					if(".mid".equalsIgnoreCase(suffix)){
						return true;
					}
				}
			}
		}
		return false;
	}

	public String getDescription(){
		return "VOCALOIDシーケンスファイル (*.vsq, *.mid)";
	}
}
