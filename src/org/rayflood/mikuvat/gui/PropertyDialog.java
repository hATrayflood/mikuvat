package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.Mikuvat.ICON;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

public abstract class PropertyDialog extends JDialog{
	private static final long serialVersionUID = -8404713988778269564L;
	private Component component;
	private boolean ok;

	protected abstract void init();

	public abstract void setSelected(boolean selected);

	public PropertyDialog(Window owner, String title){
		super(owner, title, ModalityType.APPLICATION_MODAL);
		initPropertyDialog();
	}

	public PropertyDialog(Window owner, String title, boolean modal){
		super(owner, title, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
		initPropertyDialog();
	}

	protected void initPropertyDialog(){
		JButton okbutton = new JButton("OK");
		JButton ngbutton = new JButton("NG");
		okbutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				ok = true;
				setVisible(false);
			}
		});
		ngbutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setVisible(false);
			}
		});
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttons.add(okbutton);
		buttons.add(ngbutton);
		add(buttons, BorderLayout.SOUTH);

		setIconImage(ICON.getImage());
		setLocationByPlatform(true);
		setResizable(false);
	}

	public boolean showDialog(){
		ok = false;
		setVisible(true);
		return ok;
	}

	public Component getComponent(){
		return component;
	}

	public void setComponent(Component component){
		if(this.component != null){
			remove(this.component);
		}
		add(component, BorderLayout.CENTER);
		this.component = component;
	}
}
