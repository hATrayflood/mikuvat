package org.rayflood.mikuvat.gui;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public abstract class PropertyPanel extends JPanel{
	private static final long serialVersionUID = -8106086813773719724L;

	public abstract String getPanelName();

	public abstract void setSelected(boolean selected);

	public void setSpacedBorder(){
		setSpacedBorder(8);
	}

	public void setSpacedBorder(int margin){
		setBorder(new EmptyBorder(margin, margin, margin, margin));
	}

	public void setTitledBorder(){
		setBorder(new TitledBorder(getPanelName()));
	}
}
