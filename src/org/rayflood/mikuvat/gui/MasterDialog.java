package org.rayflood.mikuvat.gui;

import java.awt.Window;

import org.rayflood.mikuvat.io.vsq.MasterSection;
import org.rayflood.mikuvat.io.vsq.MixerSection;
import static org.rayflood.mikuvat.gui.MasterPanel.*;

public class MasterDialog extends PropertyDialog{
	private static final long serialVersionUID = 3397966622502930918L;
	private MasterPanel masterPanel;

	public MasterDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public MasterDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		masterPanel = new MasterPanel();
		setComponent(masterPanel);
		pack();
	}

	public void setSelected(boolean selected){
		masterPanel.setSelected(selected);
	}

	public MasterPanel getMasterPanel(){
		return masterPanel;
	}

	public void setMasterPanel(MasterPanel masterPanel){
		this.masterPanel = masterPanel;
	}

	public MasterSection getMasterSection(){
		return masterPanel.getMasterSection();
	}

	public void setMasterSection(MasterSection master){
		masterPanel.setMasterSection(master);
	}

	public MixerSection getMixerSection(){
		return masterPanel.getMixerSection();
	}

	public void setMixerSection(MixerSection mixer){
		masterPanel.setMixerSection(mixer);
	}

	public boolean isPreMeasureSelected(){
		return masterPanel.isPreMeasureSelected();
	}

	public void setPreMeasureSelected(boolean selected){
		masterPanel.setPreMeasureSelected(selected);
	}

	public int getPreMeasure(){
		return masterPanel.getPreMeasure();
	}

	public void setPreMeasure(int preMeasure){
		masterPanel.setPreMeasure(preMeasure);
	}

	public boolean isFederSelected(){
		return masterPanel.isFederSelected();
	}

	public void setFederSelected(boolean selected){
		masterPanel.setFederSelected(selected);
	}

	public int getFeder(){
		return masterPanel.getFeder();
	}

	public void setFeder(int feder){
		masterPanel.setFeder(feder);
	}

	public boolean isPanpotSelected(){
		return masterPanel.isPanpotSelected();
	}

	public void setPanpotSelected(boolean selected){
		masterPanel.setPanpotSelected(selected);
	}

	public int getPanpot(){
		return masterPanel.getPanpot();
	}

	public void setPanpot(int panpot){
		masterPanel.setPanpot(panpot);
	}

	public boolean isMuteSelected(){
		return masterPanel.isMuteSelected();
	}

	public void setMuteSelected(boolean selected){
		masterPanel.setMuteSelected(selected);
	}

	public boolean getMute(){
		return masterPanel.getMute();
	}

	public void setMute(boolean mute){
		masterPanel.setMute(mute);
	}

	public boolean isOutputModeSelected(){
		return masterPanel.isOutputModeSelected();
	}

	public void setOutputModeSelected(boolean selected){
		masterPanel.setOutputModeSelected(selected);
	}

	public int getOutputMode(){
		return masterPanel.getOutputMode();
	}

	public void setOutputMode(int outputMode){
		masterPanel.setOutputMode(outputMode);
	}
}
