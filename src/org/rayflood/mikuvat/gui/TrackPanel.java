package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.MixerSection.FEDER_MAX;
import static org.rayflood.mikuvat.io.vsq.MixerSection.FEDER_MIN;
import static org.rayflood.mikuvat.io.vsq.MixerSection.PANPOT_MAX;
import static org.rayflood.mikuvat.io.vsq.MixerSection.PANPOT_MIN;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import org.rayflood.mikuvat.io.vsq.CommonSection;
import org.rayflood.mikuvat.io.vsq.MixerSetting;

public class TrackPanel extends PropertyPanel{
	private static final long serialVersionUID = 9046489884524194988L;
	public static final String PANEL_NAME = "トラック設定";
	private String version;
	private int dynamicsMode;
	private int playMode;
	private CheckAndInput namePanel;
	private CheckAndInput colorPanel;
	private CheckAndInput federPanel;
	private CheckAndInput panpotPanel;
	private CheckAndInput mutePanel;
	private CheckAndInput soloPanel;

	public TrackPanel(){
		version = "DSB301";
		dynamicsMode = 1;
		playMode = 1;
		namePanel = new CheckAndInput("トラック名", new JTextField());
		colorPanel = new CheckAndInput("トラック色", new ColorSelectPanel());
		federPanel = new CheckAndInput("フェーダー", new JSpinner(new SpinnerNumberModel(0, FEDER_MIN, FEDER_MAX, 1)));
		panpotPanel = new CheckAndInput("パン", new JSpinner(new SpinnerNumberModel(0, PANPOT_MIN, PANPOT_MAX, 1)));
		mutePanel = new CheckAndInput("ミュート", new OnOffBox());
		soloPanel = new CheckAndInput("ソロ", new OnOffBox());

		Component c1[] = new Component[]{
				namePanel.getCheck(), namePanel.getInput()
				, colorPanel.getCheck(), colorPanel.getInput()
		};
		Component c2[] = new Component[]{
				federPanel.getCheck(), federPanel.getInput()
				, mutePanel.getCheck(), mutePanel.getInput()
				, panpotPanel.getCheck(), panpotPanel.getInput()
				, soloPanel.getCheck(), soloPanel.getInput()
		};
		JPanel p1 = new GridPanel(c1, 2);
		JPanel p2 = new GridPanel(c2, 4);

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(new Component[]{p1, p2}, 1), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setTrackNameSelected(selected);
		setColorSelected(selected);
		setFederSelected(selected);
		setPanpotSelected(selected);
		setMuteSelected(selected);
		setSoloSelected(selected);
	}

	public CommonSection getCommonSection(){
		CommonSection common = new CommonSection();
		common.setVersion(version);
		if(isTrackNameSelected()){
			common.setName(getTrackName());
		}
		if(isColorSelected()){
			common.setColor(getColor());
		}
		common.setDynamicsMode(dynamicsMode);
		common.setPlayMode(playMode);
		return common;
	}

	public void setCommonSection(CommonSection common){
		version = common.getVersion();
		if(common.hasName()){
			setTrackName(common.getName());
		}
		else{
			setTrackNameSelected(false);
		}
		if(common.hasColor()){
			setColor(common.getColor());
		}
		else{
			setColorSelected(false);
		}
		dynamicsMode = common.getDynamicsMode();
		playMode = common.getPlayMode();
	}

	public MixerSetting getMixerSetting(){
		MixerSetting mixer = new MixerSetting();
		if(isFederSelected()){
			mixer.setFeder(getFeder());
		}
		if(isPanpotSelected()){
			mixer.setPanpot(getPanpot());
		}
		if(isMuteSelected()){
			mixer.setMute(getMute());
		}
		if(isSoloSelected()){
			mixer.setSolo(getSolo());
		}
		return mixer;
	}

	public void setMixerSetting(MixerSetting mixer){
		if(mixer.hasFeder()){
			setFeder(mixer.getFeder());
		}
		else{
			setFederSelected(false);
		}
		if(mixer.hasPanpot()){
			setPanpot(mixer.getPanpot());
		}
		else{
			setPanpotSelected(false);
		}
		if(mixer.hasMute()){
			setMute(mixer.getMute());
		}
		else{
			setMuteSelected(false);
		}
		if(mixer.hasSolo()){
			setSolo(mixer.getSolo());
		}
		else{
			setSoloSelected(false);
		}
	}

	public boolean isTrackNameSelected(){
		return namePanel.isSelected();
	}

	public void setTrackNameSelected(boolean selected){
		namePanel.setSelected(selected);
	}

	public String getTrackName(){
		JTextField input = (JTextField)namePanel.getInput();
		return input.getText();
	}

	public void setTrackName(String name){
		JTextField input = (JTextField)namePanel.getInput();
		input.setText(name);
	}

	public boolean isColorSelected(){
		return colorPanel.isSelected();
	}

	public void setColorSelected(boolean selected){
		colorPanel.setSelected(selected);
	}

	public Color getColor(){
		ColorSelectPanel input = (ColorSelectPanel)colorPanel.getInput();
		return input.getColor();
	}

	public void setColor(Color color){
		ColorSelectPanel input = (ColorSelectPanel)colorPanel.getInput();
		input.setColor(color);
	}

	public boolean isFederSelected(){
		return federPanel.isSelected();
	}

	public void setFederSelected(boolean selected){
		federPanel.setSelected(selected);
	}

	public int getFeder(){
		JSpinner input = (JSpinner)federPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setFeder(int feder){
		JSpinner input = (JSpinner)federPanel.getInput();
		input.setValue(feder);
	}

	public boolean isPanpotSelected(){
		return panpotPanel.isSelected();
	}

	public void setPanpotSelected(boolean selected){
		panpotPanel.setSelected(selected);
	}

	public int getPanpot(){
		JSpinner input = (JSpinner)panpotPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setPanpot(int panpot){
		JSpinner input = (JSpinner)panpotPanel.getInput();
		input.setValue(panpot);
	}

	public boolean isMuteSelected(){
		return mutePanel.isSelected();
	}

	public void setMuteSelected(boolean selected){
		mutePanel.setSelected(selected);
	}

	public boolean getMute(){
		JCheckBox input = (JCheckBox)mutePanel.getInput();
		return input.isSelected();
	}

	public void setMute(boolean mute){
		JCheckBox input = (JCheckBox)mutePanel.getInput();
		input.setSelected(mute);
	}

	public boolean isSoloSelected(){
		return soloPanel.isSelected();
	}

	public void setSoloSelected(boolean selected){
		soloPanel.setSelected(selected);
	}

	public boolean getSolo(){
		JCheckBox input = (JCheckBox)soloPanel.getInput();
		return input.isSelected();
	}

	public void setSolo(boolean solo){
		JCheckBox input = (JCheckBox)soloPanel.getInput();
		input.setSelected(solo);
	}
}
