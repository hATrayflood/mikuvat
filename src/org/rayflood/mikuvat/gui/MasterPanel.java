package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.MasterSection.PREMEASURE_MAX;
import static org.rayflood.mikuvat.io.vsq.MasterSection.PREMEASURE_MIN;
import static org.rayflood.mikuvat.io.vsq.MixerSection.FEDER_MAX;
import static org.rayflood.mikuvat.io.vsq.MixerSection.FEDER_MIN;
import static org.rayflood.mikuvat.io.vsq.MixerSection.OUTPUTMODE_PARAMLIST;
import static org.rayflood.mikuvat.io.vsq.MixerSection.PANPOT_MAX;
import static org.rayflood.mikuvat.io.vsq.MixerSection.PANPOT_MIN;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.TreeMap;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.rayflood.mikuvat.io.vsq.MasterSection;
import org.rayflood.mikuvat.io.vsq.MixerSection;
import org.rayflood.mikuvat.io.vsq.MixerSetting;

public class MasterPanel extends PropertyPanel{
	private static final long serialVersionUID = 1972919633507868852L;
	public static final String PANEL_NAME = "マスター設定";
	private TreeMap<Integer, MixerSetting> mixerSettings;
	private CheckAndInput preMeasurePanel;
	private CheckAndInput federPanel;
	private CheckAndInput panpotPanel;
	private CheckAndInput mutePanel;
	private CheckAndInput outputModePanel;

	public MasterPanel(){
		mixerSettings = new TreeMap<Integer, MixerSetting>();
		mixerSettings.put(0, new MixerSetting());
		preMeasurePanel = new CheckAndInput("プリメジャー", new JSpinner(new SpinnerNumberModel(1, PREMEASURE_MIN, PREMEASURE_MAX, 1)));
		federPanel = new CheckAndInput("フェーダー", new JSpinner(new SpinnerNumberModel(0, FEDER_MIN, FEDER_MAX, 1)));
		panpotPanel = new CheckAndInput("パン", new JSpinner(new SpinnerNumberModel(0, PANPOT_MIN, PANPOT_MAX, 1)));
		mutePanel = new CheckAndInput("ミュート", new OnOffBox());
		outputModePanel = new CheckAndInput("出力デバイス", new JComboBox(OUTPUTMODE_PARAMLIST.toArray(new String[0])));

		Component c1[] = new Component[]{
				federPanel.getCheck(), federPanel.getInput()
				, mutePanel.getCheck(), mutePanel.getInput()
				, panpotPanel.getCheck(), panpotPanel.getInput()
				, preMeasurePanel.getCheck(), preMeasurePanel.getInput()
		};
		Component c2[] = new Component[]{
				outputModePanel.getCheck(), outputModePanel.getInput()
		};
		JPanel p1 = new GridPanel(c1, 4);
		JPanel p2 = new GridPanel(c2, 2);

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(new Component[]{p1, p2}, 1), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setPreMeasureSelected(selected);
		setFederSelected(selected);
		setPanpotSelected(selected);
		setMuteSelected(selected);
		setOutputModeSelected(selected);
	}

	public MasterSection getMasterSection(){
		MasterSection master = new MasterSection();
		if(isPreMeasureSelected()){
			master.setPreMeasure(getPreMeasure());
		}
		return master;
	}

	public void setMasterSection(MasterSection master){
		if(master.hasPreMeasure()){
			setPreMeasure(master.getPreMeasure());
		}
		else{
			setPreMeasureSelected(false);
		}
	}

	public MixerSection getMixerSection(){
		MixerSection mixer = new MixerSection();
		if(isFederSelected()){
			mixer.setMasterFeder(getFeder());
		}
		if(isPanpotSelected()){
			mixer.setMasterPanpot(getPanpot());
		}
		if(isMuteSelected()){
			mixer.setMasterMute(getMute());
		}
		if(isOutputModeSelected()){
			mixer.setOutputMode(getOutputMode());
		}
		mixer.setMixerSettings(mixerSettings);
		return mixer;
	}

	public void setMixerSection(MixerSection mixer){
		if(mixer.hasMasterFeder()){
			setFeder(mixer.getMasterFeder());
		}
		else{
			setFederSelected(false);
		}
		if(mixer.hasMasterPanpot()){
			setPanpot(mixer.getMasterPanpot());
		}
		else{
			setPanpotSelected(false);
		}
		if(mixer.hasMasterMute()){
			setMute(mixer.getMasterMute());
		}
		else{
			setMuteSelected(false);
		}
		if(mixer.hasOutputMode()){
			setOutputMode(mixer.getOutputMode());
		}
		else{
			setOutputModeSelected(false);
		}
		mixerSettings = mixer.getMixerSettings();
	}

	public boolean isPreMeasureSelected(){
		return preMeasurePanel.isSelected();
	}

	public void setPreMeasureSelected(boolean selected){
		preMeasurePanel.setSelected(selected);
	}

	public int getPreMeasure(){
		JSpinner input = (JSpinner)preMeasurePanel.getInput();
		return (Integer)input.getValue();
	}

	public void setPreMeasure(int preMeasure){
		JSpinner input = (JSpinner)preMeasurePanel.getInput();
		input.setValue(preMeasure);
	}

	public boolean isFederSelected(){
		return federPanel.isSelected();
	}

	public void setFederSelected(boolean selected){
		federPanel.setSelected(selected);
	}

	public int getFeder(){
		JSpinner input = (JSpinner)federPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setFeder(int feder){
		JSpinner input = (JSpinner)federPanel.getInput();
		input.setValue(feder);
	}

	public boolean isPanpotSelected(){
		return panpotPanel.isSelected();
	}

	public void setPanpotSelected(boolean selected){
		panpotPanel.setSelected(selected);
	}

	public int getPanpot(){
		JSpinner input = (JSpinner)panpotPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setPanpot(int panpot){
		JSpinner input = (JSpinner)panpotPanel.getInput();
		input.setValue(panpot);
	}

	public boolean isMuteSelected(){
		return mutePanel.isSelected();
	}

	public void setMuteSelected(boolean selected){
		mutePanel.setSelected(selected);
	}

	public boolean getMute(){
		JCheckBox input = (JCheckBox)mutePanel.getInput();
		return input.isSelected();
	}

	public void setMute(boolean mute){
		JCheckBox input = (JCheckBox)mutePanel.getInput();
		input.setSelected(mute);
	}

	public boolean isOutputModeSelected(){
		return outputModePanel.isSelected();
	}

	public void setOutputModeSelected(boolean selected){
		outputModePanel.setSelected(selected);
	}

	public int getOutputMode(){
		JComboBox input = (JComboBox)outputModePanel.getInput();
		return input.getSelectedIndex();
	}

	public void setOutputMode(int outputMode){
		JComboBox input = (JComboBox)outputModePanel.getInput();
		input.setSelectedIndex(outputMode);
	}
}
