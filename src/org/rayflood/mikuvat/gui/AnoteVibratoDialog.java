package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.awt.Window;

public class AnoteVibratoDialog extends PropertyDialog{
	private static final long serialVersionUID = 3101492331759339199L;
	private AnotePanel anotePanel;
	private VibratoPanel vibratoPanel;

	public AnoteVibratoDialog(Window owner){
		super(owner, "音符・ビブラート");
		init();
	}

	public AnoteVibratoDialog(Window owner, boolean modal){
		super(owner, "音符・ビブラート", modal);
		init();
	}

	protected void init(){
		anotePanel = new AnotePanel();
		vibratoPanel = ((VibratoHandleLabel)anotePanel.getVibratoHandleInput()).getVibratoPanel();
		setComponent(new GridPanel(new Component[]{anotePanel, vibratoPanel}, 1));
		pack();
	}

	public void setSelected(boolean selected){
		anotePanel.setSelected(selected);
		vibratoPanel.setSelected(selected);
	}

	public AnotePanel getAnotePanel(){
		return anotePanel;
	}

	public void setAnotePanel(AnotePanel anotePanel){
		this.anotePanel = anotePanel;
	}

	public VibratoPanel getVibratoPanel(){
		return vibratoPanel;
	}

	public void setVibratoPanel(VibratoPanel vibratoPanel){
		this.vibratoPanel = vibratoPanel;
	}
}
