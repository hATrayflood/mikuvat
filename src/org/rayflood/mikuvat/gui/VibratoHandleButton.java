package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class VibratoHandleButton extends JButton implements VibratoHandleInput{
	private static final long serialVersionUID = 3945342580841712798L;
	private VibratoDialog vibratoDialog;

	public VibratoHandleButton(){
		super("設定...");
		vibratoDialog = new VibratoDialog(JOptionPane.getFrameForComponent(this));
		addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				vibratoDialog.showDialog();
			}
		});
	}

	public Component getVibratoHandlePanel(){
		return this;
	}

	public VibratoHandle getVibratoHandle(){
		return vibratoDialog.getVibratoHandle();
	}

	public void setVibratoHandle(int noteLength, VibratoHandle vibratoHandle){
		vibratoDialog.setVibratoHandle(noteLength, vibratoHandle);
	}

	public void changeNoteLength(int noteLength){
		vibratoDialog.changeNoteLength(noteLength);
	}

	public VibratoDialog getVibratoDialog(){
		return vibratoDialog;
	}

	public void setVibratoDialog(VibratoDialog vibratoDialog){
		this.vibratoDialog = vibratoDialog;
	}
}
