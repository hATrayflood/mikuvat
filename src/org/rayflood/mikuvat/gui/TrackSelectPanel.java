package org.rayflood.mikuvat.gui;

import java.awt.BorderLayout;

import javax.swing.JCheckBox;

public class TrackSelectPanel extends PropertyPanel{
	private static final long serialVersionUID = -6682427982914960456L;
	public static final String PANEL_NAME = "トラック選択";
	private JCheckBox tracks[] = new JCheckBox[16];

	public TrackSelectPanel(){
		for(int i = 0; i < tracks.length; i++){
			tracks[i] = new JCheckBox("トラック" + (i + 1));
		}

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(tracks, 4), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		for(int i = 0; i < tracks.length; i++){
			tracks[i].setSelected(selected);
		}
	}
}
