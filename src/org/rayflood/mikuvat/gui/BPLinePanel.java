package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.EventBPTime.BREATHINESS_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.BREATHINESS_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.BRIGHTNESS_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.BRIGHTNESS_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.CLEARNESS_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.CLEARNESS_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.DYNAMICS_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.DYNAMICS_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.GENDERFACTOR_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.GENDERFACTOR_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.OPENING_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.OPENING_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PITCHBENDSENS_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PITCHBENDSENS_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PITCHBEND_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PITCHBEND_MIN;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PORTAMENTOTIMING_MAX;
import static org.rayflood.mikuvat.io.vsq.EventBPTime.PORTAMENTOTIMING_MIN;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.rayflood.mikuvat.io.vsq.EventBPTime;

public class BPLinePanel extends PropertyPanel{
	private static final long serialVersionUID = 8279919086537385501L;
	public static final String PANEL_NAME = "曲線パラメータ";
	private CheckAndInput dyn;
	private CheckAndInput bre;
	private CheckAndInput bri;
	private CheckAndInput cle;
	private CheckAndInput ope;
	private CheckAndInput gen;
	private CheckAndInput por;
	private CheckAndInput pit;
	private CheckAndInput pbs;

	public BPLinePanel(){
		dyn = new CheckAndInput("DYN", new JSpinner(new SpinnerNumberModel(0, DYNAMICS_MIN, DYNAMICS_MAX, 1)));
		bre = new CheckAndInput("BRE", new JSpinner(new SpinnerNumberModel(0, BREATHINESS_MIN, BREATHINESS_MAX, 1)));
		bri = new CheckAndInput("BRI", new JSpinner(new SpinnerNumberModel(0, BRIGHTNESS_MIN, BRIGHTNESS_MAX, 1)));
		cle = new CheckAndInput("CLE", new JSpinner(new SpinnerNumberModel(0, CLEARNESS_MIN, CLEARNESS_MAX, 1)));
		ope = new CheckAndInput("OPE", new JSpinner(new SpinnerNumberModel(0, OPENING_MIN, OPENING_MAX, 1)));
		gen = new CheckAndInput("GEN", new JSpinner(new SpinnerNumberModel(0, GENDERFACTOR_MIN, GENDERFACTOR_MAX, 1)));
		por = new CheckAndInput("POR", new JSpinner(new SpinnerNumberModel(0, PORTAMENTOTIMING_MIN, PORTAMENTOTIMING_MAX, 1)));
		pit = new CheckAndInput("PIT", new JSpinner(new SpinnerNumberModel(0, PITCHBEND_MIN, PITCHBEND_MAX, 1)));
		pbs = new CheckAndInput("PBS", new JSpinner(new SpinnerNumberModel(0, PITCHBENDSENS_MIN, PITCHBENDSENS_MAX, 1)));

		Component components[] = new Component[]{
				dyn.getCheck(), dyn.getInput()
				, bre.getCheck(), bre.getInput()
				, bri.getCheck(), bri.getInput()
				, cle.getCheck(), cle.getInput()
				, ope.getCheck(), ope.getInput()
				, gen.getCheck(), gen.getInput()
				, por.getCheck(), por.getInput()
				, pit.getCheck(), pit.getInput()
				, pbs.getCheck(), pbs.getInput()
		};

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(components, 6), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setDynamicsSelected(selected);
		setBreathinessSelected(selected);
		setBrightnessSelected(selected);
		setClearnessSelected(selected);
		setOpeningSelected(selected);
		setGenderFactorSelected(selected);
		setPortamentoTimingSelected(selected);
		setPitchBendSelected(selected);
		setPitchBendSensSelected(selected);
	}

	public EventBPTime getEventBPTime(){
		EventBPTime bp = new EventBPTime();
		if(isDynamicsSelected()){
			bp.setDynamics(getDynamics());
		}
		if(isBreathinessSelected()){
			bp.setBreathiness(getBreathiness());
		}
		if(isBrightnessSelected()){
			bp.setBrightness(getBrightness());
		}
		if(isClearnessSelected()){
			bp.setClearness(getClearness());
		}
		if(isOpeningSelected()){
			bp.setOpening(getOpening());
		}
		if(isGenderFactorSelected()){
			bp.setGenderFactor(getGenderFactor());
		}
		if(isPortamentoTimingSelected()){
			bp.setPortamentoTiming(getPortamentoTiming());
		}
		if(isPitchBendSelected()){
			bp.setPitchBend(getPitchBend());
		}
		if(isPitchBendSensSelected()){
			bp.setPitchBendSens(getPitchBendSens());
		}
		return bp;
	}

	public void setEventBPTime(EventBPTime bp){
		if(bp.hasDynamics()){
			setDynamics(bp.getDynamics());
		}
		else{
			setDynamicsSelected(false);
		}
		if(bp.hasBreathiness()){
			setBreathiness(bp.getBreathiness());
		}
		else{
			setBreathinessSelected(false);
		}
		if(bp.hasBrightness()){
			setBrightness(bp.getBrightness());
		}
		else{
			setBrightnessSelected(false);
		}
		if(bp.hasClearness()){
			setClearness(bp.getClearness());
		}
		else{
			setClearnessSelected(false);
		}
		if(bp.hasOpening()){
			setOpening(bp.getOpening());
		}
		else{
			setOpeningSelected(false);
		}
		if(bp.hasGenderFactor()){
			setGenderFactor(bp.getGenderFactor());
		}
		else{
			setGenderFactorSelected(false);
		}
		if(bp.hasPortamentoTiming()){
			setPortamentoTiming(bp.getPortamentoTiming());
		}
		else{
			setPortamentoTimingSelected(false);
		}
		if(bp.hasPitchBend()){
			setPitchBend(bp.getPitchBend());
		}
		else{
			setPitchBendSelected(false);
		}
		if(bp.hasPitchBendSens()){
			setPitchBendSens(bp.getPitchBendSens());
		}
		else{
			setPitchBendSensSelected(false);
		}
	}

	public boolean isDynamicsSelected(){
		return dyn.isSelected();
	}

	public void setDynamicsSelected(boolean selected){
		dyn.setSelected(selected);
	}

	public int getDynamics(){
		JSpinner input = (JSpinner)dyn.getInput();
		return (Integer)input.getValue();
	}

	public void setDynamics(int dynamics){
		JSpinner input = (JSpinner)dyn.getInput();
		input.setValue(dynamics);
	}

	public boolean isBreathinessSelected(){
		return bre.isSelected();
	}

	public void setBreathinessSelected(boolean selected){
		bre.setSelected(selected);
	}

	public int getBreathiness(){
		JSpinner input = (JSpinner)bre.getInput();
		return (Integer)input.getValue();
	}

	public void setBreathiness(int breathiness){
		JSpinner input = (JSpinner)bre.getInput();
		input.setValue(breathiness);
	}

	public boolean isBrightnessSelected(){
		return bri.isSelected();
	}

	public void setBrightnessSelected(boolean selected){
		bri.setSelected(selected);
	}

	public int getBrightness(){
		JSpinner input = (JSpinner)bri.getInput();
		return (Integer)input.getValue();
	}

	public void setBrightness(int brightness){
		JSpinner input = (JSpinner)bri.getInput();
		input.setValue(brightness);
	}

	public boolean isClearnessSelected(){
		return cle.isSelected();
	}

	public void setClearnessSelected(boolean selected){
		cle.setSelected(selected);
	}

	public int getClearness(){
		JSpinner input = (JSpinner)cle.getInput();
		return (Integer)input.getValue();
	}

	public void setClearness(int clearness){
		JSpinner input = (JSpinner)cle.getInput();
		input.setValue(clearness);
	}

	public boolean isOpeningSelected(){
		return ope.isSelected();
	}

	public void setOpeningSelected(boolean selected){
		ope.setSelected(selected);
	}

	public int getOpening(){
		JSpinner input = (JSpinner)ope.getInput();
		return (Integer)input.getValue();
	}

	public void setOpening(int opening){
		JSpinner input = (JSpinner)ope.getInput();
		input.setValue(opening);
	}

	public boolean isGenderFactorSelected(){
		return gen.isSelected();
	}

	public void setGenderFactorSelected(boolean selected){
		gen.setSelected(selected);
	}

	public int getGenderFactor(){
		JSpinner input = (JSpinner)gen.getInput();
		return (Integer)input.getValue();
	}

	public void setGenderFactor(int genderFactor){
		JSpinner input = (JSpinner)gen.getInput();
		input.setValue(genderFactor);
	}

	public boolean isPortamentoTimingSelected(){
		return por.isSelected();
	}

	public void setPortamentoTimingSelected(boolean selected){
		por.setSelected(selected);
	}

	public int getPortamentoTiming(){
		JSpinner input = (JSpinner)por.getInput();
		return (Integer)input.getValue();
	}

	public void setPortamentoTiming(int portamentoTiming){
		JSpinner input = (JSpinner)por.getInput();
		input.setValue(portamentoTiming);
	}

	public boolean isPitchBendSelected(){
		return pit.isSelected();
	}

	public void setPitchBendSelected(boolean selected){
		pit.setSelected(selected);
	}

	public int getPitchBend(){
		JSpinner input = (JSpinner)pit.getInput();
		return (Integer)input.getValue();
	}

	public void setPitchBend(int pitchBend){
		JSpinner input = (JSpinner)pit.getInput();
		input.setValue(pitchBend);
	}

	public boolean isPitchBendSensSelected(){
		return pbs.isSelected();
	}

	public void setPitchBendSensSelected(boolean selected){
		pbs.setSelected(selected);
	}

	public int getPitchBendSens(){
		JSpinner input = (JSpinner)pbs.getInput();
		return (Integer)input.getValue();
	}

	public void setPitchBendSens(int pitchBendSens){
		JSpinner input = (JSpinner)pbs.getInput();
		input.setValue(pitchBendSens);
	}
}
