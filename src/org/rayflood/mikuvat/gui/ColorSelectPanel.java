package org.rayflood.mikuvat.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ColorSelectPanel extends JPanel{
	private static final long serialVersionUID = -1951434272194219326L;
	private Color color;
	private JButton button;
	private JTextField preview;

	public ColorSelectPanel(){
		this(new Color(181, 162, 123));
	}

	public ColorSelectPanel(Color color){
		preview = new JTextField(8);
		preview.setEditable(false);
		button = new JButton("選択...");
		setColor(color);

		setLayout(new FlowLayout(FlowLayout.LEFT));
		add(button);
		add(preview);

		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setColor(JColorChooser.showDialog(button, "トラック色を選択(VOCALOID Editorには反映されません)", getColor()));
			}
		});
	}

	public Color getColor(){
		return color;
	}

	public void setColor(Color color){
		if(color != null){
			this.color = color;
			preview.setText(color.getRed() + "," + color.getGreen() + "," + color.getBlue());
			preview.setBackground(color);
		}
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		button.setEnabled(enabled);
		preview.setEnabled(enabled);
	}
}
