package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.GroupLayout.Alignment;

public class GridPanel extends JPanel{
	private static final long serialVersionUID = -2643141327456225178L;

	public GridPanel(List<Component> components, int cols){
		this(components.toArray(new Component[0]), cols);
	}

	public GridPanel(Component components[], int cols){
		int rows = (int)StrictMath.ceil((double)components.length / (double)cols);
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		layout.setAutoCreateGaps(true);
		//layout.setAutoCreateContainerGaps(true);
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
		GroupLayout.ParallelGroup vpg[] = new GroupLayout.ParallelGroup[rows];
		GroupLayout.ParallelGroup hpg[] = new GroupLayout.ParallelGroup[cols];
		for(int i = 0; i < components.length; i++){
			int row = i / cols;
			int col = i % cols;
			if(vpg[row] == null){
				vpg[row] = layout.createParallelGroup(Alignment.BASELINE);
			}
			if(hpg[col] == null){
				hpg[col] = layout.createParallelGroup();
			}
			vpg[row].addComponent(components[i]);
			hpg[col].addComponent(components[i]);
		}
		for(int i = 0; i < rows; i++){
			if(vpg[i] != null){
				vGroup.addGroup(vpg[i]);
			}
		}
		for(int i = 0; i < cols; i++){
			if(hpg[i] != null){
				hGroup.addGroup(hpg[i]);
			}
		}
		layout.setVerticalGroup(vGroup);
		layout.setHorizontalGroup(hGroup);
	}
}
