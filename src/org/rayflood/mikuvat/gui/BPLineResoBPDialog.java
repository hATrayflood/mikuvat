package org.rayflood.mikuvat.gui;

import java.awt.Component;
import java.awt.Window;

public class BPLineResoBPDialog extends PropertyDialog{
	private static final long serialVersionUID = 7107855193509346028L;
	private BPLinePanel bpLinePanel;
	private ResoBPLinePanel resoBPLinePanel;

	public BPLineResoBPDialog(Window owner){
		super(owner, "曲線・レゾナンス");
		init();
	}

	public BPLineResoBPDialog(Window owner, boolean modal){
		super(owner, "曲線・レゾナンス", modal);
		init();
	}

	protected void init(){
		bpLinePanel = new BPLinePanel();
		resoBPLinePanel = new ResoBPLinePanel();
		setComponent(new GridPanel(new Component[]{bpLinePanel, resoBPLinePanel}, 1));
		pack();
	}

	public void setSelected(boolean selected){
		bpLinePanel.setSelected(selected);
		resoBPLinePanel.setSelected(selected);
	}

	public BPLinePanel getBPLinePanel(){
		return bpLinePanel;
	}

	public void setBPLinePanel(BPLinePanel bpLinePanel){
		this.bpLinePanel = bpLinePanel;
	}

	public ResoBPLinePanel getResoBPLinePanel(){
		return resoBPLinePanel;
	}

	public void setResoBPLinePanel(ResoBPLinePanel resoBPLinePanel){
		this.resoBPLinePanel = resoBPLinePanel;
	}
}
