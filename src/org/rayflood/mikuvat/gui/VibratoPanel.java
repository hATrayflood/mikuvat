package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.io.vsq.VibratoHandle.CAPTION_PARAMLIST;
import static org.rayflood.mikuvat.io.vsq.VibratoHandle.DEPTH_MAX;
import static org.rayflood.mikuvat.io.vsq.VibratoHandle.DEPTH_MIN;
import static org.rayflood.mikuvat.io.vsq.VibratoHandle.RATE_MAX;
import static org.rayflood.mikuvat.io.vsq.VibratoHandle.RATE_MIN;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.rayflood.mikuvat.io.vsq.VibratoBP;
import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class VibratoPanel extends PropertyPanel{
	private static final long serialVersionUID = 7108047623670408872L;
	public static final String PANEL_NAME = "ビブラートプロパティ";
	private SpinnerNumberModel lengthspin;
	private CheckAndInput lengthPanel;
	private CheckAndInput typePanel;
	private CheckAndInput startDepthPanel;
	private CheckAndInput startRatePanel;
	private VibratoBPTable depthBP;
	private VibratoBPTable rateBP;

	public VibratoPanel(){
		lengthspin = new SpinnerNumberModel(15, 1, 15, 15);
		lengthspin.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				changeVibratoLength(getLength());
			}
		});
		lengthPanel = new CheckAndInput("長さ", new JSpinner(lengthspin));
		typePanel = new CheckAndInput("種類", new JComboBox(CAPTION_PARAMLIST.toArray(new String[0])));
		startDepthPanel = new CheckAndInput("振幅", new JSpinner(new SpinnerNumberModel(0, DEPTH_MIN, DEPTH_MAX, 1)));
		startRatePanel = new CheckAndInput("周期", new JSpinner(new SpinnerNumberModel(0, RATE_MIN, RATE_MAX, 1)));

		Component components[] = new Component[]{
				lengthPanel.getCheck(), lengthPanel.getInput()
				, typePanel.getCheck(), typePanel.getInput()
				, startDepthPanel.getCheck(), startDepthPanel.getInput()
				, startRatePanel.getCheck(), startRatePanel.getInput()
		};
		JPanel p1 = new GridPanel(components, 8);
		depthBP = new VibratoBPTable("振幅");
		rateBP = new VibratoBPTable("周期");
		JPanel p2 = new GridPanel(new Component[]{depthBP, rateBP}, 2);

		setSpacedBorder();
		setLayout(new BorderLayout());
		add(new GridPanel(new Component[]{p1, p2}, 1), BorderLayout.CENTER);
		setSelected(true);
	}

	public String getPanelName(){
		return PANEL_NAME;
	}

	public void setSelected(boolean selected){
		setLengthSelected(selected);
		setTypeSelected(selected);
		setStartDepthSelected(selected);
		setStartRateSelected(selected);
		setDepthBPSelected(selected);
		setRateBPSelected(selected);
	}

	public void setVibratoHandle(int noteLength, VibratoHandle vibrato){
		deleteVibratoBP();
		setNoteLength(noteLength);
		setVibratoHandle(vibrato);
	}

	public void changeNoteLength(int noteLength){
		if(noteLength > 0){
			double lengthrate = (double)getLength() / (double)getNoteLength();
			setNoteLength(noteLength);
			double length = StrictMath.ceil(lengthrate * (double)getNoteLength());
			setLength((int)length);
		}
	}

	public int getNoteLength(){
		return (Integer)lengthspin.getMaximum();
	}

	public void setNoteLength(int noteLength){
		if(noteLength > 0){
			lengthspin.setMaximum(noteLength);
		}
	}

	public VibratoHandle getVibratoHandle(){
		VibratoHandle vibrato = new VibratoHandle();
		if(isLengthSelected()){
			vibrato.setLength(getLength());
			vibrato.setVibratoDelay(getNoteLength() - getLength());
		}
		if(isTypeSelected()){
			vibrato.setVibratoType(getType());
		}
		if(isStartDepthSelected()){
			vibrato.setStartDepth(getStartDepth());
		}
		if(isStartRateSelected()){
			vibrato.setStartRate(getStartRate());
		}
		if(isDepthBPSelected()){
			vibrato.setDepthBP(getDepthBP());
		}
		if(isRateBPSelected()){
			vibrato.setRateBP(getRateBP());
		}
		return vibrato;
	}

	public void setVibratoHandle(VibratoHandle vibrato){
		if(vibrato.hasLength()){
			setLength(vibrato.getLength());
		}
		else{
			setLengthSelected(false);
		}
		if(vibrato.hasIconID()){
			setType(vibrato.getVibratoType());
		}
		else{
			setTypeSelected(false);
		}
		if(vibrato.hasStartDepth()){
			setStartDepth(vibrato.getStartDepth());
		}
		else{
			setStartDepthSelected(false);
		}
		if(vibrato.hasStartRate()){
			setStartRate(vibrato.getStartRate());
		}
		else{
			setStartRateSelected(false);
		}
		if(vibrato.hasDepthBP()){
			setDepthBP(vibrato.getDepthBPL());
		}
		else{
			setDepthBPSelected(false);
		}
		if(vibrato.hasRateBP()){
			setRateBP(vibrato.getRateBPL());
		}
		else{
			setRateBPSelected(false);
		}
	}

	public void changeVibratoLength(int length){
		depthBP.changeVibratoLength(length);
		rateBP.changeVibratoLength(length);
	}

	public void deleteVibratoBP(){
		depthBP.removeVibratoBP();
		rateBP.removeVibratoBP();
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		lengthPanel.setEnabled(enabled);
		typePanel.setEnabled(enabled);
		startDepthPanel.setEnabled(enabled);
		startRatePanel.setEnabled(enabled);
		depthBP.setEnabled(enabled);
		rateBP.setEnabled(enabled);
	}

	public boolean isLengthSelected(){
		return lengthPanel.isSelected();
	}

	public void setLengthSelected(boolean selected){
		lengthPanel.setSelected(selected);
	}

	public int getLength(){
		JSpinner input = (JSpinner)lengthPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setLength(int length){
		JSpinner input = (JSpinner)lengthPanel.getInput();
		input.setValue(length);
		changeVibratoLength(length);
	}

	public boolean isTypeSelected(){
		return typePanel.isSelected();
	}

	public void setTypeSelected(boolean selected){
		typePanel.setSelected(selected);
	}

	public int getType(){
		JComboBox input = (JComboBox)typePanel.getInput();
		return input.getSelectedIndex();
	}

	public void setType(int type){
		JComboBox input = (JComboBox)typePanel.getInput();
		input.setSelectedIndex(type);
	}

	public boolean isStartDepthSelected(){
		return startDepthPanel.isSelected();
	}

	public void setStartDepthSelected(boolean selected){
		startDepthPanel.setSelected(selected);
	}

	public int getStartDepth(){
		JSpinner input = (JSpinner)startDepthPanel.getInput();
		return (Integer)input.getValue();
	}

	public void setStartDepth(int startDepth){
		JSpinner input = (JSpinner)startDepthPanel.getInput();
		input.setValue(startDepth);
	}

	public boolean isStartRateSelected(){
		return startRatePanel.isSelected();
	}

	public void setStartRateSelected(boolean selected){
		startRatePanel.setSelected(selected);
	}

	public int getStartRate(){
		JSpinner input = (JSpinner)startRatePanel.getInput();
		return (Integer)input.getValue();
	}

	public void setStartRate(int startRate){
		JSpinner input = (JSpinner)startRatePanel.getInput();
		input.setValue(startRate);
	}

	public boolean isDepthBPSelected(){
		return depthBP.isSelected();
	}

	public void setDepthBPSelected(boolean selected){
		depthBP.setSelected(selected);
	}

	public List<VibratoBP> getDepthBP(){
		return depthBP.getVibratoBP();
	}

	public void setDepthBP(List<VibratoBP> depthBP){
		this.depthBP.setVibratoBP(depthBP);
	}

	public boolean isRateBPSelected(){
		return rateBP.isSelected();
	}

	public void setRateBPSelected(boolean selected){
		rateBP.setSelected(selected);
	}

	public List<VibratoBP> getRateBP(){
		return rateBP.getVibratoBP();
	}

	public void setRateBP(List<VibratoBP> rateBP){
		this.rateBP.setVibratoBP(rateBP);
	}
}
