package org.rayflood.mikuvat.gui;

import static org.rayflood.mikuvat.gui.VibratoPanel.PANEL_NAME;

import java.awt.Window;
import java.util.List;

import org.rayflood.mikuvat.io.vsq.VibratoBP;
import org.rayflood.mikuvat.io.vsq.VibratoHandle;

public class VibratoDialog extends PropertyDialog{
	private static final long serialVersionUID = 8080988729467666817L;
	private VibratoPanel vibratoPanel;

	public VibratoDialog(Window owner){
		super(owner, PANEL_NAME);
		init();
	}

	public VibratoDialog(Window owner, boolean modal){
		super(owner, PANEL_NAME, modal);
		init();
	}

	protected void init(){
		vibratoPanel = new VibratoPanel();
		setComponent(vibratoPanel);
		pack();
	}

	public void setSelected(boolean selected){
		vibratoPanel.setSelected(selected);
	}

	public VibratoPanel getVibratoPanel(){
		return vibratoPanel;
	}

	public void setVibratoPanel(VibratoPanel vibratoPanel){
		this.vibratoPanel = vibratoPanel;
	}

	public void setVibratoHandle(int noteLength, VibratoHandle vibrato){
		vibratoPanel.setVibratoHandle(noteLength, vibrato);
	}

	public void changeNoteLength(int noteLength){
		vibratoPanel.changeNoteLength(noteLength);
	}

	public int getNoteLength(){
		return vibratoPanel.getNoteLength();
	}

	public void setNoteLength(int noteLength){
		vibratoPanel.setNoteLength(noteLength);
	}

	public VibratoHandle getVibratoHandle(){
		return vibratoPanel.getVibratoHandle();
	}

	public void setVibratoHandle(VibratoHandle vibrato){
		vibratoPanel.setVibratoHandle(vibrato);
	}

	public void changeVibratoLength(int length){
		vibratoPanel.changeVibratoLength(length);
	}

	public void deleteVibratoBP(){
		vibratoPanel.deleteVibratoBP();
	}

	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		vibratoPanel.setEnabled(enabled);
	}

	public boolean isLengthSelected(){
		return vibratoPanel.isLengthSelected();
	}

	public void setLengthSelected(boolean selected){
		vibratoPanel.setLengthSelected(selected);
	}

	public int getLength(){
		return vibratoPanel.getLength();
	}

	public void setLength(int length){
		vibratoPanel.setLength(length);
	}

	public boolean isTypeSelected(){
		return vibratoPanel.isTypeSelected();
	}

	public void setTypeSelected(boolean selected){
		vibratoPanel.setTypeSelected(selected);
	}

	public int getType(){
		return vibratoPanel.getType();
	}

	public void setType(int type){
		vibratoPanel.setType(type);
	}

	public boolean isStartDepthSelected(){
		return vibratoPanel.isStartDepthSelected();
	}

	public void setStartDepthSelected(boolean selected){
		vibratoPanel.setStartDepthSelected(selected);
	}

	public int getStartDepth(){
		return vibratoPanel.getStartDepth();
	}

	public void setStartDepth(int startDepth){
		vibratoPanel.setStartDepth(startDepth);
	}

	public boolean isStartRateSelected(){
		return vibratoPanel.isStartRateSelected();
	}

	public void setStartRateSelected(boolean selected){
		vibratoPanel.setStartRateSelected(selected);
	}

	public int getStartRate(){
		return vibratoPanel.getStartRate();
	}

	public void setStartRate(int startRate){
		vibratoPanel.setStartRate(startRate);
	}

	public boolean isDepthBPSelected(){
		return vibratoPanel.isDepthBPSelected();
	}

	public void setDepthBPSelected(boolean selected){
		vibratoPanel.setDepthBPSelected(selected);
	}

	public List<VibratoBP> getDepthBP(){
		return vibratoPanel.getDepthBP();
	}

	public void setDepthBP(List<VibratoBP> depthBP){
		vibratoPanel.setDepthBP(depthBP);
	}

	public boolean isRateBPSelected(){
		return vibratoPanel.isRateBPSelected();
	}

	public void setRateBPSelected(boolean selected){
		vibratoPanel.setRateBPSelected(selected);
	}

	public List<VibratoBP> getRateBP(){
		return vibratoPanel.getRateBP();
	}

	public void setRateBP(List<VibratoBP> rateBP){
		vibratoPanel.setRateBP(rateBP);
	}
}
