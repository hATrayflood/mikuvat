package org.rayflood.mikuvat.plugin;

import java.util.Properties;

import org.rayflood.mikuvat.gui.MikuvatFrame;

public interface PluginInterface{
	public String getPluginID();

	public String getPluginName();

	public String getPluginDescription();

	public void printHelp();

	public void start(String args[], Properties properties);

	public void start(MikuvatFrame frame, Properties properties);
}
