package org.rayflood.mikuvat.plugin;

import static org.rayflood.mikuvat.Mikuvat.ICON;
import static org.rayflood.mikuvat.Mikuvat.MIKUVAT_CLASSLOADER;
import static org.rayflood.mikuvat.Mikuvat.MIKUVAT_VERSION;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import javax.swing.ImageIcon;

public class PluginLoader{
	public static Map<String, PluginInstance> load() throws Exception{
		List<URL> urls = new ArrayList<URL>();

		File plugindir = new File("plugins");
		File pluginfiles[] = plugindir.listFiles(new JarFileFilter());
		List<String> pluginclasses = new ArrayList<String>();
		for(int i = 0; i < pluginfiles.length; i++){
			JarFile jar = null;
			try{
				jar = new JarFile(pluginfiles[i].getPath());
				Manifest man = jar.getManifest();
				String plugin = man.getMainAttributes().getValue("Mikuvat-Plugin");
				float version = new Float(MIKUVAT_VERSION);
				String mins = man.getMainAttributes().getValue("Mikuvat-Min-Version");
				float min = (mins == null) ? Float.MIN_VALUE: new Float(mins);
				String maxs = man.getMainAttributes().getValue("Mikuvat-Max-Version");
				float max = (maxs == null) ? Float.MAX_VALUE: new Float(maxs);
				if(plugin != null && !plugin.isEmpty() && version >= min && version <= max){
					pluginclasses.add(plugin);
					urls.add(pluginfiles[i].toURI().toURL());
				}
			}
			catch(Exception e){
				System.out.println("プラグイン読み込み失敗! 無視します。" + pluginfiles[i].getPath());
				e.printStackTrace();
			}
			finally{
				if(jar != null){
					jar.close();
				}
			}
		}

		File libdir = new File("lib");
		File libfiles[] = libdir.listFiles(new JarFileFilter());
		for(int i = 0; i < libfiles.length; i++){
			JarFile jar = null;
			try{
				jar = new JarFile(libfiles[i].getPath());
				urls.add(libfiles[i].toURI().toURL());
			}
			catch(Exception e){
				System.out.println("ライブラリ読み込み失敗! 無視します。" + libfiles[i].getPath());
				e.printStackTrace();
			}
			finally{
				if(jar != null){
					jar.close();
				}
			}
		}

		Map<String, PluginInstance> pluginInstance = new LinkedHashMap<String, PluginInstance>();
		ClassLoader classloader = URLClassLoader.newInstance(urls.toArray(new URL[0]));
		for(int i = 0; i < pluginclasses.size(); i++){
			try{
				Class<?> pluginClass = classloader.loadClass(pluginclasses.get(i));
				PluginInterface plugin = (PluginInterface)pluginClass.newInstance();
				String pluginID = plugin.getPluginID();
				if(!pluginInstance.containsKey(pluginID)){
					pluginInstance.put(pluginID, new PluginInstance(
							pluginID, plugin.getPluginName(), plugin.getPluginDescription(), plugin));
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		MIKUVAT_CLASSLOADER = classloader;
		ICON = new ImageIcon(MIKUVAT_CLASSLOADER.getResource("org/rayflood/mikuvat/gui/mikuvat.ico"));

		return pluginInstance;
	}
}
