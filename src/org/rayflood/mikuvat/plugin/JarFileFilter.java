package org.rayflood.mikuvat.plugin;

import java.io.File;
import java.io.FilenameFilter;

public class JarFileFilter implements FilenameFilter{
	public boolean accept(File dir, String name){
		if(name != null){
			int per = name.lastIndexOf(".");
			if(per > 0){
				String suffix = name.substring(per);
				if(".jar".equalsIgnoreCase(suffix)){
					File file = new File(dir, name);
					if(file.isFile()){
						return true;
					}
				}
			}
		}
		return false;
	}
}
