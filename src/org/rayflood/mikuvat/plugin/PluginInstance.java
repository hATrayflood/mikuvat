package org.rayflood.mikuvat.plugin;

public class PluginInstance{
	private String pluginID;
	private String pluginName;
	private String pluginDescription;
	private PluginInterface plugin;

	public PluginInstance(String pluginID, String pluginName, String pluginDescription, PluginInterface plugin){
		this();
		setPluginID(pluginID);
		setPluginName(pluginName);
		setPluginDescription(pluginDescription);
		setPlugin(plugin);
	}

	public PluginInstance(){
		setPluginID("");
		setPluginName("");
		setPluginDescription("");
		setPlugin(null);
	}

	public String getPluginID(){
		return pluginID;
	}

	public void setPluginID(String pluginID){
		this.pluginID = pluginID;
	}

	public String getPluginName(){
		return pluginName;
	}

	public void setPluginName(String pluginName){
		this.pluginName = pluginName;
	}

	public String getPluginDescription(){
		return pluginDescription;
	}

	public void setPluginDescription(String pluginDescription){
		this.pluginDescription = pluginDescription;
	}

	public PluginInterface getPlugin(){
		return plugin;
	}

	public void setPlugin(PluginInterface plugin){
		this.plugin = plugin;
	}
}
