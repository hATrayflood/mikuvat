package org.rayflood.mikuvat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class MikuvatProperties{
	public static final String OPEN_PROPERTY = "open";
	public static final String SAVE_PROPERTY = "save";

	private Properties properties;
	private File propertyfile = new File("mikuvat.properties");

	public MikuvatProperties(){
		loadFromXML();
	}

	public void loadFromXML(){
		try{
			properties = new Properties();
			if(propertyfile.exists() && propertyfile.isFile()){
				properties.loadFromXML(new FileInputStream(propertyfile));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void storeToXML(){
		try{
			properties.storeToXML(new FileOutputStream(propertyfile), null);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public Properties getProperties(){
		return properties;
	}

	public void setProperties(Properties properties){
		this.properties = properties;
	}

	public String getOpenProperty(){
		return properties.getProperty(OPEN_PROPERTY);
	}

	public void setOpenProperty(String path){
		properties.setProperty(OPEN_PROPERTY, path);
	}

	public String getSaveProperty(){
		return properties.getProperty(SAVE_PROPERTY);
	}

	public void setSaveProperty(String path){
		properties.setProperty(SAVE_PROPERTY, path);
	}
}
