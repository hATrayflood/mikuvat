package org.rayflood.mikuvat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

import javax.swing.ImageIcon;

import org.rayflood.mikuvat.gui.MikuvatFrame;
import org.rayflood.mikuvat.plugin.PluginInstance;
import org.rayflood.mikuvat.plugin.PluginInterface;
import org.rayflood.mikuvat.plugin.PluginLoader;

/**
 * ミクバット - VSQ Adjustment Tool 調整の難しいVSQパラメータを、ある程度オートで調整するツールです。
 * 調整機能そのものはプラグインで実装されており、拡張可能です。 プラグインの機能については各プラグインのヘルプを参照してください。
 *
 * コマンド GUIツールを起動します。 java -jar mikuvat.jar このヘルプ画面を表示します。 java -jar mikuvat.jar
 * --help プラグインのヘルプを表示します。 java -jar mikuvat.jar PLUGIN オプションを渡してプラグインを起動します。
 * java -jar mikuvat.jar PLUGIN [OPTION]...
 */
public class Mikuvat{
	public static final String MIKUVAT_APPNAME = "ミクバット";
	public static final String MIKUVAT_VERSION = "0.2";
	public static final String MIKUVAT_TITLE = MIKUVAT_APPNAME + " - ver " + MIKUVAT_VERSION;

	public static ClassLoader MIKUVAT_CLASSLOADER;
	public static ImageIcon ICON;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception{
		Mikuvat mikuvat = new Mikuvat();
		mikuvat.start(args);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public void start(String args[]) throws Exception{
		Map<String, PluginInstance> pluginInstance = PluginLoader.load();
		MikuvatProperties properties = new MikuvatProperties();
		if(args.length > 0){
			if("/?".equalsIgnoreCase(args[0]) || "-h".equalsIgnoreCase(args[0]) || "--help".equalsIgnoreCase(args[0])){
				printTextResource("org/rayflood/mikuvat/mikuvat.txt");
				System.out.println("使用可能プラグイン");
				String keys[] = pluginInstance.keySet().toArray(new String[0]);
				for(int i = 0; i < keys.length; i++){
					PluginInstance plugin = pluginInstance.get(keys[i]);
					System.out.println("\t" + plugin.getPluginID());
				}
			}
			else{
				try{
					System.out.println(MIKUVAT_TITLE + " by rayflood.org\n");
					if(pluginInstance.containsKey(args[0])){
						PluginInterface plugin = pluginInstance.get(args[0]).getPlugin();
						if(args.length > 1){
							plugin.start(args, properties.getProperties());
						}
						else{
							plugin.printHelp();
						}
					}
					else{
						System.out.println("プラグインがないよ！");
					}
				}
				catch(Exception e){
					e.printStackTrace();
					System.out.println("プラグイン起動失敗！");
				}
				finally{
					properties.storeToXML();
				}
			}
		}
		else{
			new MikuvatFrame(pluginInstance, properties);
		}
	}

	/**
	 * @param resource
	 */
	public static void printTextResource(String resource){
		System.out.println(getTextResource(resource));
	}

	/**
	 * @param resource
	 * @return textResource
	 */
	public static String getTextResource(String resource){
		StringBuilder sb = new StringBuilder();
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(
					MIKUVAT_CLASSLOADER.getResourceAsStream(resource), "UTF-8"));
			int len = 65536;
			char cbuf[] = new char[len];
			int red = br.read(cbuf, 0, len);
			for(int i = 1; red != -1; i++){
				sb.append(cbuf, 0, red);
				red = br.read(cbuf, 0, len);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
}
