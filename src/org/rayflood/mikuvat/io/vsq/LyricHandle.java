package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class LyricHandle extends BaseObject{
	private static final long serialVersionUID = 3044157326865723314L;
	private ArrayList<LyricNote> lyricNotes;

	public LyricHandle(String caption, String phonetic, boolean protect){
		this();
		setLyricNote(new LyricNote(caption, phonetic, protect));
	}

	public LyricHandle(LyricNote lyricNote){
		this();
		setLyricNote(lyricNote);
	}

	public LyricHandle(List<String> ps){
		this();
		setHandle(ps);
	}

	public void setHandle(List<String> ps){
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			lyricNotes.add(new LyricNote(pv[1]));
		}
	}

	public List<String> getHandle(){
		List<String> handle = new ArrayList<String>();
		for(int i = 0; i < lyricNotes.size(); i++){
			handle.add("L" + i + "=" + lyricNotes.get(i).getLyric());
		}
		return handle;
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasLyricNotes()){
			sl.add("LyricNote={" + getLyricNote().toString() + "}");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("LyricHandle={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasLyricNotes() && getLyricNote().validate();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		LyricHandle lyricHandle = (LyricHandle)obj;
		return compare(getLyricNote(), lyricHandle.getLyricNote());
	}

	public LyricHandle over(BaseObject obj, boolean fill, boolean has){
		LyricHandle lyricHandle = ((LyricHandle)obj).clone();
		if(lyricHandle.hasLyricNotes()){
			LyricNote lyricNote = getLyricNote();
			setLyricNote(lyricNote.over(lyricHandle.getLyricNote(), fill, has));
		}
		return this;
	}

	public LyricHandle setDefault(){
		deleteLyricNotes();
		setLyricNote(LyricNote.getDefault());
		return this;
	}

	public static LyricHandle getDefault(){
		LyricHandle lyricHandle = new LyricHandle();
		lyricHandle.setDefault();
		return lyricHandle;
	}

	public LyricHandle delete(){
		deleteLyricNotes();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasLyricNotes();
		return !has;
	}

	public LyricHandle(){
		delete();
	}

	public LyricNote getLyricNote(){
		ArrayList<LyricNote> lyricNotes = getLyricNotes();
		LyricNote lyricNote = lyricNotes.isEmpty() ? new LyricNote() : lyricNotes.get(0);
		return lyricNote;
	}

	public void setLyricNote(LyricNote lyricNote){
		if(lyricNote == null || lyricNote.isDeleted()){
			deleteLyricNotes();
			return;
		}
		ArrayList<LyricNote> lyricNotes = getLyricNotes();
		if(lyricNotes.isEmpty()){
			lyricNotes.add(lyricNote);
		}
		else{
			lyricNotes.set(0, lyricNote);
		}
	}

	public ArrayList<LyricNote> getLyricNotes(){
		return lyricNotes;
	}

	public void setLyricNotes(ArrayList<LyricNote> lyricNotes){
		if(lyricNotes == null){
			deleteLyricNotes();
			return;
		}
		this.lyricNotes = lyricNotes;
	}

	public void deleteLyricNotes(){
		setLyricNotes(new ArrayList<LyricNote>());
	}

	public boolean hasLyricNotes(){
		return !lyricNotes.isEmpty();
	}

	public LyricHandle clone(){
		return (LyricHandle)super.clone();
	}

	public static LyricHandle load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (LyricHandle)BaseObject.load(file);
	}

	public static LyricHandle load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (LyricHandle)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static LyricHandle unzip(String file) throws IOException, ClassNotFoundException{
		return (LyricHandle)BaseObject.unzip(file);
	}

	public static LyricHandle unzip(File file) throws IOException, ClassNotFoundException{
		return (LyricHandle)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
