package org.rayflood.mikuvat.io.vsq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.rayflood.mikuvat.io.BaseObject;

public class EOSEvent extends Event{
	private static final long serialVersionUID = 4776243860952379785L;

	public void setEvent(Map<String, String> pvs, Map<String, List<String>> sections){
		// nop
	}

	public void getEvent(long tick, List<String> eventlist, List<List<String>> idlists, List<List<String>> handlelists){
		eventlist.add(tick + "=" + EOS);
	}

	public String toString(){
		return "EOSEvent={" + EOS + "}";
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		if(!(obj instanceof EOSEvent)){
			throw new ClassCastException();
		}
		return 0;
	}

	public EOSEvent over(BaseObject obj, boolean fill, boolean has){
		if(!(obj instanceof EOSEvent)){
			throw new ClassCastException();
		}
		return this;
	}

	public EOSEvent setDefault(){
		return this;
	}

	public EOSEvent delete(){
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public EOSEvent clone(){
		return (EOSEvent)super.clone();
	}

	public static EOSEvent load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (EOSEvent)BaseObject.load(file);
	}

	public static EOSEvent load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (EOSEvent)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static EOSEvent unzip(String file) throws IOException, ClassNotFoundException{
		return (EOSEvent)BaseObject.unzip(file);
	}

	public static EOSEvent unzip(File file) throws IOException, ClassNotFoundException{
		return (EOSEvent)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
