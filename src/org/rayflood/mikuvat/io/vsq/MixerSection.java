package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.getBooleanValue;
import static org.rayflood.mikuvat.Utilities.getStringValue;
import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class MixerSection extends BaseObject{
	private static final long serialVersionUID = 1038661919778326460L;
	public static final String MASTERFEDER = "MasterFeder";
	public static final String MASTERPANPOT = "MasterPanpot";
	public static final String MASTERMUTE = "MasterMute";
	public static final String OUTPUTMODE = "OutputMode";
	public static final String TRACKS = "Tracks";
	public static final String FEDER = "Feder";
	public static final String PANPOT = "Panpot";
	public static final String MUTE = "Mute";
	public static final String SOLO = "Solo";
	public static final String MSPARAMS[] = {FEDER, PANPOT, MUTE, SOLO};

	public static final int FEDER_MAX = 55;
	public static final int FEDER_MIN = -898;
	public static final int PANPOT_MAX = 64;
	public static final int PANPOT_MIN = -64;
	public static final int TRACKS_MAX = 16;
	public static final int TRACKS_MIN = 1;

	public static final List<String> OUTPUTMODE_PARAMLIST = new ArrayList<String>();
	static{
		OUTPUTMODE_PARAMLIST.add("オーディオデバイス");
		OUTPUTMODE_PARAMLIST.add("Rewire(マスター)");
		OUTPUTMODE_PARAMLIST.add("Rewire(各トラック)");
	}

	private Integer masterFeder;
	private Integer masterPanpot;
	private Boolean masterMute;
	private Integer outputMode;
	private Integer tracks;
	private TreeMap<Integer, MixerSetting> mixerSettings;

	public MixerSection(List<String> ps){
		this();
		setSection(ps);
	}

	public void setSection(List<String> ps){
		HashMap<String, String> tms = new HashMap<String, String>();
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			if(MASTERFEDER.equals(pv[0])){
				setMasterFeder(pv[1]);
			}
			else if(MASTERPANPOT.equals(pv[0])){
				setMasterPanpot(pv[1]);
			}
			else if(MASTERMUTE.equals(pv[0])){
				setMasterMute(pv[1]);
			}
			else if(OUTPUTMODE.equals(pv[0])){
				setOutputMode(pv[1]);
			}
			else if(TRACKS.equals(pv[0])){
				setTracks(pv[1]);
			}
			else{
				tms.put(pv[0], pv[1]);
			}
		}

		String keys[] = tms.keySet().toArray(new String[0]);
		for(int i = 0; i < keys.length; i++){
			String param = keys[i];
			String value = tms.get(param);
			setMixerSetting(param, value);
		}
	}

	public List<String> getSection(){
		List<String> ps = new ArrayList<String>();
		ps.add(MASTERFEDER + "=" + getMasterFeder());
		ps.add(MASTERPANPOT + "=" + getMasterPanpot());
		ps.add(MASTERMUTE + "=" + getMasterMuteS());
		ps.add(OUTPUTMODE + "=" + getOutputMode());
		ps.add(TRACKS + "=" + getTracks());
		Integer indexes[] = getIndexes();
		for(int i = 0; i < indexes.length; i++){
			MixerSetting ms = getMixerSetting(indexes[i]);
			ps.add(FEDER + i + "=" + ms.getFeder());
			ps.add(PANPOT + i + "=" + ms.getPanpot());
			ps.add(MUTE + i + "=" + ms.getMuteS());
			ps.add(SOLO + i + "=" + ms.getSoloS());
		}
		return ps;
	}

	public void setMixerSetting(String param, String value){
		for(int i = 0; i < MSPARAMS.length; i++){
			String msparam = MSPARAMS[i];
			if(param.startsWith(msparam)){
				int track = new Integer(param.substring(msparam.length()));
				setMixerSetting(track, msparam, value);
			}
		}
	}

	public void setMixerSetting(int track, String msparam, String value){
		MixerSetting ms = getMixerSetting(track);
		if(FEDER.equals(msparam)){
			ms.setFeder(value);
		}
		if(PANPOT.equals(msparam)){
			ms.setPanpot(value);
		}
		if(MUTE.equals(msparam)){
			ms.setMute(value);
		}
		if(SOLO.equals(msparam)){
			ms.setSolo(value);
		}
		setMixerSetting(track, ms);
	}

	public Integer[] getIndexes(){
		return mixerSettings.keySet().toArray(new Integer[0]);
	}

	public MixerSetting getMixerSetting(int index){
		if(hasMixerSetting(index)){
			return mixerSettings.get(index);
		}
		else{
			return new MixerSetting();
		}
	}

	public void setMixerSetting(int index, MixerSetting mixerSetting){
		if(mixerSetting == null || mixerSetting.isDeleted()){
			deleteMixerSetting(index);
			return;
		}
		mixerSettings.put(index, mixerSetting);
	}

	public MixerSetting deleteMixerSetting(int index){
		return mixerSettings.remove(index);
	}

	public boolean hasMixerSetting(int index){
		return mixerSettings.containsKey(index);
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasMasterFeder()){
			sl.add(MASTERFEDER + "=" + getMasterFeder());
		}
		if(hasMasterPanpot()){
			sl.add(MASTERPANPOT + "=" + getMasterPanpot());
		}
		if(hasMasterMute()){
			sl.add(MASTERMUTE + "=" + getMasterMute());
		}
		if(hasOutputMode()){
			sl.add(OUTPUTMODE + "=" + OUTPUTMODE_PARAMLIST.get(getOutputMode()));
		}
		if(hasTracks()){
			sl.add(TRACKS + "=" + getTracks());
		}
		if(hasMixerSettings()){
			StringBuilder sb = new StringBuilder();
			Integer indexes[] = getIndexes();
			sb.append(indexes[0] + "={" + getMixerSetting(indexes[0]).toString() + "}");
			for(int i = 1; i < indexes.length; i++){
				sb.append("," + indexes[i] + "={" + getMixerSetting(indexes[i]).toString() + "}");
			}
			sl.add("MixerSetting=[" + sb.toString() + "]");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("MixerSection={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasMasterFeder();
		ready &= hasMasterPanpot();
		ready &= hasMasterMute();
		ready &= hasOutputMode();
		ready &= hasTracks();
		Integer indexes[] = getIndexes();
		for(int i = 0; i < indexes.length; i++){
			ready &= getMixerSetting(indexes[i]).validate();
		}
		ready &= (getTracks() == indexes.length);
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		MixerSection mixerSection = (MixerSection)obj;
		int c = compare(getMasterFeder(), mixerSection.getMasterFeder());
		if(c == 0){
			c = compare(getMasterPanpot(), mixerSection.getMasterPanpot());
		}
		if(c == 0){
			c = compare(getMasterMute(), mixerSection.getMasterMute());
		}
		if(c == 0){
			c = compare(getOutputMode(), mixerSection.getOutputMode());
		}
		return c;
	}

	public MixerSection over(BaseObject obj, boolean fill, boolean has){
		MixerSection mixerSection = ((MixerSection)obj).clone();
		if(mixerSection.hasMasterFeder() && (fill || has == hasMasterFeder())){
			setMasterFeder(mixerSection.getMasterFeder());
		}
		if(mixerSection.hasMasterPanpot() && (fill || has == hasMasterPanpot())){
			setMasterPanpot(mixerSection.getMasterPanpot());
		}
		if(mixerSection.hasMasterMute() && (fill || has == hasMasterMute())){
			setMasterMute(mixerSection.getMasterMute());
		}
		if(mixerSection.hasOutputMode() && (fill || has == hasOutputMode())){
			setOutputMode(mixerSection.getOutputMode());
		}
		if(mixerSection.hasMixerSettings() && (fill || has == hasMixerSettings())){
			setMixerSettings(mixerSection.getMixerSettings());
		}
		Integer indexes[] = mixerSection.getIndexes();
		for(int i = 0; i < indexes.length; i++){
			int index = indexes[i];
			if(fill || has == hasMixerSetting(index)){
				MixerSetting mixerSetting = getMixerSetting(index);
				setMixerSetting(index, mixerSetting.over(mixerSection.getMixerSetting(index), fill, has));
			}
		}
		return this;
	}

	public MixerSection setDefault(){
		setMasterFeder(0);
		setMasterPanpot(0);
		setMasterMute(false);
		setOutputMode(0);
		deleteMixerSettings();
		setMixerSetting(0, MixerSetting.getDefault());
		return this;
	}

	public static MixerSection getDefault(){
		MixerSection mixerSection = new MixerSection();
		mixerSection.setDefault();
		return mixerSection;
	}

	public MixerSection delete(){
		deleteMasterFeder();
		deleteMasterPanpot();
		deleteMasterMute();
		deleteOutputMode();
		deleteMixerSettings();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasMasterFeder();
		has |= hasMasterPanpot();
		has |= hasMasterMute();
		has |= hasOutputMode();
		has |= hasMixerSettings();
		return !has;
	}

	public MixerSection(){
		delete();
	}

	public Integer getMasterFeder(){
		return masterFeder;
	}

	public String getMasterFederS(){
		return Integer.toString(getMasterFeder());
	}

	public void setMasterFeder(Integer masterFeder){
		this.masterFeder = adjustMaxMin(masterFeder, FEDER_MAX, FEDER_MIN);
	}

	public void setMasterFeder(String masterFeder){
		setMasterFeder(new Integer(masterFeder));
	}

	public void deleteMasterFeder(){
		masterFeder = Integer.MIN_VALUE;
	}

	public boolean hasMasterFeder(){
		return masterFeder != Integer.MIN_VALUE;
	}

	public Integer getMasterPanpot(){
		return masterPanpot;
	}

	public String getMasterPanpotS(){
		return Integer.toString(getMasterPanpot());
	}

	public void setMasterPanpot(Integer masterPanpot){
		this.masterPanpot = adjustMaxMin(masterPanpot, PANPOT_MAX, PANPOT_MIN);
	}

	public void setMasterPanpot(String masterPanpot){
		setMasterPanpot(new Integer(masterPanpot));
	}

	public void deleteMasterPanpot(){
		masterPanpot = Integer.MIN_VALUE;
	}

	public boolean hasMasterPanpot(){
		return masterPanpot != Integer.MIN_VALUE;
	}

	public Boolean getMasterMute(){
		return masterMute;
	}

	public String getMasterMuteS(){
		return getStringValue(getMasterMute());
	}

	public void setMasterMute(Boolean masterMute){
		this.masterMute = masterMute;
	}

	public void setMasterMute(String masterMute){
		setMasterMute(getBooleanValue(masterMute));
	}

	public void deleteMasterMute(){
		masterMute = null;
	}

	public boolean hasMasterMute(){
		return masterMute != null;
	}

	public Integer getOutputMode(){
		return outputMode;
	}

	public String getOutputModeS(){
		return Integer.toString(getOutputMode());
	}

	public void setOutputMode(Integer outputMode){
		this.outputMode = outputMode;
	}

	public void setOutputMode(String outputMode){
		setOutputMode(new Integer(outputMode));
	}

	public void deleteOutputMode(){
		outputMode = Integer.MIN_VALUE;
	}

	public boolean hasOutputMode(){
		return outputMode != Integer.MIN_VALUE;
	}

	public Integer getTracks(){
		return tracks;
	}

	public String getTracksS(){
		return Integer.toString(getTracks());
	}

	public void setTracks(Integer tracks){
		this.tracks = adjustMaxMin(tracks, TRACKS_MAX, TRACKS_MIN);
	}

	public void setTracks(String tracks){
		setTracks(new Integer(tracks));
	}

	public void deleteTracks(){
		tracks = Integer.MIN_VALUE;
	}

	public boolean hasTracks(){
		return tracks != Integer.MIN_VALUE;
	}

	public TreeMap<Integer, MixerSetting> getMixerSettings(){
		return mixerSettings;
	}

	public void setMixerSettings(TreeMap<Integer, MixerSetting> mixerSettings){
		if(mixerSettings == null){
			deleteMixerSettings();
			return;
		}
		this.mixerSettings = mixerSettings;
		setTracks(mixerSettings.size());
	}

	public void deleteMixerSettings(){
		setMixerSettings(new TreeMap<Integer, MixerSetting>());
		deleteTracks();
	}

	public boolean hasMixerSettings(){
		return !mixerSettings.isEmpty();
	}

	public MixerSection clone(){
		return (MixerSection)super.clone();
	}

	public static MixerSection load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MixerSection)BaseObject.load(file);
	}

	public static MixerSection load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MixerSection)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MixerSection unzip(String file) throws IOException, ClassNotFoundException{
		return (MixerSection)BaseObject.unzip(file);
	}

	public static MixerSection unzip(File file) throws IOException, ClassNotFoundException{
		return (MixerSection)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
