package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.setSectionBracket;
import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class TimeLine extends BaseObject{
	private static final long serialVersionUID = -3486238291169847310L;
	public static final String PITCHBEND = "PitchBendBPList";
	public static final String PITCHBENDSENS = "PitchBendSensBPList";
	public static final String DYNAMICS = "DynamicsBPList";
	public static final String BREATHINESS = "EpRResidualBPList";
	public static final String BRIGHTNESS = "EpRESlopeBPList";
	public static final String CLEARNESS = "EpRESlopeDepthBPList";
	public static final String RESO1FREQ = "Reso1FreqBPList";
	public static final String RESO2FREQ = "Reso2FreqBPList";
	public static final String RESO3FREQ = "Reso3FreqBPList";
	public static final String RESO4FREQ = "Reso4FreqBPList";
	public static final String RESO1BW = "Reso1BWBPList";
	public static final String RESO2BW = "Reso2BWBPList";
	public static final String RESO3BW = "Reso3BWBPList";
	public static final String RESO4BW = "Reso4BWBPList";
	public static final String RESO1AMP = "Reso1AmpBPList";
	public static final String RESO2AMP = "Reso2AmpBPList";
	public static final String RESO3AMP = "Reso3AmpBPList";
	public static final String RESO4AMP = "Reso4AmpBPList";
	public static final String GENDERFACTOR = "GenderFactorBPList";
	public static final String PORTAMENTOTIMING = "PortamentoTimingBPList";
	public static final String OPENING = "OpeningBPList";
	public static final String BPPARAMS[] = {
		PITCHBEND, PITCHBENDSENS, DYNAMICS, BREATHINESS, BRIGHTNESS, CLEARNESS
		, RESO1FREQ, RESO2FREQ, RESO3FREQ, RESO4FREQ, RESO1BW, RESO2BW, RESO3BW, RESO4BW
		, RESO1AMP, RESO2AMP, RESO3AMP, RESO4AMP, GENDERFACTOR, PORTAMENTOTIMING, OPENING};

	private SingerEvent initialSinger;
	private TreeMap<Long, EventBPTime> eventTimes;

	public TimeLine(Map<String, List<String>> sections){
		this();
		setSection(sections);
	}

	public List<String> getSection(){
		List<String> sections = new ArrayList<String>();
		sections.addAll(getEventList());
		for(int i = 0; i < BPPARAMS.length; i++){
			String param = BPPARAMS[i];
			List<String> bplist = getBPList(param);
			if(bplist != null && !bplist.isEmpty()){
				sections.add(setSectionBracket(param));
				sections.addAll(bplist);
			}
		}
		return sections;
	}

	public List<String> getSectionFast(){
		List<String> eventlist = new ArrayList<String>();
		List<List<String>> idlists = new ArrayList<List<String>>();
		List<List<String>> handlelists = new ArrayList<List<String>>();
		List<String> pitchBendList = new ArrayList<String>();
		List<String> pitchBendSensList = new ArrayList<String>();
		List<String> dynamicsList = new ArrayList<String>();
		List<String> breathinessList = new ArrayList<String>();
		List<String> brightnessList = new ArrayList<String>();
		List<String> clearnessList = new ArrayList<String>();
		List<String> reso1FreqList = new ArrayList<String>();
		List<String> reso2FreqList = new ArrayList<String>();
		List<String> reso3FreqList = new ArrayList<String>();
		List<String> reso4FreqList = new ArrayList<String>();
		List<String> reso1BWList = new ArrayList<String>();
		List<String> reso2BWList = new ArrayList<String>();
		List<String> reso3BWList = new ArrayList<String>();
		List<String> reso4BWList = new ArrayList<String>();
		List<String> reso1AmpList = new ArrayList<String>();
		List<String> reso2AmpList = new ArrayList<String>();
		List<String> reso3AmpList = new ArrayList<String>();
		List<String> reso4AmpList = new ArrayList<String>();
		List<String> genderFactorList = new ArrayList<String>();
		List<String> portamentoTimingList = new ArrayList<String>();
		List<String> openingList = new ArrayList<String>();

		getInitialSinger().getEvent(0, eventlist, idlists, handlelists);
		EventBPTime bfet = new EventBPTime();
		Long ticks[] = getTicks();
		for(int i = 0; i < ticks.length; i++){
			long tick = ticks[i];
			EventBPTime et = getEventBPTime(tick);
			if(et.hasEvent()){
				et.getEvent().getEvent(tick, eventlist, idlists, handlelists);
			}

			Integer pitchBend = et.getPitchBend();
			if(et.hasPitchBend() && bfet.getPitchBend() != pitchBend){
				pitchBendList.add(tick + "=" + pitchBend);
				bfet.setPitchBend(pitchBend);
			}
			Integer pitchBendSens = et.getPitchBendSens();
			if(et.hasPitchBendSens() && bfet.getPitchBendSens() != pitchBendSens){
				pitchBendSensList.add(tick + "=" + pitchBendSens);
				bfet.setPitchBendSens(pitchBendSens);
			}
			Integer dynamics = et.getDynamics();
			if(et.hasDynamics() && bfet.getDynamics() != dynamics){
				dynamicsList.add(tick + "=" + dynamics);
				bfet.setDynamics(dynamics);
			}
			Integer breathiness = et.getBreathiness();
			if(et.hasBreathiness() && bfet.getBreathiness() != breathiness){
				breathinessList.add(tick + "=" + breathiness);
				bfet.setBreathiness(breathiness);
			}
			Integer brightness = et.getBrightness();
			if(et.hasBrightness() && bfet.getBrightness() != brightness){
				brightnessList.add(tick + "=" + brightness);
				bfet.setBrightness(brightness);
			}
			Integer clearness = et.getClearness();
			if(et.hasClearness() && bfet.getClearness() != clearness){
				clearnessList.add(tick + "=" + clearness);
				bfet.setClearness(clearness);
			}
			Integer reso1Freq = et.getReso1Freq();
			if(et.hasReso1Freq() && bfet.getReso1Freq() != reso1Freq){
				reso1FreqList.add(tick + "=" + reso1Freq);
				bfet.setReso1Freq(reso1Freq);
			}
			Integer reso2Freq = et.getReso2Freq();
			if(et.hasReso2Freq() && bfet.getReso2Freq() != reso2Freq){
				reso2FreqList.add(tick + "=" + reso2Freq);
				bfet.setReso2Freq(reso2Freq);
			}
			Integer reso3Freq = et.getReso3Freq();
			if(et.hasReso3Freq() && bfet.getReso3Freq() != reso3Freq){
				reso3FreqList.add(tick + "=" + reso3Freq);
				bfet.setReso3Freq(reso3Freq);
			}
			Integer reso4Freq = et.getReso4Freq();
			if(et.hasReso4Freq() && bfet.getReso4Freq() != reso4Freq){
				reso4FreqList.add(tick + "=" + reso4Freq);
				bfet.setReso4Freq(reso4Freq);
			}
			Integer reso1BW = et.getReso1BW();
			if(et.hasReso1BW() && bfet.getReso1BW() != reso1BW){
				reso1BWList.add(tick + "=" + reso1BW);
				bfet.setReso1BW(reso1BW);
			}
			Integer reso2BW = et.getReso2BW();
			if(et.hasReso2BW() && bfet.getReso2BW() != reso2BW){
				reso2BWList.add(tick + "=" + reso2BW);
				bfet.setReso2BW(reso2BW);
			}
			Integer reso3BW = et.getReso3BW();
			if(et.hasReso3BW() && bfet.getReso3BW() != reso3BW){
				reso3BWList.add(tick + "=" + reso3BW);
				bfet.setReso3BW(reso3BW);
			}
			Integer reso4BW = et.getReso4BW();
			if(et.hasReso4BW() && bfet.getReso4BW() != reso4BW){
				reso4BWList.add(tick + "=" + reso4BW);
				bfet.setReso4BW(reso4BW);
			}
			Integer reso1Amp = et.getReso1Amp();
			if(et.hasReso1Amp() && bfet.getReso1Amp() != reso1Amp){
				reso1AmpList.add(tick + "=" + reso1Amp);
				bfet.setReso1Amp(reso1Amp);
			}
			Integer reso2Amp = et.getReso2Amp();
			if(et.hasReso2Amp() && bfet.getReso2Amp() != reso2Amp){
				reso2AmpList.add(tick + "=" + reso2Amp);
				bfet.setReso2Amp(reso2Amp);
			}
			Integer reso3Amp = et.getReso3Amp();
			if(et.hasReso3Amp() && bfet.getReso3Amp() != reso3Amp){
				reso3AmpList.add(tick + "=" + reso3Amp);
				bfet.setReso3Amp(reso3Amp);
			}
			Integer reso4Amp = et.getReso4Amp();
			if(et.hasReso4Amp() && bfet.getReso4Amp() != reso4Amp){
				reso4AmpList.add(tick + "=" + reso4Amp);
				bfet.setReso4Amp(reso4Amp);
			}
			Integer genderFactor = et.getGenderFactor();
			if(et.hasGenderFactor() && bfet.getGenderFactor() != genderFactor){
				genderFactorList.add(tick + "=" + genderFactor);
				bfet.setGenderFactor(genderFactor);
			}
			Integer portamentoTiming = et.getPortamentoTiming();
			if(et.hasPortamentoTiming() && bfet.getPortamentoTiming() != portamentoTiming){
				portamentoTimingList.add(tick + "=" + portamentoTiming);
				bfet.setPortamentoTiming(portamentoTiming);
			}
			Integer opening = et.getOpening();
			if(et.hasOpening() && bfet.getOpening() != opening){
				openingList.add(tick + "=" + opening);
				bfet.setOpening(opening);
			}
		}
		for(int i = 0; i < idlists.size(); i++){
			eventlist.addAll(idlists.get(i));
		}
		for(int i = 0; i < handlelists.size(); i++){
			eventlist.addAll(handlelists.get(i));
		}

		List<String> sections = new ArrayList<String>();
		sections.addAll(eventlist);
		if(!pitchBendList.isEmpty()){
			sections.add(setSectionBracket(PITCHBEND));
			sections.addAll(pitchBendList);
		}
		if(!pitchBendSensList.isEmpty()){
			sections.add(setSectionBracket(PITCHBENDSENS));
			sections.addAll(pitchBendSensList);
		}
		if(!dynamicsList.isEmpty()){
			sections.add(setSectionBracket(DYNAMICS));
			sections.addAll(dynamicsList);
		}
		if(!breathinessList.isEmpty()){
			sections.add(setSectionBracket(BREATHINESS));
			sections.addAll(breathinessList);
		}
		if(!brightnessList.isEmpty()){
			sections.add(setSectionBracket(BRIGHTNESS));
			sections.addAll(brightnessList);
		}
		if(!clearnessList.isEmpty()){
			sections.add(setSectionBracket(CLEARNESS));
			sections.addAll(clearnessList);
		}
		if(!reso1FreqList.isEmpty()){
			sections.add(setSectionBracket(RESO1FREQ));
			sections.addAll(reso1FreqList);
		}
		if(!reso2FreqList.isEmpty()){
			sections.add(setSectionBracket(RESO2FREQ));
			sections.addAll(reso2FreqList);
		}
		if(!reso3FreqList.isEmpty()){
			sections.add(setSectionBracket(RESO3FREQ));
			sections.addAll(reso3FreqList);
		}
		if(!reso4FreqList.isEmpty()){
			sections.add(setSectionBracket(RESO4FREQ));
			sections.addAll(reso4FreqList);
		}
		if(!reso1BWList.isEmpty()){
			sections.add(setSectionBracket(RESO1BW));
			sections.addAll(reso1BWList);
		}
		if(!reso2BWList.isEmpty()){
			sections.add(setSectionBracket(RESO2BW));
			sections.addAll(reso2BWList);
		}
		if(!reso3BWList.isEmpty()){
			sections.add(setSectionBracket(RESO3BW));
			sections.addAll(reso3BWList);
		}
		if(!reso4BWList.isEmpty()){
			sections.add(setSectionBracket(RESO4BW));
			sections.addAll(reso4BWList);
		}
		if(!reso1AmpList.isEmpty()){
			sections.add(setSectionBracket(RESO1AMP));
			sections.addAll(reso1AmpList);
		}
		if(!reso2AmpList.isEmpty()){
			sections.add(setSectionBracket(RESO2AMP));
			sections.addAll(reso2AmpList);
		}
		if(!reso3AmpList.isEmpty()){
			sections.add(setSectionBracket(RESO3AMP));
			sections.addAll(reso3AmpList);
		}
		if(!reso4AmpList.isEmpty()){
			sections.add(setSectionBracket(RESO4AMP));
			sections.addAll(reso4AmpList);
		}
		if(!genderFactorList.isEmpty()){
			sections.add(setSectionBracket(GENDERFACTOR));
			sections.addAll(genderFactorList);
		}
		if(!portamentoTimingList.isEmpty()){
			sections.add(setSectionBracket(PORTAMENTOTIMING));
			sections.addAll(portamentoTimingList);
		}
		if(!openingList.isEmpty()){
			sections.add(setSectionBracket(OPENING));
			sections.addAll(openingList);
		}

		return sections;
	}

	public void setSection(Map<String, List<String>> sections){
		setEventList(sections);
		for(int i = 0; i < BPPARAMS.length; i++){
			String param = BPPARAMS[i];
			if(sections.containsKey(param)){
				setBPList(param, sections.get(param));
			}
		}
	}

	public List<String> getEventList(){
		List<String> eventlist = new ArrayList<String>();
		List<List<String>> idlists = new ArrayList<List<String>>();
		List<List<String>> handlelists = new ArrayList<List<String>>();

		getInitialSinger().getEvent(0, eventlist, idlists, handlelists);
		Long ticks[] = getTicks();
		for(int i = 0; i < ticks.length; i++){
			long tick = ticks[i];
			EventBPTime et = getEventBPTime(tick);
			if(et.hasEvent()){
				et.getEvent().getEvent(tick, eventlist, idlists, handlelists);
			}
		}
		for(int i = 0; i < idlists.size(); i++){
			eventlist.addAll(idlists.get(i));
		}
		for(int i = 0; i < handlelists.size(); i++){
			eventlist.addAll(handlelists.get(i));
		}

		return eventlist;
	}

	public void setEventList(Map<String, List<String>> sections){
		List<String> eventlist = sections.get(VSQFile.EVENTLIST);
		for(int i = 0; i < eventlist.size(); i++){
			String tev[] = splitEqual(eventlist.get(i));
			long tick = new Long(tev[0]);
			String eventid = tev[1];
			EventBPTime et = getEventBPTime(tick);
			Event event = Event.newEvent(eventid, sections);
			et.setEvent(event);
			setEventBPTime(tick, et);
		}
	}

	public List<String> getBPList(String bpparam){
		List<String> bplist = new ArrayList<String>();

		Integer bfbp = null;
		Long ticks[] = getTicks();
		for(int i = 0; i < ticks.length; i++){
			long tick = ticks[i];
			Integer bp = null;
			EventBPTime et = getEventBPTime(tick);
			if(PITCHBEND.equals(bpparam) && et.hasPitchBend()){
				bp = et.getPitchBend();
			}
			else if(PITCHBENDSENS.equals(bpparam) && et.hasPitchBendSens()){
				bp = et.getPitchBendSens();
			}
			else if(DYNAMICS.equals(bpparam) && et.hasDynamics()){
				bp = et.getDynamics();
			}
			else if(BREATHINESS.equals(bpparam) && et.hasBreathiness()){
				bp = et.getBreathiness();
			}
			else if(BRIGHTNESS.equals(bpparam) && et.hasBrightness()){
				bp = et.getBrightness();
			}
			else if(CLEARNESS.equals(bpparam) && et.hasClearness()){
				bp = et.getClearness();
			}
			else if(RESO1FREQ.equals(bpparam) && et.hasReso1Freq()){
				bp = et.getReso1Freq();
			}
			else if(RESO2FREQ.equals(bpparam) && et.hasReso2Freq()){
				bp = et.getReso2Freq();
			}
			else if(RESO3FREQ.equals(bpparam) && et.hasReso3Freq()){
				bp = et.getReso3Freq();
			}
			else if(RESO4FREQ.equals(bpparam) && et.hasReso4Freq()){
				bp = et.getReso4Freq();
			}
			else if(RESO1BW.equals(bpparam) && et.hasReso1BW()){
				bp = et.getReso1BW();
			}
			else if(RESO2BW.equals(bpparam) && et.hasReso2BW()){
				bp = et.getReso2BW();
			}
			else if(RESO3BW.equals(bpparam) && et.hasReso3BW()){
				bp = et.getReso3BW();
			}
			else if(RESO4BW.equals(bpparam) && et.hasReso4BW()){
				bp = et.getReso4BW();
			}
			else if(RESO1AMP.equals(bpparam) && et.hasReso1Amp()){
				bp = et.getReso1Amp();
			}
			else if(RESO2AMP.equals(bpparam) && et.hasReso2Amp()){
				bp = et.getReso2Amp();
			}
			else if(RESO3AMP.equals(bpparam) && et.hasReso3Amp()){
				bp = et.getReso3Amp();
			}
			else if(RESO4AMP.equals(bpparam) && et.hasReso4Amp()){
				bp = et.getReso4Amp();
			}
			else if(GENDERFACTOR.equals(bpparam) && et.hasGenderFactor()){
				bp = et.getGenderFactor();
			}
			else if(PORTAMENTOTIMING.equals(bpparam) && et.hasPortamentoTiming()){
				bp = et.getPortamentoTiming();
			}
			else if(OPENING.equals(bpparam) && et.hasOpening()){
				bp = et.getOpening();
			}

			if(bp != null && bfbp != bp){
				bplist.add(tick + "=" + bp);
				bfbp = bp;
			}
		}

		return bplist;
	}

	public void setBPList(String bpparam, List<String> bplist){
		for(int i = 0; i < bplist.size(); i++){
			String tbp[] = splitEqual(bplist.get(i));
			long tick = new Long(tbp[0]);
			int bp = new Integer(tbp[1]);
			EventBPTime et = getEventBPTime(tick);

			if(PITCHBEND.equals(bpparam)){
				et.setPitchBend(bp);
			}
			else if(PITCHBENDSENS.equals(bpparam)){
				et.setPitchBendSens(bp);
			}
			else if(DYNAMICS.equals(bpparam)){
				et.setDynamics(bp);
			}
			else if(BREATHINESS.equals(bpparam)){
				et.setBreathiness(bp);
			}
			else if(BRIGHTNESS.equals(bpparam)){
				et.setBrightness(bp);
			}
			else if(CLEARNESS.equals(bpparam)){
				et.setClearness(bp);
			}
			else if(RESO1FREQ.equals(bpparam)){
				et.setReso1Freq(bp);
			}
			else if(RESO2FREQ.equals(bpparam)){
				et.setReso2Freq(bp);
			}
			else if(RESO3FREQ.equals(bpparam)){
				et.setReso3Freq(bp);
			}
			else if(RESO4FREQ.equals(bpparam)){
				et.setReso4Freq(bp);
			}
			else if(RESO1BW.equals(bpparam)){
				et.setReso1BW(bp);
			}
			else if(RESO2BW.equals(bpparam)){
				et.setReso2BW(bp);
			}
			else if(RESO3BW.equals(bpparam)){
				et.setReso3BW(bp);
			}
			else if(RESO4BW.equals(bpparam)){
				et.setReso4BW(bp);
			}
			else if(RESO1AMP.equals(bpparam)){
				et.setReso1Amp(bp);
			}
			else if(RESO2AMP.equals(bpparam)){
				et.setReso2Amp(bp);
			}
			else if(RESO3AMP.equals(bpparam)){
				et.setReso3Amp(bp);
			}
			else if(RESO4AMP.equals(bpparam)){
				et.setReso4Amp(bp);
			}
			else if(GENDERFACTOR.equals(bpparam)){
				et.setGenderFactor(bp);
			}
			else if(PORTAMENTOTIMING.equals(bpparam)){
				et.setPortamentoTiming(bp);
			}
			else if(OPENING.equals(bpparam)){
				et.setOpening(bp);
			}
			setEventBPTime(tick, et);
		}
	}

	public Long[] getTicks(){
		return eventTimes.keySet().toArray(new Long[0]);
	}

	public EventBPTime getEventBPTime(long tick){
		if(tick == 0L){
			EventBPTime et = new EventBPTime();
			et.setEvent(getInitialSinger());
			return et;
		}
		else{
			if(hasEventBPTime(tick)){
				return eventTimes.get(tick);
			}
			else{
				return new EventBPTime();
			}
		}
	}

	public void setEventBPTime(long tick, EventBPTime et){
		if(et == null){
			deleteEventBPTime(tick);
			return;
		}
		if(tick == 0L && et.hasEvent() && et.getEvent() instanceof SingerEvent){
			setInitialSinger((SingerEvent)et.getEvent());
		}
		else if(tick != 0L){
			if(et.isDeleted()){
				deleteEventBPTime(tick);
				return;
			}
			eventTimes.put(tick, et);
		}
	}

	public void coverEventBPTime(long tick, EventBPTime et){
		overEventBPTime(tick, et, true, false);
	}

	public void heapEventBPTime(long tick, EventBPTime et){
		overEventBPTime(tick, et, false, true);
	}

	public void layEventBPTime(long tick, EventBPTime et){
		overEventBPTime(tick, et, false, false);
	}

	public void overEventBPTime(long tick, EventBPTime et, boolean fill, boolean has){
		if(tick == 0L && et.hasEvent() && et.getEvent() instanceof SingerEvent){
			overInitialSinger((SingerEvent)et.getEvent(), fill, has);
		}
		else if(tick != 0L){
			EventBPTime eventBPTime = getEventBPTime(tick);
			setEventBPTime(tick, eventBPTime.over(et, fill, has));
		}
	}

	public void overInitialSinger(SingerEvent initialSinger, boolean fill, boolean has){
		SingerEvent is = hasInitialSinger() ? getInitialSinger() : new SingerEvent();
		setInitialSinger(is.over(initialSinger, fill, has));
	}

	public EventBPTime deleteEventBPTime(long tick){
		return eventTimes.remove(tick);
	}

	public boolean hasEventBPTime(long tick){
		return eventTimes.containsKey(tick);
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasInitialSinger()){
			sl.add("InitialSinger={" + getInitialSinger().toString() + "}");
		}
		if(hasEventTimes()){
			StringBuilder sb = new StringBuilder();
			Long ticks[] = getTicks();
			sb.append(ticks[0] + "={" + getEventBPTime(ticks[0]).toString() + "}");
			for(int i = 1; i < ticks.length; i++){
				sb.append("," + ticks[i] + "={" + getEventBPTime(ticks[i]).toString() + "}");
			}
			sl.add("EventTime=[" + sb.toString() + "]");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("TimeLine={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasInitialSinger() && getInitialSinger().validate();
		Long ticks[] = getTicks();
		for(int i = 0; i < ticks.length; i++){
			ready &= getEventBPTime(ticks[i]).validate();
		}
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		TimeLine timeLine = (TimeLine)obj;
		return compare(getInitialSinger(), timeLine.getInitialSinger());
	}

	public TimeLine over(BaseObject obj, boolean fill, boolean has){
		TimeLine timeLine = ((TimeLine)obj).clone();
		if(timeLine.hasInitialSinger()){
			overInitialSinger(timeLine.getInitialSinger(), fill, has);
		}
		Long ticks[] = timeLine.getTicks();
		for(int i = 0; i < ticks.length; i++){
			long tick = ticks[i];
			if(fill || has == hasEventBPTime(tick)){
				overEventBPTime(tick, timeLine.getEventBPTime(tick), fill, has);
			}
		}
		return this;
	}

	public TimeLine setDefault(){
		setInitialSinger(SingerEvent.getDefault());
		deleteEventTimes();
		return this;
	}

	public static TimeLine getDefault(){
		TimeLine timeLine = new TimeLine();
		timeLine.setDefault();
		return timeLine;
	}

	public TimeLine delete(){
		deleteInitialSinger();
		deleteEventTimes();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasInitialSinger();
		has |= hasEventTimes();
		return !has;
	}

	public TimeLine(){
		delete();
	}

	public SingerEvent getInitialSinger(){
		return initialSinger;
	}

	public void setInitialSinger(SingerEvent initialSinger){
		if(initialSinger == null || initialSinger.isDeleted()){
			deleteInitialSinger();
			return;
		}
		this.initialSinger = initialSinger;
	}

	public void deleteInitialSinger(){
		initialSinger = null;
	}

	public boolean hasInitialSinger(){
		return initialSinger != null;
	}

	public TreeMap<Long, EventBPTime> getEventTimes(){
		return eventTimes;
	}

	public void setEventTimes(TreeMap<Long, EventBPTime> eventTimes){
		if(eventTimes == null){
			deleteEventTimes();
			return;
		}
		this.eventTimes = eventTimes;
	}

	public void deleteEventTimes(){
		setEventTimes(new TreeMap<Long, EventBPTime>());
	}

	public boolean hasEventTimes(){
		return !eventTimes.isEmpty();
	}

	public List<String> getPitchBendBPList(){
		return getBPList(PITCHBEND);
	}

	public void setPitchBendBPList(List<String> bplist){
		setBPList(PITCHBEND, bplist);
	}

	public List<String> getPitchBendSensBPList(){
		return getBPList(PITCHBENDSENS);
	}

	public void setPitchBendSensBPList(List<String> bplist){
		setBPList(PITCHBENDSENS, bplist);
	}

	public List<String> getDynamicsBPList(){
		return getBPList(DYNAMICS);
	}

	public void setDynamicsBPList(List<String> bplist){
		setBPList(DYNAMICS, bplist);
	}

	public List<String> getBreathinessBPList(){
		return getBPList(BREATHINESS);
	}

	public void setBreathinessBPList(List<String> bplist){
		setBPList(BREATHINESS, bplist);
	}

	public List<String> getBrightnessBPList(){
		return getBPList(BRIGHTNESS);
	}

	public void setBrightnessBPList(List<String> bplist){
		setBPList(BRIGHTNESS, bplist);
	}

	public List<String> getClearnessBPList(){
		return getBPList(CLEARNESS);
	}

	public void setClearnessBPList(List<String> bplist){
		setBPList(CLEARNESS, bplist);
	}

	public List<String> getReso1FreqBPList(){
		return getBPList(RESO1FREQ);
	}

	public void setReso1FreqBPList(List<String> bplist){
		setBPList(RESO1FREQ, bplist);
	}

	public List<String> getReso2FreqBPList(){
		return getBPList(RESO2FREQ);
	}

	public void setReso2FreqBPList(List<String> bplist){
		setBPList(RESO2FREQ, bplist);
	}

	public List<String> getReso3FreqBPList(){
		return getBPList(RESO3FREQ);
	}

	public void setReso3FreqBPList(List<String> bplist){
		setBPList(RESO3FREQ, bplist);
	}

	public List<String> getReso4FreqBPList(){
		return getBPList(RESO4FREQ);
	}

	public void setReso4FreqBPList(List<String> bplist){
		setBPList(RESO4FREQ, bplist);
	}

	public List<String> getReso1BWBPList(){
		return getBPList(RESO1BW);
	}

	public void setReso1BWBPList(List<String> bplist){
		setBPList(RESO1BW, bplist);
	}

	public List<String> getReso2BWBPList(){
		return getBPList(RESO2BW);
	}

	public void setReso2BWBPList(List<String> bplist){
		setBPList(RESO2BW, bplist);
	}

	public List<String> getReso3BWBPList(){
		return getBPList(RESO3BW);
	}

	public void setReso3BWBPList(List<String> bplist){
		setBPList(RESO3BW, bplist);
	}

	public List<String> getReso4BWBPList(){
		return getBPList(RESO4BW);
	}

	public void setReso4BWBPList(List<String> bplist){
		setBPList(RESO4BW, bplist);
	}

	public List<String> getReso1AmpBPList(){
		return getBPList(RESO1AMP);
	}

	public void setReso1AmpBPList(List<String> bplist){
		setBPList(RESO1AMP, bplist);
	}

	public List<String> getReso2AmpBPList(){
		return getBPList(RESO2AMP);
	}

	public void setReso2AmpBPList(List<String> bplist){
		setBPList(RESO2AMP, bplist);
	}

	public List<String> getReso3AmpBPList(){
		return getBPList(RESO3AMP);
	}

	public void setReso3AmpBPList(List<String> bplist){
		setBPList(RESO3AMP, bplist);
	}

	public List<String> getReso4AmpBPList(){
		return getBPList(RESO4AMP);
	}

	public void setReso4AmpBPList(List<String> bplist){
		setBPList(RESO4AMP, bplist);
	}

	public List<String> getGenderFactorBPList(){
		return getBPList(GENDERFACTOR);
	}

	public void setGenderFactorBPList(List<String> bplist){
		setBPList(GENDERFACTOR, bplist);
	}

	public List<String> getPortamentoTimingBPList(){
		return getBPList(PORTAMENTOTIMING);
	}

	public void setPortamentoTimingBPList(List<String> bplist){
		setBPList(PORTAMENTOTIMING, bplist);
	}

	public List<String> getOpeningBPList(){
		return getBPList(OPENING);
	}

	public void setOpeningBPList(List<String> bplist){
		setBPList(OPENING, bplist);
	}

	public TimeLine clone(){
		return (TimeLine)super.clone();
	}

	public static TimeLine load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (TimeLine)BaseObject.load(file);
	}

	public static TimeLine load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (TimeLine)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static TimeLine unzip(String file) throws IOException, ClassNotFoundException{
		return (TimeLine)BaseObject.unzip(file);
	}

	public static TimeLine unzip(File file) throws IOException, ClassNotFoundException{
		return (TimeLine)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
