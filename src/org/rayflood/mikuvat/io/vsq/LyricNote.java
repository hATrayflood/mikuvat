package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.SPLITCOMMA;
import static org.rayflood.mikuvat.Utilities.SPLITDQUOTE;
import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.getBigDecimalString;
import static org.rayflood.mikuvat.Utilities.getBooleanValue;
import static org.rayflood.mikuvat.Utilities.getStringValue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class LyricNote extends BaseObject{
	private static final long serialVersionUID = -119405351990339359L;
	public static final BigDecimal A_MAX = new BigDecimal("1.0");
	public static final BigDecimal A_MIN = new BigDecimal("0.0");

	private String caption;
	private String phonetic;
	private BigDecimal a;
	private Integer b[];
	private Boolean protect;

	public LyricNote(String caption, String phonetic, Boolean protect){
		this();
		setLyric(caption, phonetic, protect);
	}

	public LyricNote(String s){
		this();
		setLyric(s);
	}

	public void setLyric(String caption, String phonetic, Boolean protect){
		setCaption(caption);
		setPhonetic(phonetic);
		setProtect(protect);
	}

	public void setLyric(String s){
		String qs[] = SPLITDQUOTE.split(s);
		setCaption(qs[1]);
		setPhonetic(qs[3]);
		String cs[] = SPLITCOMMA.split(qs[4]);
		setA(cs[1]);
		Integer b[] = new Integer[cs.length - 3];
		for(int i = 2; i < cs.length - 1; i++){
			b[i - 2] = new Integer(cs[i]);
		}
		setB(b);
		setProtect(cs[cs.length - 1]);
	}

	public String getLyric(){
		String note = "\"" + getCaption() + "\"";
		note += "," + "\"" + getPhonetic() + "\"";
		note += "," + getAS();
		Integer b[] = getB();
		for(int i = 0; i < b.length; i++){
			note += "," + b[i];
		}
		note += "," + getProtectS();
		return note;
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasCaption()){
			sl.add("Caption=" + getCaption());
		}
		if(hasPhonetic()){
			sl.add("Phonetic=" + getPhonetic());
		}
		if(hasA()){
			sl.add("A=" + getAS());
		}
		if(hasB()){
			StringBuilder sb = new StringBuilder();
			sb.append(b[0]);
			for(int i = 1; i < b.length; i++){
				sb.append("," + b[i]);
			}
			sl.add("B=" + sb.toString());
		}
		if(hasProtect()){
			sl.add("Protect=" + getProtectS());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("LyricNote={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasCaption();
		ready &= hasPhonetic();
		ready &= hasA();
		ready &= hasB();
		ready &= hasProtect();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		LyricNote lyricNote = (LyricNote)obj;
		int c = compare(getCaption(), lyricNote.getCaption());
		if(c == 0){
			c = compare(getPhonetic(), lyricNote.getPhonetic());
		}
		if(c == 0){
			c = compare(isProtect(), lyricNote.isProtect());
		}
		return c;
	}

	public LyricNote over(BaseObject obj, boolean fill, boolean has){
		LyricNote lyricNote = ((LyricNote)obj).clone();
		if(lyricNote.hasCaption() && (fill || has == hasCaption())){
			setCaption(lyricNote.getCaption());
		}
		if(lyricNote.hasPhonetic() && (fill || has == hasPhonetic())){
			setPhonetic(lyricNote.getPhonetic());
		}
		if(lyricNote.hasA() && (fill || has == hasA())){
			setA(lyricNote.getA());
		}
		if(lyricNote.hasB() && (fill || has == hasB())){
			setB(lyricNote.getB());
		}
		if(lyricNote.hasProtect() && (fill || has == hasProtect())){
			setProtect(lyricNote.isProtect());
		}
		return this;
	}

	public LyricNote setDefault(){
		setCaption("あ");
		setPhonetic("a");
		setA("0.0");
		setB(new Integer[]{0});
		setProtect(false);
		return this;
	}

	public static LyricNote getDefault(){
		LyricNote lyricNote = new LyricNote();
		lyricNote.setDefault();
		return lyricNote;
	}

	public LyricNote delete(){
		deleteCaption();
		deletePhonetic();
		deleteA();
		deleteB();
		deleteProtect();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasCaption();
		has |= hasPhonetic();
		has |= hasA();
		has |= hasB();
		has |= hasProtect();
		return !has;
	}

	public LyricNote(){
		delete();
	}

	public String getCaption(){
		return caption;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public void deleteCaption(){
		caption = null;
	}

	public boolean hasCaption(){
		return caption != null;
	}

	public String getPhonetic(){
		return phonetic;
	}

	public void setPhonetic(String phonetic){
		this.phonetic = phonetic;
	}

	public void deletePhonetic(){
		phonetic = null;
	}

	public boolean hasPhonetic(){
		return phonetic != null;
	}

	public BigDecimal getA(){
		return a;
	}

	public String getAS(){
		return getBigDecimalString(getA());
	}

	public void setA(BigDecimal a){
		this.a = adjustMaxMin(a, A_MAX, A_MIN);
	}

	public void setA(String a){
		setA(new BigDecimal(a));
	}

	public void deleteA(){
		a = null;
	}

	public boolean hasA(){
		return a != null;
	}

	public Integer[] getB(){
		return b;
	}

	public void setB(Integer[] b){
		this.b = b;
	}

	public void deleteB(){
		b = null;
	}

	public boolean hasB(){
		return b != null;
	}

	public Boolean isProtect(){
		return protect;
	}

	public String getProtectS(){
		return getStringValue(isProtect());
	}

	public void setProtect(Boolean protect){
		this.protect = protect;
	}

	public void setProtect(String protect){
		setProtect(getBooleanValue(protect));
	}

	public void deleteProtect(){
		protect = null;
	}

	public boolean hasProtect(){
		return protect != null;
	}

	public LyricNote clone(){
		return (LyricNote)super.clone();
	}

	public static LyricNote load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (LyricNote)BaseObject.load(file);
	}

	public static LyricNote load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (LyricNote)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static LyricNote unzip(String file) throws IOException, ClassNotFoundException{
		return (LyricNote)BaseObject.unzip(file);
	}

	public static LyricNote unzip(File file) throws IOException, ClassNotFoundException{
		return (LyricNote)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
