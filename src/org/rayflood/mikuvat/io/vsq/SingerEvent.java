package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.setSectionBracket;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class SingerEvent extends Event{
	private static final long serialVersionUID = 6187827984918909739L;

	public static final String ICONHANDLE = "IconHandle";

	private IconHandle iconHandle;

	public SingerEvent(Map<String, String> pvs, Map<String, List<String>> sections){
		setEvent(pvs, sections);
	}

	public void setEvent(Map<String, String> pvs, Map<String, List<String>> sections){
		setIconHandle(new IconHandle(sections.get(pvs.get(ICONHANDLE))));
	}

	public void getEvent(long tick, List<String> eventlist, List<List<String>> idlists, List<List<String>> handlelists){
		String id = getIDString(eventlist.size());
		eventlist.add(tick + "=" + id);

		List<String> ps = new ArrayList<String>();
		ps.add(setSectionBracket(id));
		ps.add(TYPE + "=" + SINGER);

		String ih = getHandleString(handlelists.size());
		ps.add(ICONHANDLE + "=" + ih);
		List<String> ips = new ArrayList<String>();
		ips.add(setSectionBracket(ih));
		ips.addAll(getIconHandle().getHandle());
		handlelists.add(ips);

		idlists.add(ps);
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasIconHandle()){
			sl.add(ICONHANDLE + "={" + getIconHandle().toString() + "}");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("SingerEvent={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasIconHandle() && getIconHandle().validate();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		SingerEvent singerEvent = (SingerEvent)obj;
		return compare(getIconHandle(), singerEvent.getIconHandle());
	}

	public SingerEvent over(BaseObject obj, boolean fill, boolean has){
		SingerEvent singerEvent = ((SingerEvent)obj).clone();
		if(singerEvent.hasIconHandle()){
			IconHandle iconHandle = hasIconHandle() ? getIconHandle() : new IconHandle();
			setIconHandle(iconHandle.over(singerEvent.getIconHandle(), fill, has));
		}
		return this;
	}

	public SingerEvent setDefault(){
		setIconHandle(IconHandle.getDefault());
		return this;
	}

	public static SingerEvent getDefault(){
		SingerEvent singerEvent = new SingerEvent();
		singerEvent.setDefault();
		return singerEvent;
	}

	public SingerEvent delete(){
		deleteIconHandle();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasIconHandle();
		return !has;
	}

	public SingerEvent(){
		delete();
	}

	public IconHandle getIconHandle(){
		return iconHandle;
	}

	public void setIconHandle(IconHandle iconHandle){
		if(iconHandle == null || iconHandle.isDeleted()){
			deleteIconHandle();
			return;
		}
		this.iconHandle = iconHandle;
	}

	public void deleteIconHandle(){
		iconHandle = null;
	}

	public boolean hasIconHandle(){
		return iconHandle != null;
	}

	public SingerEvent clone(){
		return (SingerEvent)super.clone();
	}

	public static SingerEvent load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (SingerEvent)BaseObject.load(file);
	}

	public static SingerEvent load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (SingerEvent)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static SingerEvent unzip(String file) throws IOException, ClassNotFoundException{
		return (SingerEvent)BaseObject.unzip(file);
	}

	public static SingerEvent unzip(File file) throws IOException, ClassNotFoundException{
		return (SingerEvent)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
