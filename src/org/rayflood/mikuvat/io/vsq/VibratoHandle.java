package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.SPLITCOMMA;
import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.getBigDecimalString;
import static org.rayflood.mikuvat.Utilities.getVibratoBPList;
import static org.rayflood.mikuvat.Utilities.getVibratoBPMap;
import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class VibratoHandle extends BaseObject{
	private static final long serialVersionUID = 7575294004927556748L;
	public static final String ICONID = "IconID";
	public static final String IDS = "IDS";
	public static final String ORIGINAL = "Original";
	public static final String CAPTION = "Caption";
	public static final String LENGTH = "Length";
	public static final String STARTDEPTH = "StartDepth";
	public static final String DEPTHBPNUN = "DepthBPNum";
	public static final String DEPTHBPX = "DepthBPX";
	public static final String DEPTHBPY = "DepthBPY";
	public static final String STARTRATE = "StartRate";
	public static final String RATEBPNUM = "RateBPNum";
	public static final String RATEBPX = "RateBPX";
	public static final String RATEBPY = "RateBPY";

	public static final int DEPTH_MAX = 127;
	public static final int DEPTH_MIN = 0;
	public static final int RATE_MAX = 127;
	public static final int RATE_MIN = 0;

	public static final List<String> ICONID_PARAMLIST = new ArrayList<String>();
	public static final List<String> IDS_PARAMLIST = new ArrayList<String>();
	public static final List<String> CAPTION_PARAMLIST = new ArrayList<String>();
	static{
		ICONID_PARAMLIST.add("$04040001");
		IDS_PARAMLIST.add("normal");
		CAPTION_PARAMLIST.add("[Normal] Type 1");

		ICONID_PARAMLIST.add("$04040002");
		IDS_PARAMLIST.add("normal");
		CAPTION_PARAMLIST.add("[Normal] Type 2");

		ICONID_PARAMLIST.add("$04040003");
		IDS_PARAMLIST.add("normal");
		CAPTION_PARAMLIST.add("[Normal] Type 3");

		ICONID_PARAMLIST.add("$04040004");
		IDS_PARAMLIST.add("normal");
		CAPTION_PARAMLIST.add("[Normal] Type 4");

		ICONID_PARAMLIST.add("$04040005");
		IDS_PARAMLIST.add("extreme");
		CAPTION_PARAMLIST.add("[Extreme] Type 1");

		ICONID_PARAMLIST.add("$04040006");
		IDS_PARAMLIST.add("extreme");
		CAPTION_PARAMLIST.add("[Extreme] Type 2");

		ICONID_PARAMLIST.add("$04040007");
		IDS_PARAMLIST.add("extreme");
		CAPTION_PARAMLIST.add("[Extreme] Type 3");

		ICONID_PARAMLIST.add("$04040008");
		IDS_PARAMLIST.add("extreme");
		CAPTION_PARAMLIST.add("[Extreme] Type 4");

		ICONID_PARAMLIST.add("$04040009");
		IDS_PARAMLIST.add("fast");
		CAPTION_PARAMLIST.add("[Fast] Type 1");

		ICONID_PARAMLIST.add("$0404000a");
		IDS_PARAMLIST.add("fast");
		CAPTION_PARAMLIST.add("[Fast] Type 2");

		ICONID_PARAMLIST.add("$0404000b");
		IDS_PARAMLIST.add("fast");
		CAPTION_PARAMLIST.add("[Fast] Type 3");

		ICONID_PARAMLIST.add("$0404000c");
		IDS_PARAMLIST.add("fast");
		CAPTION_PARAMLIST.add("[Fast] Type 4");

		ICONID_PARAMLIST.add("$0404000d");
		IDS_PARAMLIST.add("slight");
		CAPTION_PARAMLIST.add("[Slight] Type 1");

		ICONID_PARAMLIST.add("$0404000e");
		IDS_PARAMLIST.add("slight");
		CAPTION_PARAMLIST.add("[Slight] Type 2");

		ICONID_PARAMLIST.add("$0404000f");
		IDS_PARAMLIST.add("slight");
		CAPTION_PARAMLIST.add("[Slight] Type 3");

		ICONID_PARAMLIST.add("$04040010");
		IDS_PARAMLIST.add("slight");
		CAPTION_PARAMLIST.add("[Slight] Type 4");
	}

	private String iconID;
	private String ids;
	private Integer original;
	private String caption;
	private Integer length;
	private Integer vibratoDelay;
	private Integer startDepth;
	private Integer depthBPNum;
	private TreeMap<BigDecimal, Integer> depthBP;
	private Integer startRate;
	private Integer rateBPNum;
	private TreeMap<BigDecimal, Integer> rateBP;

	public VibratoHandle(List<String> ps){
		this();
		setHandle(ps);
	}

	public void setHandle(List<String> ps){
		String depthBPX = null;
		String depthBPY = null;
		String rateBPX = null;
		String rateBPY = null;

		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			if(ICONID.equals(pv[0])){
				setIconID(pv[1]);
			}
			else if(IDS.equals(pv[0])){
				setIDS(pv[1]);
			}
			else if(ORIGINAL.equals(pv[0])){
				setOriginal(pv[1]);
			}
			else if(CAPTION.equals(pv[0])){
				setCaption(pv[1]);
			}
			else if(LENGTH.equals(pv[0])){
				setLength(pv[1]);
			}
			else if(STARTDEPTH.equals(pv[0])){
				setStartDepth(pv[1]);
			}
			else if(DEPTHBPNUN.equals(pv[0])){
				setDepthBPNum(pv[1]);
			}
			else if(STARTRATE.equals(pv[0])){
				setStartRate(pv[1]);
			}
			else if(RATEBPNUM.equals(pv[0])){
				setRateBPNum(pv[1]);
			}

			else if(DEPTHBPX.equals(pv[0])){
				depthBPX = pv[1];
			}
			else if(DEPTHBPY.equals(pv[0])){
				depthBPY = pv[1];
			}
			else if(RATEBPX.equals(pv[0])){
				rateBPX = pv[1];
			}
			else if(RATEBPY.equals(pv[0])){
				rateBPY = pv[1];
			}
		}

		int depthBPNum = getDepthBPNum();
		if(depthBPNum > 0){
			String dx[] = SPLITCOMMA.split(depthBPX);
			String dy[] = SPLITCOMMA.split(depthBPY);
			TreeMap<BigDecimal, Integer> depthBP = new TreeMap<BigDecimal, Integer>();
			for(int i = 0; i < depthBPNum; i++){
				depthBP.put(new BigDecimal(dx[i]), new Integer(dy[i]));
			}
			setDepthBP(depthBP);
		}

		int rateBPNum = getRateBPNum();
		if(rateBPNum > 0){
			String rx[] = SPLITCOMMA.split(rateBPX);
			String ry[] = SPLITCOMMA.split(rateBPY);
			TreeMap<BigDecimal, Integer> rateBP = new TreeMap<BigDecimal, Integer>();
			for(int i = 0; i < rateBPNum; i++){
				rateBP.put(new BigDecimal(rx[i]), new Integer(ry[i]));
			}
			setRateBP(rateBP);
		}
	}

	public List<String> getHandle(){
		List<String> handle = new ArrayList<String>();
		handle.add(ICONID + "=" + getIconID());
		handle.add(IDS + "=" + getIDS());
		handle.add(ORIGINAL + "=" + getOriginal());
		handle.add(CAPTION + "=" + getCaption());
		handle.add(LENGTH + "=" + getLength());

		handle.add(STARTDEPTH + "=" + getStartDepth());
		handle.add(DEPTHBPNUN + "=" + getDepthBPNum());
		if(hasDepthBP()){
			TreeMap<BigDecimal, Integer> bps = getDepthBP();
			BigDecimal xs[] = bps.keySet().toArray(new BigDecimal[0]);
			StringBuilder xsb = new StringBuilder();
			StringBuilder ysb = new StringBuilder();
			xsb.append(getBigDecimalString(xs[0]));
			ysb.append(bps.get(xs[0]));
			for(int i = 1; i < xs.length; i++){
				xsb.append("," + getBigDecimalString(xs[i]));
				ysb.append("," + bps.get(xs[i]));
			}
			handle.add(DEPTHBPX + "=" + xsb.toString());
			handle.add(DEPTHBPY + "=" + ysb.toString());
		}
		handle.add(STARTRATE + "=" + getStartRate());
		handle.add(RATEBPNUM + "=" + getRateBPNum());
		if(hasRateBP()){
			TreeMap<BigDecimal, Integer> bps = getRateBP();
			BigDecimal xs[] = bps.keySet().toArray(new BigDecimal[0]);
			StringBuilder xsb = new StringBuilder();
			StringBuilder ysb = new StringBuilder();
			xsb.append(getBigDecimalString(xs[0]));
			ysb.append(bps.get(xs[0]));
			for(int i = 1; i < xs.length; i++){
				xsb.append("," + getBigDecimalString(xs[i]));
				ysb.append("," + bps.get(xs[i]));
			}
			handle.add(RATEBPX + "=" + xsb.toString());
			handle.add(RATEBPY + "=" + ysb.toString());
		}
		return handle;
	}

	public int getVibratoType(){
		return VibratoHandle.ICONID_PARAMLIST.indexOf(getIconID());
	}

	public void setVibratoType(String iconID){
		setVibratoType(ICONID_PARAMLIST.indexOf(iconID));
	}

	public void setVibratoType(int index){
		index = adjustMaxMin(index, ICONID_PARAMLIST.size(), 0);
		setIconID(ICONID_PARAMLIST.get(index));
		setIDS(IDS_PARAMLIST.get(index));
		setOriginal(index + 1);
		setCaption(CAPTION_PARAMLIST.get(index));
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasIconID()){
			sl.add(ICONID + "=" + getIconID());
		}
		if(hasIDS()){
			sl.add(IDS + "=" + getIDS());
		}
		if(hasOriginal()){
			sl.add(ORIGINAL + "=" + getOriginal());
		}
		if(hasCaption()){
			sl.add(CAPTION + "=" + getCaption());
		}
		if(hasLength()){
			sl.add(LENGTH + "=" + getLength());
		}
		if(this.hasVibratoDelay()){
			sl.add("VibratoDelay=" + getVibratoDelay());
		}

		if(hasStartDepth()){
			sl.add(STARTDEPTH + "=" + getStartDepth());
		}
		if(hasDepthBPNum()){
			sl.add(DEPTHBPNUN + "=" + getDepthBPNum());
		}
		if(hasDepthBP()){
			TreeMap<BigDecimal, Integer> bps = getDepthBP();
			BigDecimal xs[] = bps.keySet().toArray(new BigDecimal[0]);
			StringBuilder xsb = new StringBuilder();
			StringBuilder ysb = new StringBuilder();
			xsb.append(getBigDecimalString(xs[0]));
			ysb.append(bps.get(xs[0]));
			for(int i = 1; i < xs.length; i++){
				xsb.append("," + getBigDecimalString(xs[i]));
				ysb.append("," + bps.get(xs[i]));
			}
			sl.add(DEPTHBPX + "=" + xsb.toString());
			sl.add(DEPTHBPY + "=" + ysb.toString());
		}
		if(hasStartDepth()){
			sl.add(STARTRATE + "=" + getStartRate());
		}
		if(hasRateBPNum()){
			sl.add(RATEBPNUM + "=" + getRateBPNum());
		}
		if(hasRateBP()){
			TreeMap<BigDecimal, Integer> bps = getRateBP();
			BigDecimal xs[] = bps.keySet().toArray(new BigDecimal[0]);
			StringBuilder xsb = new StringBuilder();
			StringBuilder ysb = new StringBuilder();
			xsb.append(getBigDecimalString(xs[0]));
			ysb.append(bps.get(xs[0]));
			for(int i = 1; i < xs.length; i++){
				xsb.append("," + getBigDecimalString(xs[i]));
				ysb.append("," + bps.get(xs[i]));
			}
			sl.add(RATEBPX + "=" + xsb.toString());
			sl.add(RATEBPY + "=" + ysb.toString());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("VibratoHandle={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasIconID();
		ready &= hasIDS();
		ready &= hasOriginal();
		ready &= hasCaption();
		ready &= hasLength();
		ready &= hasVibratoDelay();
		ready &= hasStartDepth();
		ready &= hasDepthBPNum();
		ready &= (getDepthBPNum() == getDepthBP().size());
		ready &= hasStartRate();
		ready &= hasRateBPNum();
		ready &= (getRateBPNum() == getRateBP().size());
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		VibratoHandle vibratoHandle = (VibratoHandle)obj;
		int c = compare(getIconID(), vibratoHandle.getIconID());
		if(c == 0){
			c = compare(getLength(), vibratoHandle.getLength());
		}
		return c;
	}

	public VibratoHandle over(BaseObject obj, boolean fill, boolean has){
		VibratoHandle vibratoHandle = ((VibratoHandle)obj).clone();
		if(vibratoHandle.hasIconID() && (fill || has == hasIconID())){
			setIconID(vibratoHandle.getIconID());
		}
		if(vibratoHandle.hasIDS() && (fill || has == hasIDS())){
			setIDS(vibratoHandle.getIDS());
		}
		if(vibratoHandle.hasOriginal() && (fill || has == hasOriginal())){
			setOriginal(vibratoHandle.getOriginal());
		}
		if(vibratoHandle.hasCaption() && (fill || has == hasCaption())){
			setCaption(vibratoHandle.getCaption());
		}
		if(vibratoHandle.hasLength() && (fill || has == hasLength())){
			setLength(vibratoHandle.getLength());
		}
		if(vibratoHandle.hasVibratoDelay() && (fill || has == hasVibratoDelay())){
			setVibratoDelay(vibratoHandle.getVibratoDelay());
		}
		if(vibratoHandle.hasStartDepth() && (fill || has == hasStartDepth())){
			setStartDepth(vibratoHandle.getStartDepth());
		}
		if(vibratoHandle.hasDepthBP() && (fill || has == hasDepthBP())){
			setDepthBP(vibratoHandle.getDepthBP());
		}
		if(vibratoHandle.hasStartRate() && (fill || has == hasStartRate())){
			setStartRate(vibratoHandle.getStartRate());
		}
		if(vibratoHandle.hasRateBP() && (fill || has == hasRateBP())){
			setRateBP(vibratoHandle.getRateBP());
		}
		return this;
	}

	public VibratoHandle setDefault(){
		setVibratoType(0);
		setLength(320);
		setVibratoDelay(160);
		setStartDepth(0);
		deleteDepthBP();
		setStartRate(0);
		deleteRateBP();
		return this;
	}

	public static VibratoHandle getDefault(){
		VibratoHandle vibratoHandle = new VibratoHandle();
		vibratoHandle.setDefault();
		return vibratoHandle;
	}

	public VibratoHandle delete(){
		deleteIconID();
		deleteIDS();
		deleteOriginal();
		deleteCaption();
		deleteLength();
		deleteVibratoDelay();
		deleteStartDepth();
		deleteDepthBP();
		deleteStartRate();
		deleteRateBP();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasIconID();
		has |= hasIDS();
		has |= hasOriginal();
		has |= hasCaption();
		has |= hasLength();
		has |= hasVibratoDelay();
		has |= hasStartDepth();
		has |= hasDepthBP();
		has |= hasStartRate();
		has |= hasRateBP();
		return !has;
	}

	public VibratoHandle(){
		delete();
	}

	public String getIconID(){
		return iconID;
	}

	public void setIconID(String iconID){
		this.iconID = iconID;
	}

	public void deleteIconID(){
		iconID = null;
	}

	public boolean hasIconID(){
		return iconID != null;
	}

	public String getIDS(){
		return ids;
	}

	public void setIDS(String ids){
		this.ids = ids;
	}

	public void deleteIDS(){
		ids = null;
	}

	public boolean hasIDS(){
		return ids != null;
	}

	public Integer getOriginal(){
		return original;
	}

	public String getOriginalS(){
		return Integer.toString(getOriginal());
	}

	public void setOriginal(Integer original){
		this.original = original;
	}

	public void setOriginal(String original){
		setOriginal(new Integer(original));
	}

	public void deleteOriginal(){
		original = null;
	}

	public boolean hasOriginal(){
		return original != null;
	}

	public String getCaption(){
		return caption;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public void deleteCaption(){
		caption = null;
	}

	public boolean hasCaption(){
		return caption != null;
	}

	public Integer getLength(){
		return length;
	}

	public String getLengthS(){
		return Integer.toString(getLength());
	}

	public void setLength(Integer length){
		this.length = length;
	}

	public void setLength(String length){
		setLength(new Integer(length));
	}

	public void deleteLength(){
		length = null;
	}

	public boolean hasLength(){
		return length != null;
	}

	public Integer getVibratoDelay(){
		return vibratoDelay;
	}

	public String getVibratoDelayS(){
		return Integer.toString(getVibratoDelay());
	}

	public void setVibratoDelay(Integer vibratoDelay){
		this.vibratoDelay = vibratoDelay;
	}

	public void setVibratoDelay(String vibratoDelay){
		setVibratoDelay(new Integer(vibratoDelay));
	}

	public void deleteVibratoDelay(){
		vibratoDelay = null;
	}

	public boolean hasVibratoDelay(){
		return vibratoDelay != null;
	}

	public Integer getStartDepth(){
		return startDepth;
	}

	public String getStartDepthS(){
		return Integer.toString(getStartDepth());
	}

	public void setStartDepth(Integer startDepth){
		this.startDepth = adjustMaxMin(startDepth, DEPTH_MAX, DEPTH_MIN);
	}

	public void setStartDepth(String startDepth){
		setStartDepth(new Integer(startDepth));
	}

	public void deleteStartDepth(){
		startDepth = null;
	}

	public boolean hasStartDepth(){
		return startDepth != null;
	}

	public Integer getDepthBPNum(){
		return depthBPNum;
	}

	public String getDepthBPNumS(){
		return Integer.toString(getDepthBPNum());
	}

	public void setDepthBPNum(Integer depthBPNum){
		this.depthBPNum = depthBPNum;
	}

	public void setDepthBPNum(String depthBPNum){
		setDepthBPNum(new Integer(depthBPNum));
	}

	public void deleteDepthBPNum(){
		depthBPNum = null;
	}

	public boolean hasDepthBPNum(){
		return depthBPNum != null;
	}

	public TreeMap<BigDecimal, Integer> getDepthBP(){
		return depthBP;
	}

	public List<VibratoBP> getDepthBPL(){
		return getVibratoBPList(getDepthBP());
	}

	public void setDepthBP(TreeMap<BigDecimal, Integer> depthBP){
		if(depthBP == null){
			deleteDepthBP();
			return;
		}
		this.depthBP = depthBP;
		setDepthBPNum(depthBP.size());
	}

	public void setDepthBP(List<VibratoBP> depthBP){
		setDepthBP(getVibratoBPMap(depthBP));
	}

	public void deleteDepthBP(){
		setDepthBP(new TreeMap<BigDecimal, Integer>());
	}

	public boolean hasDepthBP(){
		return !depthBP.isEmpty();
	}

	public Integer getStartRate(){
		return startRate;
	}

	public String getStartRateS(){
		return Integer.toString(getStartRate());
	}

	public void setStartRate(Integer startRate){
		this.startRate = adjustMaxMin(startRate, RATE_MAX, RATE_MIN);
	}

	public void setStartRate(String startRate){
		setStartRate(new Integer(startRate));
	}

	public void deleteStartRate(){
		startRate = null;
	}

	public boolean hasStartRate(){
		return startRate != null;
	}

	public Integer getRateBPNum(){
		return rateBPNum;
	}

	public String getRateBPNumS(){
		return Integer.toString(getRateBPNum());
	}

	public void setRateBPNum(Integer rateBPNum){
		this.rateBPNum = rateBPNum;
	}

	public void setRateBPNum(String rateBPNum){
		setRateBPNum(new Integer(rateBPNum));
	}

	public void deleteRateBPNum(){
		rateBPNum = null;
	}

	public boolean hasRateBPNum(){
		return rateBPNum != null;
	}

	public TreeMap<BigDecimal, Integer> getRateBP(){
		return rateBP;
	}

	public List<VibratoBP> getRateBPL(){
		return getVibratoBPList(getRateBP());
	}

	public void setRateBP(TreeMap<BigDecimal, Integer> rateBP){
		if(rateBP == null){
			deleteRateBP();
			return;
		}
		this.rateBP = rateBP;
		setRateBPNum(rateBP.size());
	}

	public void setRateBP(List<VibratoBP> rateBP){
		setRateBP(getVibratoBPMap(rateBP));
	}

	public void deleteRateBP(){
		setRateBP(new TreeMap<BigDecimal, Integer>());
	}

	public boolean hasRateBP(){
		return !rateBP.isEmpty();
	}

	public VibratoHandle clone(){
		return (VibratoHandle)super.clone();
	}

	public static VibratoHandle load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VibratoHandle)BaseObject.load(file);
	}

	public static VibratoHandle load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VibratoHandle)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static VibratoHandle unzip(String file) throws IOException, ClassNotFoundException{
		return (VibratoHandle)BaseObject.unzip(file);
	}

	public static VibratoHandle unzip(File file) throws IOException, ClassNotFoundException{
		return (VibratoHandle)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
