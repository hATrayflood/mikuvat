package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.SPLITCOMMA;
import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class CommonSection extends BaseObject{
	private static final long serialVersionUID = 8435457526013952633L;
	public static final String VERSION = "Version";
	public static final String NAME = "Name";
	public static final String COLOR = "Color";
	public static final String DYNAMICSMODE = "DynamicsMode";
	public static final String PLAYMODE = "PlayMode";

	private String version;
	private String name;
	private Color color;
	private Integer dynamicsMode;
	private Integer playMode;

	public CommonSection(List<String> ps){
		this();
		setSection(ps);
	}

	public void setSection(List<String> ps){
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			if(VERSION.equals(pv[0])){
				setVersion(pv[1]);
			}
			else if(NAME.equals(pv[0])){
				setName(pv[1]);
			}
			else if(COLOR.equals(pv[0])){
				setColor(pv[1]);
			}
			else if(DYNAMICSMODE.equals(pv[0])){
				setDynamicsMode(pv[1]);
			}
			else if(PLAYMODE.equals(pv[0])){
				setPlayMode(pv[1]);
			}
		}
	}

	public List<String> getSection(){
		List<String> ps = new ArrayList<String>();
		ps.add(VERSION + "=" + getVersion());
		ps.add(NAME + "=" + getName());
		ps.add(COLOR + "=" + getColorS());
		ps.add(DYNAMICSMODE + "=" + getDynamicsMode());
		ps.add(PLAYMODE + "=" + getPlayMode());
		return ps;
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasVersion()){
			sl.add(VERSION + "=" + getVersion());
		}
		if(hasName()){
			sl.add(NAME + "=" + getName());
		}
		if(hasColor()){
			sl.add(COLOR + "=" + getColorS());
		}
		if(hasDynamicsMode()){
			sl.add(DYNAMICSMODE + "=" + getDynamicsMode());
		}
		if(hasPlayMode()){
			sl.add(PLAYMODE + "=" + getPlayMode());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("CommonSection={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasVersion();
		ready &= hasName();
		ready &= hasColor();
		ready &= hasDynamicsMode();
		ready &= hasPlayMode();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		CommonSection commonSection = (CommonSection)obj;
		int c = compare(getName(), commonSection.getName());
		if(c == 0){
			c = compare(getColorS(), commonSection.getColorS());
		}
		return c;
	}

	public CommonSection over(BaseObject obj, boolean fill, boolean has){
		CommonSection commonSection = ((CommonSection)obj).clone();
		if(commonSection.hasVersion() && (fill || has == hasVersion())){
			setVersion(commonSection.getVersion());
		}
		if(commonSection.hasName() && (fill || has == hasName())){
			setName(commonSection.getName());
		}
		if(commonSection.hasColor() && (fill || has == hasColor())){
			setColor(commonSection.getColor());
		}
		if(commonSection.hasDynamicsMode() && (fill || has == hasDynamicsMode())){
			setDynamicsMode(commonSection.getDynamicsMode());
		}
		if(commonSection.hasPlayMode() && (fill || has == hasPlayMode())){
			setPlayMode(commonSection.getPlayMode());
		}
		return this;
	}

	public CommonSection setDefault(){
		setVersion("DSB301");
		setName("Voice1");
		setColor(new Color(181, 162, 123));
		setDynamicsMode(1);
		setPlayMode(1);
		return this;
	}

	public static CommonSection getDefault(){
		CommonSection commonSection = new CommonSection();
		commonSection.setDefault();
		return commonSection;
	}

	public CommonSection delete(){
		deleteVersion();
		deleteName();
		deleteColor();
		deleteDynamicsMode();
		deletePlayMode();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasVersion();
		has |= hasName();
		has |= hasColor();
		has |= hasDynamicsMode();
		has |= hasPlayMode();
		return !has;
	}

	public CommonSection(){
		delete();
	}

	public String getVersion(){
		return version;
	}

	public void setVersion(String version){
		this.version = version;
	}

	public void deleteVersion(){
		version = null;
	}

	public boolean hasVersion(){
		return version != null;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public void deleteName(){
		name = null;
	}

	public boolean hasName(){
		return name != null;
	}

	public Color getColor(){
		return color;
	}

	public String getColorS(){
		Color color = getColor();
		return color.getRed() + "," + color.getGreen() + "," + color.getBlue();
	}

	public void setColor(Color color){
		this.color = color;
	}

	public void setColor(String color){
		String rbg[] = SPLITCOMMA.split(color, 3);
		setColor(new Color(new Integer(rbg[0]), new Integer(rbg[1]), new Integer(rbg[2])));
	}

	public void deleteColor(){
		color = null;
	}

	public boolean hasColor(){
		return color != null;
	}

	public Integer getDynamicsMode(){
		return dynamicsMode;
	}

	public String getDynamicsModeS(){
		return Integer.toString(getDynamicsMode());
	}

	public void setDynamicsMode(Integer dynamicsMode){
		this.dynamicsMode = dynamicsMode;
	}

	public void setDynamicsMode(String dynamicsMode){
		setDynamicsMode(new Integer(dynamicsMode));
	}

	public void deleteDynamicsMode(){
		dynamicsMode = Integer.MIN_VALUE;
	}

	public boolean hasDynamicsMode(){
		return dynamicsMode != Integer.MIN_VALUE;
	}

	public Integer getPlayMode(){
		return playMode;
	}

	public String getPlayModeS(){
		return Integer.toString(getPlayMode());
	}

	public void setPlayMode(Integer playMode){
		this.playMode = playMode;
	}

	public void setPlayMode(String playMode){
		setPlayMode(new Integer(playMode));
	}

	public void deletePlayMode(){
		playMode = null;
	}

	public boolean hasPlayMode(){
		return playMode != null;
	}

	public CommonSection clone(){
		return (CommonSection)super.clone();
	}

	public static CommonSection load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (CommonSection)BaseObject.load(file);
	}

	public static CommonSection load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (CommonSection)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static CommonSection unzip(String file) throws IOException, ClassNotFoundException{
		return (CommonSection)BaseObject.unzip(file);
	}

	public static CommonSection unzip(File file) throws IOException, ClassNotFoundException{
		return (CommonSection)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
