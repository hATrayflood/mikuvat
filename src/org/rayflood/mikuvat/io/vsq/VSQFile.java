package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.setSectionBracket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Sequence;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;
import org.rayflood.mikuvat.io.midi.MidiData;
import org.rayflood.mikuvat.io.midi.MidiFile;
import org.rayflood.mikuvat.io.midi.MidiMeta;
import org.rayflood.mikuvat.io.midi.MidiTrack;

public class VSQFile extends MidiFile{
	private static final long serialVersionUID = -6314152290164983853L;
	public static final Pattern MMHEADER = Pattern.compile("^DM:\\d+:");
	public static final Charset WIN31J = Charset.forName("Windows-31J");
	public static final Pattern SECTIONLINE = Pattern.compile("^\\[.+\\]$");
	public static final int TEXTLENGTH = 119;
	public static final String COMMON = "Common";
	public static final String MASTER = "Master";
	public static final String MIXER = "Mixer";
	public static final String EVENTLIST = "EventList";

	private MasterSection master;
	private MixerSection mixer;
	private ArrayList<VSQTrack> tracks;

	public VSQFile(String vsq) throws IOException, InvalidMidiDataException{
		this();
		readMidiFile(vsq);
		setVSQSequence(getMidiTracks());
	}

	public VSQFile(File vsq) throws IOException, InvalidMidiDataException{
		this();
		readMidiFile(vsq);
		setVSQSequence(getMidiTracks());
	}

	public VSQFile(Sequence sq) throws IOException{
		this();
		setMidiSequence(sq);
		setVSQSequence(getMidiTracks());
	}

	public void readVSQFile(String vsq) throws IOException, InvalidMidiDataException{
		readVSQFile(new File(vsq));
	}

	public void readVSQFile(File vsq) throws IOException, InvalidMidiDataException{
		super.readMidiFile(vsq);
		setVSQSequence(getMidiTracks());
	}

	public void writeVSQFile() throws IOException, InvalidMidiDataException, InvalidBaseObjectException{
		writeVSQFile(getVsqFile());
	}

	public void writeVSQFile(String vsq) throws IOException, InvalidMidiDataException, InvalidBaseObjectException{
		writeVSQFile(new File(vsq));
	}

	public void writeVSQFile(File vsq) throws IOException, InvalidMidiDataException, InvalidBaseObjectException{
		setMidiTracks(getVSQSequence());
		super.writeMidiFile(vsq);
	}

	public Map<String, List<String>> decode(List<MidiData> md) throws IOException{
		List<Byte> v = new ArrayList<Byte>();
		for(int i = 0; i < md.size(); i++){
			MidiData m = md.get(i);
			if(m instanceof MidiMeta){
				byte b[] = ((MidiMeta)m).getData();
				if(b.length == 0){
					continue;
				}
				String mmheader = new String(b);
				String[] ss = MMHEADER.split(mmheader, 2);
				if(ss.length > 1){
					for(int k = mmheader.indexOf(ss[1]); k < b.length; k++){
						if(b[k] != 0x00){
							v.add(b[k]);
						}
					}
				}
			}
		}
		byte b[] = new byte[v.size()];
		for(int i = 0; i < v.size(); i++){
			b[i] = v.get(i);
		}
		String textevent = new String(b, WIN31J);

		Map<String, List<String>> sections = new LinkedHashMap<String, List<String>>();
		BufferedReader br = new BufferedReader(new StringReader(textevent));
		String bf;
		String sn = "";
		List<String> sv = new ArrayList<String>();
		do{
			bf = br.readLine();
			if(bf == null || SECTIONLINE.matcher(bf).matches()){
				if(sn.length() > 0 && !sections.containsKey(sn)){
					sections.put(sn.substring(1, sn.length() - 1), sv);
				}
				sn = bf;
				sv = new ArrayList<String>();
			}
			else if(bf.length() > 0){
				sv.add(bf);
			}
		}while(bf != null);

		return sections;
	}

	public List<MidiData> encode(List<String> trackSections){
		String te = "";
		for(int i = 0; i < trackSections.size(); i++){
			String vts = trackSections.get(i);
			te += vts + "\n";
		}

		List<MidiData> tme = new ArrayList<MidiData>();
		byte b[] = te.getBytes(WIN31J);
		for(int j = 0; j <= b.length / TEXTLENGTH; j++){
			int h = (int)StrictMath.ceil((double)Integer.toString(j).length() / 4.0);
			String mef = "DM:%0" + h * 4 + "d:";
			byte header[] = String.format(mef, j).getBytes(WIN31J);
			List<Byte> mbl = new ArrayList<Byte>();
			for(int k = 0; k < header.length; k++){
				mbl.add(header[k]);
			}
			for(int k = 0; k < TEXTLENGTH; k++){
				int bi = j * TEXTLENGTH + k;
				if(bi >= b.length){
					break;
				}
				mbl.add(b[bi]);
			}
			byte mb[] = new byte[mbl.size()];
			for(int k = 0; k < mbl.size(); k++){
				mb[k] = mbl.get(k);
			}
			tme.add(new MidiMeta(1, mb));
		}

		return tme;
	}

	public void setVSQSequence(ArrayList<MidiTrack> t) throws IOException{
		Map<String, List<String>> sections = decode(t.get(0).getMidiData(0L));
		setMaster(new MasterSection(sections.get(MASTER)));
		setMixer(new MixerSection(sections.get(MIXER)));
		addVSQTrack(new VSQTrack(getMixerSetting(0), sections));
		for(int i = 1; i < t.size(); i++){
			addVSQTrack(new VSQTrack(getMixerSetting(i), decode(t.get(i).getMidiData(0L))));
		}
	}

	public ArrayList<MidiTrack> getVSQSequence() throws InvalidBaseObjectException{
		ArrayList<MidiTrack> t = new ArrayList<MidiTrack>();

		List<List<String>> tsections = getVSQText();
		for(int i = 0; i < getVSQTrackSize(); i++){
			MidiTrack mt = new MidiTrack();
			mt.setMidiData(0L, encode(tsections.get(i)));
			t.add(mt);
		}

		return t;
	}

	public List<List<String>> getVSQText() throws InvalidBaseObjectException{
		validate();

		List<List<String>> tcommons = new ArrayList<List<String>>();
		List<List<String>> ttimelines = new ArrayList<List<String>>();
		for(int i = 0; i < getVSQTrackSize(); i++){
			VSQTrack vt = getVSQTrack(i);
			tcommons.add(vt.getCommon().getSection());
			ttimelines.add(vt.getTimeLine().getSectionFast());
			setMixerSetting(i, vt.getMixer());
		}

		List<List<String>> vsqtext = new ArrayList<List<String>>();
		List<String> sections = new ArrayList<String>();
		sections.add(setSectionBracket(COMMON));
		sections.addAll(tcommons.get(0));
		sections.add(setSectionBracket(MASTER));
		sections.addAll(getMaster().getSection());
		sections.add(setSectionBracket(MIXER));
		sections.addAll(getMixer().getSection());
		sections.add(setSectionBracket(EVENTLIST));
		sections.addAll(ttimelines.get(0));
		vsqtext.add(sections);

		for(int i = 1; i < getVSQTrackSize(); i++){
			sections = new ArrayList<String>();
			sections.add(setSectionBracket(COMMON));
			sections.addAll(tcommons.get(i));
			sections.add(setSectionBracket(EVENTLIST));
			sections.addAll(ttimelines.get(i));
			vsqtext.add(sections);
		}

		return vsqtext;
	}

	public Integer[] getMixerIndexes(){
		return mixer.getIndexes();
	}

	public MixerSetting getMixerSetting(int index){
		return mixer.getMixerSetting(index);
	}

	public void setMixerSetting(int index, MixerSetting mixerSetting){
		if(mixerSetting == null || mixerSetting.isDeleted()){
			deleteMixerSetting(index);
			return;
		}
		mixer.setMixerSetting(index, mixerSetting);
	}

	public MixerSetting deleteMixerSetting(int index){
		return mixer.deleteMixerSetting(index);
	}

	public boolean hasMixerSetting(int index){
		return mixer.hasMixerSetting(index);
	}

	public int getVSQTrackSize(){
		return tracks.size();
	}

	public void addVSQTrack(VSQTrack vt){
		if(vt == null || vt.isDeleted()){
			return;
		}
		tracks.add(vt);
	}

	public void addVSQTrack(int index, VSQTrack vt){
		if(vt == null || vt.isDeleted()){
			return;
		}
		if(index < getVSQTrackSize()){
			tracks.add(index, vt);
		}
		else{
			addVSQTrack(vt);
		}
	}

	public void setVSQTrack(int index, VSQTrack vt){
		if(vt == null || vt.isDeleted()){
			removeVSQTrack(index);
			return;
		}
		if(index < getVSQTrackSize()){
			tracks.set(index, vt);
		}
		else{
			addVSQTrack(vt);
		}
	}

	public VSQTrack getVSQTrack(int index){
		if(index < getVSQTrackSize()){
			return tracks.get(index);
		}
		else{
			return new VSQTrack();
		}
	}

	public VSQTrack removeVSQTrack(int index){
		if(index < getVSQTrackSize()){
			return tracks.remove(index);
		}
		else{
			return null;
		}
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasVsqFile()){
			sl.add("VsqFile=" + getVsqFileS());
		}
		if(hasMaster()){
			sl.add("Master={" + getMaster().toString() + "}");
		}
		if(hasMixer()){
			sl.add("Mixer={" + getMixer().toString() + "}");
		}
		if(hasVSQTracks()){
			StringBuilder sb = new StringBuilder();
			sb.append(getVSQTrack(0).toString());
			for(int i = 1; i < getVSQTrackSize(); i++){
				sb.append("," + getVSQTrack(i).toString());
			}
			sl.add("Tracks=[" + sb.toString() + "]");
		}

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());

		sb.append("VSQFile={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = super.validate();
		ready &= hasMaster() && getMaster().validate();
		ready &= hasMixer() && getMixer().validate();
		for(int i = 0; i < getVSQTrackSize(); i++){
			ready &= getVSQTrack(i).validate();
		}
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public VSQFile over(BaseObject obj, boolean fill, boolean has){
		super.over(obj, fill, has);
		VSQFile vsqFile = ((VSQFile)obj).clone();
		if(vsqFile.hasMaster()){
			MasterSection master = hasMaster() ? getMaster() : new MasterSection();
			setMaster(master.over(vsqFile.getMaster(), fill, has));
		}
		if(vsqFile.hasMixer()){
			MixerSection mixer = hasMixer() ? getMixer() : new MixerSection();
			setMixer(mixer.over(vsqFile.getMixer(), fill, has));
		}
		for(int i = 0; i < vsqFile.getVSQTracks().size(); i++){
			VSQTrack vt = getVSQTrack(i);
			setVSQTrack(i, vt.over(vsqFile.getVSQTrack(i), fill, has));
		}
		return this;
	}

	public VSQFile setDefault(){
		super.setDefault();
		setMaster(MasterSection.getDefault());
		setMixer(MixerSection.getDefault());
		deleteVSQTracks();
		addVSQTrack(VSQTrack.getDefault());
		return this;
	}

	public static VSQFile getDefault(){
		VSQFile vsqFile = new VSQFile();
		vsqFile.setDefault();
		return vsqFile;
	}

	public VSQFile delete(){
		super.delete();
		deleteMaster();
		deleteMixer();
		deleteVSQTracks();
		return this;
	}

	public boolean isDeleted(){
		boolean has = super.isDeleted();
		has |= hasMaster();
		has |= hasMixer();
		has |= hasVSQTracks();
		return !has;
	}

	public VSQFile(){
		super();
		delete();
	}

	public File getVsqFile(){
		return getMidiFile();
	}

	public String getVsqFileS(){
		return getVsqFile().getPath();
	}

	public void setVsqFile(File vsqFile){
		setMidiFile(vsqFile);
	}

	public void setVsqFile(String vsqFile){
		setVsqFile(new File(vsqFile));
	}

	public void deleteVsqFile(){
		deleteMidiFile();
	}

	public boolean hasVsqFile(){
		return hasMidiFile();
	}

	public MasterSection getMaster(){
		return master;
	}

	public void setMaster(MasterSection master){
		if(master == null || master.isDeleted()){
			deleteMaster();
			return;
		}
		this.master = master;
	}

	public void deleteMaster(){
		master = null;
	}

	public boolean hasMaster(){
		return master != null;
	}

	public MixerSection getMixer(){
		return mixer;
	}

	public void setMixer(MixerSection mixer){
		if(mixer == null || mixer.isDeleted()){
			deleteMixer();
			return;
		}
		this.mixer = mixer;
	}

	public void deleteMixer(){
		mixer = null;
	}

	public boolean hasMixer(){
		return mixer != null;
	}

	public ArrayList<VSQTrack> getVSQTracks(){
		return tracks;
	}

	public void setVSQTracks(ArrayList<VSQTrack> tracks){
		if(tracks == null){
			deleteVSQTracks();
			return;
		}
		this.tracks = tracks;
	}

	public void deleteVSQTracks(){
		setVSQTracks(new ArrayList<VSQTrack>());
	}

	public boolean hasVSQTracks(){
		return !tracks.isEmpty();
	}

	public VSQFile clone(){
		return (VSQFile)super.clone();
	}

	public static VSQFile load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VSQFile)BaseObject.load(file);
	}

	public static VSQFile load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VSQFile)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static VSQFile unzip(String file) throws IOException, ClassNotFoundException{
		return (VSQFile)BaseObject.unzip(file);
	}

	public static VSQFile unzip(File file) throws IOException, ClassNotFoundException{
		return (VSQFile)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
