package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.getBigDecimalString;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import org.rayflood.mikuvat.io.BaseObject;

public class VibratoBP extends BaseObject{
	private static final long serialVersionUID = -1542047841867886356L;
	public static final BigDecimal X_MAX = new BigDecimal("1.0");
	public static final BigDecimal X_MIN = new BigDecimal("0.0");
	public static final int Y_MAX = 127;
	public static final int Y_MIN = 0;

	private BigDecimal x;
	private Integer y;

	public VibratoBP(BigDecimal x, Integer y){
		this();
		setX(x);
		setY(y);
	}

	public VibratoBP(String x, String y){
		this();
		setX(x);
		setY(y);
	}

	public String toString(){
		return "VibratoBP={" + "x=" + getXS() + ",y=" + getYS() + "}";
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		VibratoBP bp = (VibratoBP)obj;
		int c = compare(getX(), bp.getX());
		if(c == 0){
			c = compare(getY(), bp.getY());
		}
		return c;
	}

	public VibratoBP over(BaseObject obj, boolean fill, boolean has){
		VibratoBP vibratoBP = ((VibratoBP)obj).clone();
		setX(vibratoBP.getX());
		setY(vibratoBP.getY());
		return this;
	}

	public VibratoBP setDefault(){
		setX("1.0");
		setY(0);
		return this;
	}

	public static VibratoBP getDefault(){
		VibratoBP vibratoBP = new VibratoBP();
		return vibratoBP;
	}

	public VibratoBP delete(){
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public VibratoBP(){
		setDefault();
	}

	public BigDecimal getX(){
		return x;
	}

	public String getXS(){
		return getBigDecimalString(getX());
	}

	public void setX(BigDecimal x){
		this.x = adjustMaxMin(x, X_MAX, X_MIN);
	}

	public void setX(String x){
		setX(new BigDecimal(x));
	}

	public Integer getY(){
		return y;
	}

	public String getYS(){
		return Integer.toString(getY());
	}

	public void setY(Integer y){
		this.y = adjustMaxMin(y, Y_MAX, Y_MIN);
	}

	public void setY(String y){
		setY(new Integer(y));
	}

	public VibratoBP clone(){
		return (VibratoBP)super.clone();
	}

	public static VibratoBP load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VibratoBP)BaseObject.load(file);
	}

	public static VibratoBP load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VibratoBP)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static VibratoBP unzip(String file) throws IOException, ClassNotFoundException{
		return (VibratoBP)BaseObject.unzip(file);
	}

	public static VibratoBP unzip(File file) throws IOException, ClassNotFoundException{
		return (VibratoBP)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
