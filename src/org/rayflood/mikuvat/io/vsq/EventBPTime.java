package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class EventBPTime extends BaseObject{
	private static final long serialVersionUID = 817431318781476681L;
	public static final int PITCHBEND_MAX = 8191;
	public static final int PITCHBEND_MIN = -8192;
	public static final int PITCHBENDSENS_MAX = 24;
	public static final int PITCHBENDSENS_MIN = 0;
	public static final int DYNAMICS_MAX = 127;
	public static final int DYNAMICS_MIN = 0;
	public static final int BREATHINESS_MAX = 127;
	public static final int BREATHINESS_MIN = 0;
	public static final int BRIGHTNESS_MAX = 127;
	public static final int BRIGHTNESS_MIN = 0;
	public static final int CLEARNESS_MAX = 127;
	public static final int CLEARNESS_MIN = 0;
	public static final int RESO1FREQ_MAX = 255;
	public static final int RESO1FREQ_MIN = 0;
	public static final int RESO2FREQ_MAX = 255;
	public static final int RESO2FREQ_MIN = 0;
	public static final int RESO3FREQ_MAX = 255;
	public static final int RESO3FREQ_MIN = 0;
	public static final int RESO4FREQ_MAX = 255;
	public static final int RESO4FREQ_MIN = 0;
	public static final int RESO1BW_MAX = 255;
	public static final int RESO1BW_MIN = 0;
	public static final int RESO2BW_MAX = 255;
	public static final int RESO2BW_MIN = 0;
	public static final int RESO3BW_MAX = 255;
	public static final int RESO3BW_MIN = 0;
	public static final int RESO4BW_MAX = 255;
	public static final int RESO4BW_MIN = 0;
	public static final int RESO1AMP_MAX = 255;
	public static final int RESO1AMP_MIN = 0;
	public static final int RESO2AMP_MAX = 255;
	public static final int RESO2AMP_MIN = 0;
	public static final int RESO3AMP_MAX = 255;
	public static final int RESO3AMP_MIN = 0;
	public static final int RESO4AMP_MAX = 255;
	public static final int RESO4AMP_MIN = 0;
	public static final int GENDERFACTOR_MAX = 127;
	public static final int GENDERFACTOR_MIN = 0;
	public static final int PORTAMENTOTIMING_MAX = 127;
	public static final int PORTAMENTOTIMING_MIN = 0;
	public static final int OPENING_MAX = 127;
	public static final int OPENING_MIN = 0;

	private Event event;
	private Integer pitchBend;
	private Integer pitchBendSens;
	private Integer dynamics;
	private Integer breathiness;
	private Integer brightness;
	private Integer clearness;
	private Integer reso1Freq;
	private Integer reso2Freq;
	private Integer reso3Freq;
	private Integer reso4Freq;
	private Integer reso1BW;
	private Integer reso2BW;
	private Integer reso3BW;
	private Integer reso4BW;
	private Integer reso1Amp;
	private Integer reso2Amp;
	private Integer reso3Amp;
	private Integer reso4Amp;
	private Integer genderFactor;
	private Integer portamentoTiming;
	private Integer opening;

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasEvent()){
			sl.add("Event={" + getEvent().toString() + "}");
		}
		if(hasPitchBend()){
			sl.add("PitchBend=" + getPitchBend());
		}
		if(hasPitchBendSens()){
			sl.add("PitchBendSens=" + getPitchBendSens());
		}
		if(hasDynamics()){
			sl.add("Dynamics=" + getDynamics());
		}
		if(hasBreathiness()){
			sl.add("Breathiness=" + getBreathiness());
		}
		if(hasBrightness()){
			sl.add("Brightness=" + getBrightness());
		}
		if(hasClearness()){
			sl.add("Clearness=" + getClearness());
		}
		if(hasReso1Freq()){
			sl.add("Reso1Freq=" + getReso1Freq());
		}
		if(hasReso2Freq()){
			sl.add("Reso2Freq=" + getReso2Freq());
		}
		if(hasReso3Freq()){
			sl.add("Reso3Freq=" + getReso3Freq());
		}
		if(hasReso4Freq()){
			sl.add("Reso4Freq=" + getReso4Freq());
		}
		if(hasReso1BW()){
			sl.add("Reso1BW=" + getReso1BW());
		}
		if(hasReso2BW()){
			sl.add("Reso2BW=" + getReso2BW());
		}
		if(hasReso3BW()){
			sl.add("Reso3BW=" + getReso3BW());
		}
		if(hasReso4BW()){
			sl.add("Reso4BW=" + getReso4BW());
		}
		if(hasReso1Amp()){
			sl.add("Reso1Amp=" + getReso1Amp());
		}
		if(hasReso2Amp()){
			sl.add("Reso2Amp=" + getReso2Amp());
		}
		if(hasReso3Amp()){
			sl.add("Reso3Amp=" + getReso3Amp());
		}
		if(hasReso4Amp()){
			sl.add("Reso4Amp=" + getReso4Amp());
		}
		if(hasGenderFactor()){
			sl.add("GenderFactor=" + getGenderFactor());
		}
		if(hasPortamentoTiming()){
			sl.add("PortamentoTiming=" + getPortamentoTiming());
		}
		if(hasOpening()){
			sl.add("Opening=" + getOpening());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("EventBPTime={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= !hasEvent() || getEvent().validate();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		EventBPTime eventBPTime = (EventBPTime)obj;
		int c = compare(getDynamics(), eventBPTime.getDynamics());
		if(c == 0){
			c = compare(getBreathiness(), eventBPTime.getBreathiness());
		}
		if(c == 0){
			c = compare(getBrightness(), eventBPTime.getBrightness());
		}
		if(c == 0){
			c = compare(getClearness(), eventBPTime.getClearness());
		}
		if(c == 0){
			c = compare(getOpening(), eventBPTime.getOpening());
		}
		if(c == 0){
			c = compare(getGenderFactor(), eventBPTime.getGenderFactor());
		}
		if(c == 0){
			c = compare(getPortamentoTiming(), eventBPTime.getPortamentoTiming());
		}
		if(c == 0){
			c = compare(getPitchBend(), eventBPTime.getPitchBend());
		}
		if(c == 0){
			c = compare(getPitchBendSens(), eventBPTime.getPitchBendSens());
		}
		return c;
	}

	public void overEvent(Event event, boolean fill, boolean has){
		if(hasEvent()){
			if(getEvent().getClass().getName().equals(event.getClass().getName())){
				getEvent().over(event, fill, has);
			}
		}
		else{
			setEvent((Event)event.clone().delete().over(event, fill, has));
		}
	}

	public EventBPTime over(BaseObject obj, boolean fill, boolean has){
		EventBPTime eventBPTime = ((EventBPTime)obj).clone();
		if(eventBPTime.hasEvent()){
			overEvent(eventBPTime.getEvent(), fill, has);
		}
		if(eventBPTime.hasPitchBend() && (fill || has == hasPitchBend())){
			setPitchBend(eventBPTime.getPitchBend());
		}
		if(eventBPTime.hasPitchBendSens() && (fill || has == hasPitchBendSens())){
			setPitchBendSens(eventBPTime.getPitchBendSens());
		}
		if(eventBPTime.hasDynamics() && (fill || has == hasDynamics())){
			setDynamics(eventBPTime.getDynamics());
		}
		if(eventBPTime.hasBreathiness() && (fill || has == hasBreathiness())){
			setBreathiness(eventBPTime.getBreathiness());
		}
		if(eventBPTime.hasBrightness() && (fill || has == hasBrightness())){
			setBrightness(eventBPTime.getBrightness());
		}
		if(eventBPTime.hasClearness() && (fill || has == hasClearness())){
			setClearness(eventBPTime.getClearness());
		}
		if(eventBPTime.hasReso1Freq() && (fill || has == hasReso1Freq())){
			setReso1Freq(eventBPTime.getReso1Freq());
		}
		if(eventBPTime.hasReso2Freq() && (fill || has == hasReso2Freq())){
			setReso2Freq(eventBPTime.getReso2Freq());
		}
		if(eventBPTime.hasReso3Freq() && (fill || has == hasReso3Freq())){
			setReso3Freq(eventBPTime.getReso3Freq());
		}
		if(eventBPTime.hasReso4Freq() && (fill || has == hasReso4Freq())){
			setReso4Freq(eventBPTime.getReso4Freq());
		}
		if(eventBPTime.hasReso1BW() && (fill || has == hasReso1BW())){
			setReso1BW(eventBPTime.getReso1BW());
		}
		if(eventBPTime.hasReso2BW() && (fill || has == hasReso2BW())){
			setReso2BW(eventBPTime.getReso2BW());
		}
		if(eventBPTime.hasReso3BW() && (fill || has == hasReso3BW())){
			setReso3BW(eventBPTime.getReso3BW());
		}
		if(eventBPTime.hasReso4BW() && (fill || has == hasReso4BW())){
			setReso4BW(eventBPTime.getReso4BW());
		}
		if(eventBPTime.hasReso1Amp() && (fill || has == hasReso1Amp())){
			setReso1Amp(eventBPTime.getReso1Amp());
		}
		if(eventBPTime.hasReso2Amp() && (fill || has == hasReso2Amp())){
			setReso2Amp(eventBPTime.getReso2Amp());
		}
		if(eventBPTime.hasReso3Amp() && (fill || has == hasReso3Amp())){
			setReso3Amp(eventBPTime.getReso3Amp());
		}
		if(eventBPTime.hasReso4Amp() && (fill || has == hasReso4Amp())){
			setReso4Amp(eventBPTime.getReso4Amp());
		}
		if(eventBPTime.hasGenderFactor() && (fill || has == hasGenderFactor())){
			setGenderFactor(eventBPTime.getGenderFactor());
		}
		if(eventBPTime.hasPortamentoTiming() && (fill || has == hasPortamentoTiming())){
			setPortamentoTiming(eventBPTime.getPortamentoTiming());
		}
		if(eventBPTime.hasOpening() && (fill || has == hasOpening())){
			setOpening(eventBPTime.getOpening());
		}
		return this;
	}

	public EventBPTime setDefault(){
		deleteEvent();
		deletePitchBend();
		deletePitchBendSens();
		deleteDynamics();
		deleteBreathiness();
		deleteBrightness();
		deleteClearness();
		deleteReso1Freq();
		deleteReso2Freq();
		deleteReso3Freq();
		deleteReso4Freq();
		deleteReso1BW();
		deleteReso2BW();
		deleteReso3BW();
		deleteReso4BW();
		deleteReso1Amp();
		deleteReso2Amp();
		deleteReso3Amp();
		deleteReso4Amp();
		deleteGenderFactor();
		deletePortamentoTiming();
		deleteOpening();
		return this;
	}

	public static EventBPTime getDefault(){
		EventBPTime eventBPTime = new EventBPTime();
		eventBPTime.setDefault();
		return eventBPTime;
	}

	public EventBPTime delete(){
		deleteEvent();
		deletePitchBend();
		deletePitchBendSens();
		deleteDynamics();
		deleteBreathiness();
		deleteBrightness();
		deleteClearness();
		deleteReso1Freq();
		deleteReso2Freq();
		deleteReso3Freq();
		deleteReso4Freq();
		deleteReso1BW();
		deleteReso2BW();
		deleteReso3BW();
		deleteReso4BW();
		deleteReso1Amp();
		deleteReso2Amp();
		deleteReso3Amp();
		deleteReso4Amp();
		deleteGenderFactor();
		deletePortamentoTiming();
		deleteOpening();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasEvent();
		has |= hasPitchBend();
		has |= hasPitchBendSens();
		has |= hasDynamics();
		has |= hasBreathiness();
		has |= hasBrightness();
		has |= hasClearness();
		has |= hasReso1Freq();
		has |= hasReso2Freq();
		has |= hasReso3Freq();
		has |= hasReso4Freq();
		has |= hasReso1BW();
		has |= hasReso2BW();
		has |= hasReso3BW();
		has |= hasReso4BW();
		has |= hasReso1Amp();
		has |= hasReso2Amp();
		has |= hasReso3Amp();
		has |= hasReso4Amp();
		has |= hasGenderFactor();
		has |= hasPortamentoTiming();
		has |= hasOpening();
		return !has;
	}

	public EventBPTime(){
		delete();
	}

	public Event getEvent(){
		return event;
	}

	public void setEvent(Event event){
		if(event == null || event.isDeleted()){
			deleteEvent();
			return;
		}
		this.event = event;
	}

	public void deleteEvent(){
		event = null;
	}

	public boolean hasEvent(){
		return event != null;
	}

	public Integer getPitchBend(){
		return pitchBend;
	}

	public String getPitchBendS(){
		return Integer.toString(getPitchBend());
	}

	public void setPitchBend(Integer pitchBend){
		this.pitchBend = adjustMaxMin(pitchBend, PITCHBEND_MAX, PITCHBEND_MIN);
	}

	public void setPitchBend(String pitchBend){
		setPitchBend(new Integer(pitchBend));
	}

	public void deletePitchBend(){
		pitchBend = null;
	}

	public boolean hasPitchBend(){
		return pitchBend != null;
	}

	public Integer getPitchBendSens(){
		return pitchBendSens;
	}

	public String getPitchBendSensS(){
		return Integer.toString(getPitchBendSens());
	}

	public void setPitchBendSens(Integer pitchBendSens){
		this.pitchBendSens = adjustMaxMin(pitchBendSens, PITCHBENDSENS_MAX, PITCHBENDSENS_MIN);
	}

	public void setPitchBendSens(String pitchBendSens){
		setPitchBendSens(new Integer(pitchBendSens));
	}

	public void deletePitchBendSens(){
		pitchBendSens = null;
	}

	public boolean hasPitchBendSens(){
		return pitchBendSens != null;
	}

	public Integer getDynamics(){
		return dynamics;
	}

	public String getDynamicsS(){
		return Integer.toString(getDynamics());
	}

	public void setDynamics(Integer dynamics){
		this.dynamics = adjustMaxMin(dynamics, DYNAMICS_MAX, DYNAMICS_MIN);
	}

	public void setDynamics(String dynamics){
		setDynamics(new Integer(dynamics));
	}

	public void deleteDynamics(){
		dynamics = null;
	}

	public boolean hasDynamics(){
		return dynamics != null;
	}

	public Integer getBreathiness(){
		return breathiness;
	}

	public String getBreathinessS(){
		return Integer.toString(getBreathiness());
	}

	public void setBreathiness(Integer breathiness){
		this.breathiness = adjustMaxMin(breathiness, BREATHINESS_MAX, BREATHINESS_MIN);
	}

	public void setBreathiness(String breathiness){
		setBreathiness(new Integer(breathiness));
	}

	public void deleteBreathiness(){
		breathiness = null;
	}

	public boolean hasBreathiness(){
		return breathiness != null;
	}

	public Integer getBrightness(){
		return brightness;
	}

	public String getBrightnessS(){
		return Integer.toString(getBrightness());
	}

	public void setBrightness(Integer brightness){
		this.brightness = adjustMaxMin(brightness, BRIGHTNESS_MAX, BRIGHTNESS_MIN);
	}

	public void setBrightness(String brightness){
		setBrightness(new Integer(brightness));
	}

	public void deleteBrightness(){
		brightness = null;
	}

	public boolean hasBrightness(){
		return brightness != null;
	}

	public Integer getClearness(){
		return clearness;
	}

	public String getClearnessS(){
		return Integer.toString(getClearness());
	}

	public void setClearness(Integer clearness){
		this.clearness = adjustMaxMin(clearness, CLEARNESS_MAX, CLEARNESS_MIN);
	}

	public void setClearness(String clearness){
		setClearness(new Integer(clearness));
	}

	public void deleteClearness(){
		clearness = null;
	}

	public boolean hasClearness(){
		return clearness != null;
	}

	public Integer getReso1Freq(){
		return reso1Freq;
	}

	public String getReso1FreqS(){
		return Integer.toString(getReso1Freq());
	}

	public void setReso1Freq(Integer reso1Freq){
		this.reso1Freq = adjustMaxMin(reso1Freq, RESO1FREQ_MAX, RESO1FREQ_MIN);
	}

	public void setReso1Freq(String reso1Freq){
		setReso1Freq(new Integer(reso1Freq));
	}

	public void deleteReso1Freq(){
		reso1Freq = null;
	}

	public boolean hasReso1Freq(){
		return reso1Freq != null;
	}

	public Integer getReso2Freq(){
		return reso2Freq;
	}

	public String getReso2FreqS(){
		return Integer.toString(getReso2Freq());
	}

	public void setReso2Freq(Integer reso2Freq){
		this.reso2Freq = adjustMaxMin(reso2Freq, RESO2FREQ_MAX, RESO2FREQ_MIN);
	}

	public void setReso2Freq(String reso2Freq){
		setReso2Freq(new Integer(reso2Freq));
	}

	public void deleteReso2Freq(){
		reso2Freq = null;
	}

	public boolean hasReso2Freq(){
		return reso2Freq != null;
	}

	public Integer getReso3Freq(){
		return reso3Freq;
	}

	public String getReso3FreqS(){
		return Integer.toString(getReso3Freq());
	}

	public void setReso3Freq(Integer reso3Freq){
		this.reso3Freq = adjustMaxMin(reso3Freq, RESO3FREQ_MAX, RESO3FREQ_MIN);
	}

	public void setReso3Freq(String reso3Freq){
		setReso3Freq(new Integer(reso3Freq));
	}

	public void deleteReso3Freq(){
		reso3Freq = null;
	}

	public boolean hasReso3Freq(){
		return reso3Freq != null;
	}

	public Integer getReso4Freq(){
		return reso4Freq;
	}

	public String getReso4FreqS(){
		return Integer.toString(getReso4Freq());
	}

	public void setReso4Freq(Integer reso4Freq){
		this.reso4Freq = adjustMaxMin(reso4Freq, RESO4FREQ_MAX, RESO4FREQ_MIN);
	}

	public void setReso4Freq(String reso4Freq){
		setReso4Freq(new Integer(reso4Freq));
	}

	public void deleteReso4Freq(){
		reso4Freq = null;
	}

	public boolean hasReso4Freq(){
		return reso4Freq != null;
	}

	public Integer getReso1BW(){
		return reso1BW;
	}

	public String getReso1BWS(){
		return Integer.toString(getReso1BW());
	}

	public void setReso1BW(Integer reso1BW){
		this.reso1BW = adjustMaxMin(reso1BW, RESO1BW_MAX, RESO1BW_MIN);
	}

	public void setReso1BW(String reso1BW){
		setReso1BW(new Integer(reso1BW));
	}

	public void deleteReso1BW(){
		reso1BW = null;
	}

	public boolean hasReso1BW(){
		return reso1BW != null;
	}

	public Integer getReso2BW(){
		return reso2BW;
	}

	public String getReso2BWS(){
		return Integer.toString(getReso2BW());
	}

	public void setReso2BW(Integer reso2BW){
		this.reso2BW = adjustMaxMin(reso2BW, RESO2BW_MAX, RESO2BW_MIN);
	}

	public void setReso2BW(String reso2BW){
		setReso2BW(new Integer(reso2BW));
	}

	public void deleteReso2BW(){
		reso2BW = null;
	}

	public boolean hasReso2BW(){
		return reso2BW != null;
	}

	public Integer getReso3BW(){
		return reso3BW;
	}

	public String getReso3BWS(){
		return Integer.toString(getReso3BW());
	}

	public void setReso3BW(Integer reso3BW){
		this.reso3BW = adjustMaxMin(reso3BW, RESO3BW_MAX, RESO3BW_MIN);
	}

	public void setReso3BW(String reso3BW){
		setReso3BW(new Integer(reso3BW));
	}

	public void deleteReso3BW(){
		reso3BW = null;
	}

	public boolean hasReso3BW(){
		return reso3BW != null;
	}

	public Integer getReso4BW(){
		return reso4BW;
	}

	public String getReso4BWS(){
		return Integer.toString(getReso4BW());
	}

	public void setReso4BW(Integer reso4BW){
		this.reso4BW = adjustMaxMin(reso4BW, RESO4BW_MAX, RESO4BW_MIN);
	}

	public void setReso4BW(String reso4BW){
		setReso4BW(new Integer(reso4BW));
	}

	public void deleteReso4BW(){
		reso4BW = null;
	}

	public boolean hasReso4BW(){
		return reso4BW != null;
	}

	public Integer getReso1Amp(){
		return reso1Amp;
	}

	public String getReso1AmpS(){
		return Integer.toString(getReso1Amp());
	}

	public void setReso1Amp(Integer reso1Amp){
		this.reso1Amp = adjustMaxMin(reso1Amp, RESO1AMP_MAX, RESO1AMP_MIN);
	}

	public void setReso1Amp(String reso1Amp){
		setReso1Amp(new Integer(reso1Amp));
	}

	public void deleteReso1Amp(){
		reso1Amp = null;
	}

	public boolean hasReso1Amp(){
		return reso1Amp != null;
	}

	public Integer getReso2Amp(){
		return reso2Amp;
	}

	public String getReso2AmpS(){
		return Integer.toString(getReso2Amp());
	}

	public void setReso2Amp(Integer reso2Amp){
		this.reso2Amp = adjustMaxMin(reso2Amp, RESO2AMP_MAX, RESO2AMP_MIN);
	}

	public void setReso2Amp(String reso2Amp){
		setReso2Amp(new Integer(reso2Amp));
	}

	public void deleteReso2Amp(){
		reso2Amp = null;
	}

	public boolean hasReso2Amp(){
		return reso2Amp != null;
	}

	public Integer getReso3Amp(){
		return reso3Amp;
	}

	public String getReso3AmpS(){
		return Integer.toString(getReso3Amp());
	}

	public void setReso3Amp(Integer reso3Amp){
		this.reso3Amp = adjustMaxMin(reso3Amp, RESO3AMP_MAX, RESO3AMP_MIN);
	}

	public void setReso3Amp(String reso3Amp){
		setReso3Amp(new Integer(reso3Amp));
	}

	public void deleteReso3Amp(){
		reso3Amp = null;
	}

	public boolean hasReso3Amp(){
		return reso3Amp != null;
	}

	public Integer getReso4Amp(){
		return reso4Amp;
	}

	public String getReso4AmpS(){
		return Integer.toString(getReso4Amp());
	}

	public void setReso4Amp(Integer reso4Amp){
		this.reso4Amp = adjustMaxMin(reso4Amp, RESO4AMP_MAX, RESO4AMP_MIN);
	}

	public void setReso4Amp(String reso4Amp){
		setReso4Amp(new Integer(reso4Amp));
	}

	public void deleteReso4Amp(){
		reso4Amp = null;
	}

	public boolean hasReso4Amp(){
		return reso4Amp != null;
	}

	public Integer getGenderFactor(){
		return genderFactor;
	}

	public String getGenderFactorS(){
		return Integer.toString(getGenderFactor());
	}

	public void setGenderFactor(Integer genderFactor){
		this.genderFactor = adjustMaxMin(genderFactor, GENDERFACTOR_MAX, GENDERFACTOR_MIN);
	}

	public void setGenderFactor(String genderFactor){
		setGenderFactor(new Integer(genderFactor));
	}

	public void deleteGenderFactor(){
		genderFactor = null;
	}

	public boolean hasGenderFactor(){
		return genderFactor != null;
	}

	public Integer getPortamentoTiming(){
		return portamentoTiming;
	}

	public String getPortamentoTimingS(){
		return Integer.toString(getPortamentoTiming());
	}

	public void setPortamentoTiming(Integer portamentoTiming){
		this.portamentoTiming = adjustMaxMin(portamentoTiming, PORTAMENTOTIMING_MAX, PORTAMENTOTIMING_MIN);
	}

	public void setPortamentoTiming(String portamentoTiming){
		setPortamentoTiming(new Integer(portamentoTiming));
	}

	public void deletePortamentoTiming(){
		portamentoTiming = null;
	}

	public boolean hasPortamentoTiming(){
		return portamentoTiming != null;
	}

	public Integer getOpening(){
		return opening;
	}

	public String getOpeningS(){
		return Integer.toString(getOpening());
	}

	public void setOpening(Integer opening){
		this.opening = adjustMaxMin(opening, OPENING_MAX, OPENING_MIN);
	}

	public void setOpening(String opening){
		setOpening(new Integer(opening));
	}

	public void deleteOpening(){
		opening = null;
	}

	public boolean hasOpening(){
		return opening != null;
	}

	public EventBPTime clone(){
		return (EventBPTime)super.clone();
	}

	public static EventBPTime load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (EventBPTime)BaseObject.load(file);
	}

	public static EventBPTime load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (EventBPTime)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static EventBPTime unzip(String file) throws IOException, ClassNotFoundException{
		return (EventBPTime)BaseObject.unzip(file);
	}

	public static EventBPTime unzip(File file) throws IOException, ClassNotFoundException{
		return (EventBPTime)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
