package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class IconHandle extends BaseObject{
	private static final long serialVersionUID = 6787028875195143543L;
	public static final String ICONID = "IconID";
	public static final String IDS = "IDS";
	public static final String ORIGINAL = "Original";
	public static final String CAPTION = "Caption";
	public static final String LENGTH = "Length";
	public static final String LANGUAGE = "Language";
	public static final String PROGRAM = "Program";

	private String iconID;
	private String ids;
	private Integer original;
	private String caption;
	private Integer length;
	private Integer language;
	private Integer program;

	public IconHandle(List<String> ps){
		this();
		setHandle(ps);
	}

	public void setHandle(List<String> ps){
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			if(ICONID.equals(pv[0])){
				setIconID(pv[1]);
			}
			else if(IDS.equals(pv[0])){
				setIDS(pv[1]);
			}
			else if(ORIGINAL.equals(pv[0])){
				setOriginal(pv[1]);
			}
			else if(CAPTION.equals(pv[0])){
				setCaption(pv[1]);
			}
			else if(LENGTH.equals(pv[0])){
				setLength(pv[1]);
			}
			else if(LANGUAGE.equals(pv[0])){
				setLanguage(pv[1]);
			}
			else if(PROGRAM.equals(pv[0])){
				setProgram(pv[1]);
			}
		}
	}

	public List<String> getHandle(){
		List<String> handle = new ArrayList<String>();
		handle.add(ICONID + "=" + getIconID());
		handle.add(IDS + "=" + getIDS());
		handle.add(ORIGINAL + "=" + getOriginal());
		handle.add(CAPTION + "=" + getCaption());
		handle.add(LENGTH + "=" + getLength());
		handle.add(LANGUAGE + "=" + getLanguage());
		handle.add(PROGRAM + "=" + getProgram());
		return handle;
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasIconID()){
			sl.add(ICONID + "=" + getIconID());
		}
		if(hasIDS()){
			sl.add(IDS + "=" + getIDS());
		}
		if(hasOriginal()){
			sl.add(ORIGINAL + "=" + getOriginal());
		}
		if(hasCaption()){
			sl.add(CAPTION + "=" + getCaption());
		}
		if(hasLength()){
			sl.add(LENGTH + "=" + getLength());
		}
		if(hasLanguage()){
			sl.add(LANGUAGE + "=" + getLanguage());
		}
		if(hasProgram()){
			sl.add(PROGRAM + "=" + getProgram());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("IconHandle={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasIconID();
		ready &= hasIDS();
		ready &= hasOriginal();
		ready &= hasCaption();
		ready &= hasLength();
		ready &= hasLanguage();
		ready &= hasProgram();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		IconHandle iconHandle = (IconHandle)obj;
		return compare(getIconID(), iconHandle.getIconID());
	}

	public IconHandle over(BaseObject obj, boolean fill, boolean has){
		IconHandle iconHandle = ((IconHandle)obj).clone();
		if(iconHandle.hasIconID() && (fill || has == hasIconID())){
			setIconID(iconHandle.getIconID());
		}
		if(iconHandle.hasIDS() && (fill || has == hasIDS())){
			setIDS(iconHandle.getIDS());
		}
		if(iconHandle.hasOriginal() && (fill || has == hasOriginal())){
			setOriginal(iconHandle.getOriginal());
		}
		if(iconHandle.hasCaption() && (fill || has == hasCaption())){
			setCaption(iconHandle.getCaption());
		}
		if(iconHandle.hasLength() && (fill || has == hasLength())){
			setLength(iconHandle.getLength());
		}
		if(iconHandle.hasLanguage() && (fill || has == hasLanguage())){
			setLanguage(iconHandle.getLanguage());
		}
		if(iconHandle.hasProgram() && (fill || has == hasProgram())){
			setProgram(iconHandle.getProgram());
		}
		return this;
	}

	public IconHandle setDefault(){
		setIconID("$07010000");
		setIDS("");
		setOriginal(0);
		setCaption("");
		setLength(1);
		setLanguage(0);
		setProgram(0);
		return this;
	}

	public static IconHandle getDefault(){
		IconHandle iconHandle = new IconHandle();
		iconHandle.setDefault();
		return iconHandle;
	}

	public IconHandle delete(){
		deleteIconID();
		deleteIDS();
		deleteOriginal();
		deleteCaption();
		deleteLength();
		deleteLanguage();
		deleteProgram();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasIconID();
		has |= hasIDS();
		has |= hasOriginal();
		has |= hasCaption();
		has |= hasLength();
		has |= hasLanguage();
		has |= hasProgram();
		return !has;
	}

	public IconHandle(){
		delete();
	}

	public String getIconID(){
		return iconID;
	}

	public void setIconID(String iconID){
		this.iconID = iconID;
	}

	public void deleteIconID(){
		iconID = null;
	}

	public boolean hasIconID(){
		return iconID != null;
	}

	public String getIDS(){
		return ids;
	}

	public void setIDS(String ids){
		this.ids = ids;
	}

	public void deleteIDS(){
		ids = null;
	}

	public boolean hasIDS(){
		return ids != null;
	}

	public Integer getOriginal(){
		return original;
	}

	public String getOriginalS(){
		return Integer.toString(getOriginal());
	}

	public void setOriginal(Integer original){
		this.original = original;
	}

	public void setOriginal(String original){
		setOriginal(new Integer(original));
	}

	public void deleteOriginal(){
		original = null;
	}

	public boolean hasOriginal(){
		return original != null;
	}

	public String getCaption(){
		return caption;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public void deleteCaption(){
		caption = null;
	}

	public boolean hasCaption(){
		return caption != null;
	}

	public Integer getLength(){
		return length;
	}

	public String getLengthS(){
		return Integer.toString(getLength());
	}

	public void setLength(Integer length){
		this.length = length;
	}

	public void setLength(String length){
		setLength(new Integer(length));
	}

	public void deleteLength(){
		length = null;
	}

	public boolean hasLength(){
		return length != null;
	}

	public Integer getLanguage(){
		return language;
	}

	public String getLanguageS(){
		return Integer.toString(getLanguage());
	}

	public void setLanguage(Integer language){
		this.language = language;
	}

	public void setLanguage(String language){
		setLanguage(new Integer(language));
	}

	public void deleteLanguage(){
		language = null;
	}

	public boolean hasLanguage(){
		return language != null;
	}

	public Integer getProgram(){
		return program;
	}

	public String getProgramS(){
		return Integer.toString(getProgram());
	}

	public void setProgram(Integer program){
		this.program = program;
	}

	public void setProgram(String program){
		setProgram(new Integer(program));
	}

	public void deleteProgram(){
		program = null;
	}

	public boolean hasProgram(){
		return program != null;
	}

	public IconHandle clone(){
		return (IconHandle)super.clone();
	}

	public static IconHandle load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (IconHandle)BaseObject.load(file);
	}

	public static IconHandle load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (IconHandle)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static IconHandle unzip(String file) throws IOException, ClassNotFoundException{
		return (IconHandle)BaseObject.unzip(file);
	}

	public static IconHandle unzip(File file) throws IOException, ClassNotFoundException{
		return (IconHandle)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
