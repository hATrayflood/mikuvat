package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.setSectionBracket;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class AnoteEvent extends Event{
	private static final long serialVersionUID = -5646135402807317362L;
	public static final String LENGTH = "Length";
	public static final String NOTE = "Note#";
	public static final String VELOCITY = "Dynamics";
	public static final String BENDDEPTH = "PMBendDepth";
	public static final String BENDLENGTH = "PMBendLength";
	public static final String PORTAMENTOUSE = "PMbPortamentoUse";
	public static final String DECAY = "DEMdecGainRate";
	public static final String ACCENT = "DEMaccent";
	public static final String LYRICHANDLE = "LyricHandle";
	public static final String VIBRATOHANDLE = "VibratoHandle";
	public static final String VIBRATODELAY = "VibratoDelay";

	public static final int NOTE_MAX = 127;
	public static final int NOTE_MIN = 0;
	public static final int VELOCITY_MAX = 127;
	public static final int VELOCITY_MIN = 0;
	public static final int BENDDEPTH_MAX = 100;
	public static final int BENDDEPTH_MIN = 0;
	public static final int BENDLENGTH_MAX = 100;
	public static final int BENDLENGTH_MIN = 0;
	public static final int PORTAMENTOUSE_MAX = 3;
	public static final int PORTAMENTOUSE_MIN = 0;
	public static final int DECAY_MAX = 100;
	public static final int DECAY_MIN = 0;
	public static final int ACCENT_MAX = 100;
	public static final int ACCENT_MIN = 0;

	public static final List<String> NOTE_PARAMLIST = new ArrayList<String>();
	public static final List<String> PORTAMENTOUSE_PARAMLIST = new ArrayList<String>();
	static{
		NOTE_PARAMLIST.add("C-2");
		NOTE_PARAMLIST.add("C#-2");
		NOTE_PARAMLIST.add("D-2");
		NOTE_PARAMLIST.add("Eb-2");
		NOTE_PARAMLIST.add("E-2");
		NOTE_PARAMLIST.add("F-2");
		NOTE_PARAMLIST.add("F#-2");
		NOTE_PARAMLIST.add("G-2");
		NOTE_PARAMLIST.add("G#-2");
		NOTE_PARAMLIST.add("A-2");
		NOTE_PARAMLIST.add("Bb-2");
		NOTE_PARAMLIST.add("B-2");

		NOTE_PARAMLIST.add("C-1");
		NOTE_PARAMLIST.add("C#-1");
		NOTE_PARAMLIST.add("D-1");
		NOTE_PARAMLIST.add("Eb-1");
		NOTE_PARAMLIST.add("E-1");
		NOTE_PARAMLIST.add("F-1");
		NOTE_PARAMLIST.add("F#-1");
		NOTE_PARAMLIST.add("G-1");
		NOTE_PARAMLIST.add("G#-1");
		NOTE_PARAMLIST.add("A-1");
		NOTE_PARAMLIST.add("Bb-1");
		NOTE_PARAMLIST.add("B-1");

		NOTE_PARAMLIST.add("C0");
		NOTE_PARAMLIST.add("C#0");
		NOTE_PARAMLIST.add("D0");
		NOTE_PARAMLIST.add("Eb0");
		NOTE_PARAMLIST.add("E0");
		NOTE_PARAMLIST.add("F0");
		NOTE_PARAMLIST.add("F#0");
		NOTE_PARAMLIST.add("G0");
		NOTE_PARAMLIST.add("G#0");
		NOTE_PARAMLIST.add("A0");
		NOTE_PARAMLIST.add("Bb0");
		NOTE_PARAMLIST.add("B0");

		NOTE_PARAMLIST.add("C1");
		NOTE_PARAMLIST.add("C#1");
		NOTE_PARAMLIST.add("D1");
		NOTE_PARAMLIST.add("Eb1");
		NOTE_PARAMLIST.add("E1");
		NOTE_PARAMLIST.add("F1");
		NOTE_PARAMLIST.add("F#1");
		NOTE_PARAMLIST.add("G1");
		NOTE_PARAMLIST.add("G#1");
		NOTE_PARAMLIST.add("A1");
		NOTE_PARAMLIST.add("Bb1");
		NOTE_PARAMLIST.add("B1");

		NOTE_PARAMLIST.add("C2");
		NOTE_PARAMLIST.add("C#2");
		NOTE_PARAMLIST.add("D2");
		NOTE_PARAMLIST.add("Eb2");
		NOTE_PARAMLIST.add("E2");
		NOTE_PARAMLIST.add("F2");
		NOTE_PARAMLIST.add("F#2");
		NOTE_PARAMLIST.add("G2");
		NOTE_PARAMLIST.add("G#2");
		NOTE_PARAMLIST.add("A2");
		NOTE_PARAMLIST.add("Bb2");
		NOTE_PARAMLIST.add("B2");

		NOTE_PARAMLIST.add("C3");
		NOTE_PARAMLIST.add("C#3");
		NOTE_PARAMLIST.add("D3");
		NOTE_PARAMLIST.add("Eb3");
		NOTE_PARAMLIST.add("E3");
		NOTE_PARAMLIST.add("F3");
		NOTE_PARAMLIST.add("F#3");
		NOTE_PARAMLIST.add("G3");
		NOTE_PARAMLIST.add("G#3");
		NOTE_PARAMLIST.add("A3");
		NOTE_PARAMLIST.add("Bb3");
		NOTE_PARAMLIST.add("B3");

		NOTE_PARAMLIST.add("C4");
		NOTE_PARAMLIST.add("C#4");
		NOTE_PARAMLIST.add("D4");
		NOTE_PARAMLIST.add("Eb4");
		NOTE_PARAMLIST.add("E4");
		NOTE_PARAMLIST.add("F4");
		NOTE_PARAMLIST.add("F#4");
		NOTE_PARAMLIST.add("G4");
		NOTE_PARAMLIST.add("G#4");
		NOTE_PARAMLIST.add("A4");
		NOTE_PARAMLIST.add("Bb4");
		NOTE_PARAMLIST.add("B4");

		NOTE_PARAMLIST.add("C5");
		NOTE_PARAMLIST.add("C#5");
		NOTE_PARAMLIST.add("D5");
		NOTE_PARAMLIST.add("Eb5");
		NOTE_PARAMLIST.add("E5");
		NOTE_PARAMLIST.add("F5");
		NOTE_PARAMLIST.add("F#5");
		NOTE_PARAMLIST.add("G5");
		NOTE_PARAMLIST.add("G#5");
		NOTE_PARAMLIST.add("A5");
		NOTE_PARAMLIST.add("Bb5");
		NOTE_PARAMLIST.add("B5");

		NOTE_PARAMLIST.add("C6");
		NOTE_PARAMLIST.add("C#6");
		NOTE_PARAMLIST.add("D6");
		NOTE_PARAMLIST.add("Eb6");
		NOTE_PARAMLIST.add("E6");
		NOTE_PARAMLIST.add("F6");
		NOTE_PARAMLIST.add("F#6");
		NOTE_PARAMLIST.add("G6");
		NOTE_PARAMLIST.add("G#6");
		NOTE_PARAMLIST.add("A6");
		NOTE_PARAMLIST.add("Bb6");
		NOTE_PARAMLIST.add("B6");

		NOTE_PARAMLIST.add("C7");
		NOTE_PARAMLIST.add("C#7");
		NOTE_PARAMLIST.add("D7");
		NOTE_PARAMLIST.add("Eb7");
		NOTE_PARAMLIST.add("E7");
		NOTE_PARAMLIST.add("F7");
		NOTE_PARAMLIST.add("F#7");
		NOTE_PARAMLIST.add("G7");
		NOTE_PARAMLIST.add("G#7");
		NOTE_PARAMLIST.add("A7");
		NOTE_PARAMLIST.add("Bb7");
		NOTE_PARAMLIST.add("B7");

		NOTE_PARAMLIST.add("C8");
		NOTE_PARAMLIST.add("C#8");
		NOTE_PARAMLIST.add("D8");
		NOTE_PARAMLIST.add("Eb8");
		NOTE_PARAMLIST.add("E8");
		NOTE_PARAMLIST.add("F8");
		NOTE_PARAMLIST.add("F#8");
		NOTE_PARAMLIST.add("G8");

		PORTAMENTOUSE_PARAMLIST.add("付加しない");
		PORTAMENTOUSE_PARAMLIST.add("上方のみ");
		PORTAMENTOUSE_PARAMLIST.add("下方のみ");
		PORTAMENTOUSE_PARAMLIST.add("両方付加");
	}

	private Integer length;
	private Integer note;
	private Integer velocity;
	private Integer bendDepth;
	private Integer bendLength;
	private Integer portamentoUse;
	private Integer decay;
	private Integer accent;
	private LyricHandle lyricHandle;
	private VibratoHandle vibratoHandle;

	public AnoteEvent(Map<String, String> pvs, Map<String, List<String>> sections){
		this();
		setEvent(pvs, sections);
	}

	public void setEvent(Map<String, String> pvs, Map<String, List<String>> sections){
		setLength(pvs.get(LENGTH));
		setNote(pvs.get(NOTE));
		setVelocity(pvs.get(VELOCITY));
		setBendDepth(pvs.get(BENDDEPTH));
		setBendLength(pvs.get(BENDLENGTH));
		setPortamentoUse(pvs.get(PORTAMENTOUSE));
		setDecay(pvs.get(DECAY));
		setAccent(pvs.get(ACCENT));
		setLyricHandle(new LyricHandle(sections.get(pvs.get(LYRICHANDLE))));

		String vh = pvs.get(VIBRATOHANDLE);
		if(vh != null){
			setVibratoHandle(new VibratoHandle(sections.get(vh)));
		}
		String vd = pvs.get(VIBRATODELAY);
		if(vd != null){
			getVibratoHandle().setVibratoDelay(vd);
		}
	}

	public void getEvent(long tick, List<String> eventlist, List<List<String>> idlists, List<List<String>> handlelists){
		String id = getIDString(eventlist.size());
		eventlist.add(tick + "=" + id);

		List<String> ps = new ArrayList<String>();
		ps.add(setSectionBracket(id));
		ps.add(TYPE + "=" + ANOTE);
		ps.add(LENGTH + "=" + getLength());
		ps.add(NOTE + "=" + getNote());
		ps.add(VELOCITY + "=" + getVelocity());
		ps.add(BENDDEPTH + "=" + getBendDepth());
		ps.add(BENDLENGTH + "=" + getBendLength());
		ps.add(PORTAMENTOUSE + "=" + getPortamentoUse());
		ps.add(DECAY + "=" + getDecay());
		ps.add(ACCENT + "=" + getAccent());

		String lh = getHandleString(handlelists.size());
		ps.add(LYRICHANDLE + "=" + lh);
		List<String> lps = new ArrayList<String>();
		lps.add(setSectionBracket(lh));
		lps.addAll(getLyricHandle().getHandle());
		handlelists.add(lps);

		if(hasVibratoHandle()){
			String vh = getHandleString(handlelists.size());
			ps.add(VIBRATOHANDLE + "=" + vh);
			ps.add(VIBRATODELAY + "=" + getVibratoHandle().getVibratoDelay());
			List<String> vps = new ArrayList<String>();
			vps.add(setSectionBracket(vh));
			vps.addAll(getVibratoHandle().getHandle());
			handlelists.add(vps);
		}

		idlists.add(ps);
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasLength()){
			sl.add("Length=" + getLength());
		}
		if(hasNote()){
			sl.add("Note=" + NOTE_PARAMLIST.get(getNote()));
		}
		if(hasVelocity()){
			sl.add("Velocity=" + getVelocity());
		}
		if(hasBendDepth()){
			sl.add("BendDepth=" + getBendDepth());
		}
		if(hasBendLength()){
			sl.add("BendLength=" + getBendLength());
		}
		if(hasPortamentoUse()){
			sl.add("PortamentoUse=" + PORTAMENTOUSE_PARAMLIST.get(getPortamentoUse()));
		}
		if(hasDecay()){
			sl.add("Decay=" + getDecay());
		}
		if(hasAccent()){
			sl.add("Accent=" + getAccent());
		}
		if(hasLyricHandle()){
			sl.add(LYRICHANDLE + "={" + getLyricHandle().toString() + "}");
		}
		if(hasVibratoHandle()){
			sl.add(VIBRATOHANDLE + "={" + getVibratoHandle().toString() + "}");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("AnoteEvent={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasLength();
		ready &= hasNote();
		ready &= hasVelocity();
		ready &= hasBendDepth();
		ready &= hasBendLength();
		ready &= hasPortamentoUse();
		ready &= hasDecay();
		ready &= hasAccent();
		ready &= hasLyricHandle() && getLyricHandle().validate();
		ready &= !hasVibratoHandle() || getVibratoHandle().validate();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		AnoteEvent anoteEvent = (AnoteEvent)obj;
		int c = compare(getLyricHandle(), anoteEvent.getLyricHandle());
		if(c == 0){
			c = compare(getNote(), anoteEvent.getNote());
		}
		if(c == 0){
			c = compare(getLength(), anoteEvent.getLength());
		}
		return c;
	}

	public AnoteEvent over(BaseObject obj, boolean fill, boolean has){
		AnoteEvent anoteEvent = ((AnoteEvent)obj).clone();
		if(anoteEvent.hasLength() && (fill || has == hasLength())){
			setLength(anoteEvent.getLength());
		}
		if(anoteEvent.hasNote() && (fill || has == hasNote())){
			setNote(anoteEvent.getNote());
		}
		if(anoteEvent.hasVelocity() && (fill || has == hasVelocity())){
			setVelocity(anoteEvent.getVelocity());
		}
		if(anoteEvent.hasBendDepth() && (fill || has == hasBendDepth())){
			setBendDepth(anoteEvent.getBendDepth());
		}
		if(anoteEvent.hasBendLength() && (fill || has == hasBendLength())){
			setBendLength(anoteEvent.getBendLength());
		}
		if(anoteEvent.hasPortamentoUse() && (fill || has == hasPortamentoUse())){
			setPortamentoUse(anoteEvent.getPortamentoUse());
		}
		if(anoteEvent.hasDecay() && (fill || has == hasDecay())){
			setDecay(anoteEvent.getDecay());
		}
		if(anoteEvent.hasAccent() && (fill || has == hasAccent())){
			setAccent(anoteEvent.getAccent());
		}
		if(anoteEvent.hasLyricHandle()){
			LyricHandle lyricHandle = hasLyricHandle() ? getLyricHandle() : new LyricHandle();
			setLyricHandle(lyricHandle.over(anoteEvent.getLyricHandle(), fill, has));
		}
		if(anoteEvent.hasVibratoHandle()){
			VibratoHandle vibratoHandle = hasVibratoHandle() ? getVibratoHandle() : new VibratoHandle();
			setVibratoHandle(vibratoHandle.over(anoteEvent.getVibratoHandle(), fill, has));
		}
		return this;
	}

	public AnoteEvent setDefault(){
		setLength(480);
		setNote(64);
		setVelocity(64);
		setBendDepth(8);
		setBendLength(0);
		setPortamentoUse(0);
		setDecay(50);
		setAccent(50);
		setLyricHandle(LyricHandle.getDefault());
		deleteVibratoHandle();
		return this;
	}

	public static AnoteEvent getDefault(){
		AnoteEvent anoteEvent = new AnoteEvent();
		anoteEvent.setDefault();
		return anoteEvent;
	}

	public AnoteEvent delete(){
		deleteLength();
		deleteNote();
		deleteVelocity();
		deleteBendDepth();
		deleteBendLength();
		deletePortamentoUse();
		deleteDecay();
		deleteAccent();
		deleteLyricHandle();
		deleteVibratoHandle();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasLength();
		has |= hasNote();
		has |= hasVelocity();
		has |= hasBendDepth();
		has |= hasBendLength();
		has |= hasPortamentoUse();
		has |= hasDecay();
		has |= hasAccent();
		has |= hasLyricHandle();
		has |= hasVibratoHandle();
		return !has;
	}

	public AnoteEvent(){
		delete();
	}

	public Integer getLength(){
		return length;
	}

	public String getLengthS(){
		return Integer.toString(getLength());
	}

	public void setLength(Integer length){
		this.length = length;
	}

	public void setLength(String length){
		setLength(new Integer(length));
	}

	public void deleteLength(){
		length = null;
	}

	public boolean hasLength(){
		return length != null;
	}

	public Integer getNote(){
		return note;
	}

	public String getNoteS(){
		return Integer.toString(getNote());
	}

	public void setNote(Integer note){
		this.note = adjustMaxMin(note, NOTE_MAX, NOTE_MIN);
	}

	public void setNote(String note){
		setNote(new Integer(note));
	}

	public void deleteNote(){
		note = null;
	}

	public boolean hasNote(){
		return note != null;
	}

	public Integer getVelocity(){
		return velocity;
	}

	public String getVelocityS(){
		return Integer.toString(getVelocity());
	}

	public void setVelocity(Integer velocity){
		this.velocity = adjustMaxMin(velocity, VELOCITY_MAX, VELOCITY_MIN);
	}

	public void setVelocity(String velocity){
		setVelocity(new Integer(velocity));
	}

	public void deleteVelocity(){
		velocity = null;
	}

	public boolean hasVelocity(){
		return velocity != null;
	}

	public Integer getBendDepth(){
		return bendDepth;
	}

	public String getBendDepthS(){
		return Integer.toString(getBendDepth());
	}

	public void setBendDepth(Integer bendDepth){
		this.bendDepth = adjustMaxMin(bendDepth, BENDDEPTH_MAX, BENDDEPTH_MIN);
	}

	public void setBendDepth(String bendDepth){
		setBendDepth(new Integer(bendDepth));
	}

	public void deleteBendDepth(){
		bendDepth = null;
	}

	public boolean hasBendDepth(){
		return bendDepth != null;
	}

	public Integer getBendLength(){
		return bendLength;
	}

	public String getBendLengthS(){
		return Integer.toString(getBendLength());
	}

	public void setBendLength(Integer bendLength){
		this.bendLength = adjustMaxMin(bendLength, BENDLENGTH_MAX, BENDLENGTH_MIN);
	}

	public void setBendLength(String bendLength){
		setBendLength(new Integer(bendLength));
	}

	public void deleteBendLength(){
		bendLength = null;
	}

	public boolean hasBendLength(){
		return bendLength != null;
	}

	public Integer getPortamentoUse(){
		return portamentoUse;
	}

	public String getPortamentoUseS(){
		return Integer.toString(getPortamentoUse());
	}

	public void setPortamentoUse(Integer portamentoUse){
		this.portamentoUse = adjustMaxMin(portamentoUse, PORTAMENTOUSE_MAX, PORTAMENTOUSE_MIN);
	}

	public void setPortamentoUse(String portamentoUse){
		setPortamentoUse(new Integer(portamentoUse));
	}

	public void deletePortamentoUse(){
		portamentoUse = null;
	}

	public boolean hasPortamentoUse(){
		return portamentoUse != null;
	}

	public Integer getDecay(){
		return decay;
	}

	public String getDecayS(){
		return Integer.toString(getDecay());
	}

	public void setDecay(Integer decay){
		this.decay = adjustMaxMin(decay, DECAY_MAX, DECAY_MIN);
	}

	public void setDecay(String decay){
		setDecay(new Integer(decay));
	}

	public void deleteDecay(){
		decay = null;
	}

	public boolean hasDecay(){
		return decay != null;
	}

	public Integer getAccent(){
		return accent;
	}

	public String getAccentS(){
		return Integer.toString(getAccent());
	}

	public void setAccent(Integer accent){
		this.accent = adjustMaxMin(accent, ACCENT_MAX, ACCENT_MIN);
	}

	public void setAccent(String accent){
		setAccent(new Integer(accent));
	}

	public void deleteAccent(){
		accent = null;
	}

	public boolean hasAccent(){
		return accent != null;
	}

	public LyricHandle getLyricHandle(){
		return lyricHandle;
	}

	public void setLyricHandle(LyricHandle lyricHandle){
		if(lyricHandle == null || lyricHandle.isDeleted()){
			deleteLyricHandle();
			return;
		}
		this.lyricHandle = lyricHandle;
	}

	public void deleteLyricHandle(){
		lyricHandle = null;
	}

	public boolean hasLyricHandle(){
		return lyricHandle != null;
	}

	public VibratoHandle getVibratoHandle(){
		return vibratoHandle;
	}

	public void setVibratoHandle(VibratoHandle vibratoHandle){
		if(vibratoHandle == null || vibratoHandle.isDeleted()){
			deleteVibratoHandle();
			return;
		}
		this.vibratoHandle = vibratoHandle;
	}

	public void deleteVibratoHandle(){
		vibratoHandle = null;
	}

	public boolean hasVibratoHandle(){
		return vibratoHandle != null;
	}

	public AnoteEvent clone(){
		return (AnoteEvent)super.clone();
	}

	public static AnoteEvent load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (AnoteEvent)BaseObject.load(file);
	}

	public static AnoteEvent load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (AnoteEvent)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static AnoteEvent unzip(String file) throws IOException, ClassNotFoundException{
		return (AnoteEvent)BaseObject.unzip(file);
	}

	public static AnoteEvent unzip(File file) throws IOException, ClassNotFoundException{
		return (AnoteEvent)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
