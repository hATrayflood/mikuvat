package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.io.vsq.VSQFile.COMMON;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class VSQTrack extends BaseObject{
	private static final long serialVersionUID = 6296614810415852578L;
	private CommonSection common;
	private MixerSetting mixer;
	private TimeLine timeLine;

	public VSQTrack(MixerSetting mixer, Map<String, List<String>> sections){
		this();
		setTrack(mixer, sections);
	}

	public VSQTrack(Map<String, List<String>> sections){
		this();
		setTrack(sections);
	}

	public void setTrack(MixerSetting mixer, Map<String, List<String>> sections){
		setMixer(mixer);
		setTrack(sections);
	}

	public void setTrack(Map<String, List<String>> sections){
		setCommon(new CommonSection(sections.get(COMMON)));
		setTimeLine(new TimeLine(sections));
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasCommon()){
			sl.add("Common={" + getCommon().toString() + "}");
		}
		if(hasMixer()){
			sl.add("Mixer={" + getMixer().toString() + "}");
		}
		if(hasTimeLine()){
			sl.add("TimeLine={" + getTimeLine().toString() + "}");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("VSQTrack={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasCommon() && getCommon().validate();
		ready &= hasMixer() && getMixer().validate();
		ready &= hasTimeLine() && getTimeLine().validate();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		VSQTrack vsqTrack = (VSQTrack)obj;
		return compare(getCommon().getName(), vsqTrack.getCommon().getName());
	}

	public VSQTrack over(BaseObject obj, boolean fill, boolean has){
		VSQTrack vsqTrack = ((VSQTrack)obj).clone();
		if(vsqTrack.hasCommon()){
			CommonSection common = hasCommon() ? getCommon() : new CommonSection();
			setCommon(common.over(vsqTrack.getCommon(), fill, has));
		}
		if(vsqTrack.hasMixer()){
			MixerSetting mixer = hasMixer() ? getMixer() : new MixerSetting();
			setMixer(mixer.over(vsqTrack.getMixer(), fill, has));
		}
		if(vsqTrack.hasTimeLine()){
			TimeLine timeLine = hasTimeLine() ? getTimeLine() : new TimeLine();
			setTimeLine(timeLine.over(vsqTrack.getTimeLine(), fill, has));
		}
		return this;
	}

	public VSQTrack setDefault(){
		setCommon(CommonSection.getDefault());
		setMixer(MixerSetting.getDefault());
		setTimeLine(TimeLine.getDefault());
		return this;
	}

	public static VSQTrack getDefault(){
		VSQTrack vsqTrack = new VSQTrack();
		vsqTrack.setDefault();
		return vsqTrack;
	}

	public VSQTrack delete(){
		deleteCommon();
		deleteMixer();
		deleteTimeLine();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasCommon();
		has |= hasMixer();
		has |= hasTimeLine();
		return !has;
	}

	public VSQTrack(){
		delete();
	}

	public CommonSection getCommon(){
		return common;
	}

	public void setCommon(CommonSection common){
		if(common == null || common.isDeleted()){
			deleteCommon();
			return;
		}
		this.common = common;
	}

	public void deleteCommon(){
		common = null;
	}

	public boolean hasCommon(){
		return common != null;
	}

	public MixerSetting getMixer(){
		return mixer;
	}

	public void setMixer(MixerSetting mixer){
		if(mixer == null || mixer.isDeleted()){
			deleteMixer();
			return;
		}
		this.mixer = mixer;
	}

	public void deleteMixer(){
		mixer = null;
	}

	public boolean hasMixer(){
		return mixer != null;
	}

	public TimeLine getTimeLine(){
		return timeLine;
	}

	public void setTimeLine(TimeLine timeLine){
		if(timeLine == null || timeLine.isDeleted()){
			deleteTimeLine();
			return;
		}
		this.timeLine = timeLine;
	}

	public void deleteTimeLine(){
		timeLine = null;
	}

	public boolean hasTimeLine(){
		return timeLine != null;
	}

	public VSQTrack clone(){
		return (VSQTrack)super.clone();
	}

	public static VSQTrack load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VSQTrack)BaseObject.load(file);
	}

	public static VSQTrack load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (VSQTrack)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static VSQTrack unzip(String file) throws IOException, ClassNotFoundException{
		return (VSQTrack)BaseObject.unzip(file);
	}

	public static VSQTrack unzip(File file) throws IOException, ClassNotFoundException{
		return (VSQTrack)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
