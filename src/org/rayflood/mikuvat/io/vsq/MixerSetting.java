package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.getBooleanValue;
import static org.rayflood.mikuvat.Utilities.getStringValue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class MixerSetting extends BaseObject{
	private static final long serialVersionUID = -4879364127354448647L;
	private Integer feder;
	private Integer panpot;
	private Boolean mute;
	private Boolean solo;

	public MixerSetting(Integer feder, Integer panpot, Boolean mute, Boolean solo){
		this();
		setFeder(feder);
		setPanpot(panpot);
		setMute(mute);
		setSolo(solo);
	}

	public MixerSetting(String feder, String panpot, String mute, String solo){
		this();
		setFeder(feder);
		setPanpot(panpot);
		setMute(mute);
		setSolo(solo);
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasFeder()){
			sl.add("Feder=" + getFeder());
		}
		if(hasPanpot()){
			sl.add("Panpot=" + getPanpot());
		}
		if(hasMute()){
			sl.add("Mute=" + getMute());
		}
		if(hasSolo()){
			sl.add("Solo=" + getSolo());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("MixerSetting={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasFeder();
		ready &= hasPanpot();
		ready &= hasMute();
		ready &= hasSolo();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		MixerSetting mixerSetting = (MixerSetting)obj;
		int c = compare(getFeder(), mixerSetting.getFeder());
		if(c == 0){
			c = compare(getPanpot(), mixerSetting.getPanpot());
		}
		if(c == 0){
			c = compare(getMute(), mixerSetting.getMute());
		}
		if(c == 0){
			c = compare(getSolo(), mixerSetting.getSolo());
		}
		return c;
	}

	public MixerSetting over(BaseObject obj, boolean fill, boolean has){
		MixerSetting mixerSetting = ((MixerSetting)obj).clone();
		if(mixerSetting.hasFeder() && (fill || has == hasFeder())){
			setFeder(mixerSetting.getFeder());
		}
		if(mixerSetting.hasPanpot() && (fill || has == hasPanpot())){
			setPanpot(mixerSetting.getPanpot());
		}
		if(mixerSetting.hasMute() && (fill || has == hasMute())){
			setMute(mixerSetting.getMute());
		}
		if(mixerSetting.hasSolo() && (fill || has == hasSolo())){
			setSolo(mixerSetting.getSolo());
		}
		return this;
	}

	public MixerSetting setDefault(){
		setFeder(0);
		setPanpot(0);
		setMute(false);
		setSolo(false);
		return this;
	}

	public static MixerSetting getDefault(){
		MixerSetting mixerSetting = new MixerSetting();
		mixerSetting.setDefault();
		return mixerSetting;
	}

	public MixerSetting delete(){
		deleteFeder();
		deletePanpot();
		deleteMute();
		deleteSolo();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasFeder();
		has |= hasPanpot();
		has |= hasMute();
		has |= hasSolo();
		return !has;
	}

	public MixerSetting(){
		delete();
	}

	public Integer getFeder(){
		return feder;
	}

	public String getFederS(){
		return Integer.toString(getFeder());
	}

	public void setFeder(Integer feder){
		this.feder = adjustMaxMin(feder, MixerSection.FEDER_MAX, MixerSection.FEDER_MIN);
	}

	public void setFeder(String feder){
		setFeder(new Integer(feder));
	}

	public void deleteFeder(){
		feder = null;
	}

	public boolean hasFeder(){
		return feder != null;
	}

	public Integer getPanpot(){
		return panpot;
	}

	public String getPanpotS(){
		return Integer.toString(getPanpot());
	}

	public void setPanpot(Integer panpot){
		this.panpot = adjustMaxMin(panpot, MixerSection.PANPOT_MAX, MixerSection.PANPOT_MIN);
	}

	public void setPanpot(String panpot){
		setPanpot(new Integer(panpot));
	}

	public void deletePanpot(){
		panpot = null;
	}

	public boolean hasPanpot(){
		return panpot != null;
	}

	public Boolean getMute(){
		return mute;
	}

	public String getMuteS(){
		return getStringValue(getMute());
	}

	public void setMute(Boolean mute){
		this.mute = mute;
	}

	public void setMute(String mute){
		setMute(getBooleanValue(mute));
	}

	public void deleteMute(){
		mute = null;
	}

	public boolean hasMute(){
		return mute != null;
	}

	public Boolean getSolo(){
		return solo;
	}

	public String getSoloS(){
		return getStringValue(getSolo());
	}

	public void setSolo(Boolean solo){
		this.solo = solo;
	}

	public void setSolo(String solo){
		setSolo(getBooleanValue(solo));
	}

	public void deleteSolo(){
		solo = null;
	}

	public boolean hasSolo(){
		return solo != null;
	}

	public MixerSetting clone(){
		return (MixerSetting)super.clone();
	}

	public static MixerSetting load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MixerSetting)BaseObject.load(file);
	}

	public static MixerSetting load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MixerSetting)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MixerSetting unzip(String file) throws IOException, ClassNotFoundException{
		return (MixerSetting)BaseObject.unzip(file);
	}

	public static MixerSetting unzip(File file) throws IOException, ClassNotFoundException{
		return (MixerSetting)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
