package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rayflood.mikuvat.io.BaseObject;

public abstract class Event extends BaseObject{
	private static final long serialVersionUID = 3144497610281730738L;
	public static final String TYPE = "Type";
	public static final String SINGER = "Singer";
	public static final String ANOTE = "Anote";
	public static final String EOS = "EOS";

	public int compareTo(BaseObject obj){
		Event event = (Event)obj;
		return compare(getClass().getName(), event.getClass().getName());
	}

	public static Event newEvent(String eventid, Map<String, List<String>> sections){
		if(EOS.equals(eventid)){
			return new EOSEvent();
		}

		List<String> ps = sections.get(eventid);
		Map<String, String> pvs = new HashMap<String, String>();
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			pvs.put(pv[0], pv[1]);
		}

		Event event = null;
		if(SINGER.equals(pvs.get(TYPE))){
			event = new SingerEvent(pvs, sections);
		}
		if(ANOTE.equals(pvs.get(TYPE))){
			event = new AnoteEvent(pvs, sections);
		}
		return event;
	}

	public abstract void setEvent(Map<String, String> pvs, Map<String, List<String>> sections);

	public abstract void getEvent(long tick, List<String> eventlist, List<List<String>> idlists, List<List<String>> handlelists);

	public String getIDString(int id){
		return String.format("ID#%04d", id);
	}

	public String getHandleString(int handle){
		return String.format("h#%04d", handle);
	}

	public Event clone(){
		return (Event)super.clone();
	}

	public static Event load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (Event)BaseObject.load(file);
	}

	public static Event load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (Event)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static Event unzip(String file) throws IOException, ClassNotFoundException{
		return (Event)BaseObject.unzip(file);
	}

	public static Event unzip(File file) throws IOException, ClassNotFoundException{
		return (Event)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
