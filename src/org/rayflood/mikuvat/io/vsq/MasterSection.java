package org.rayflood.mikuvat.io.vsq;

import static org.rayflood.mikuvat.Utilities.adjustMaxMin;
import static org.rayflood.mikuvat.Utilities.splitEqual;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class MasterSection extends BaseObject{
	private static final long serialVersionUID = 3680414369532973128L;

	public static final String PREMEASURE = "PreMeasure";

	public static final int PREMEASURE_MAX = 8;
	public static final int PREMEASURE_MIN = 1;

	private Integer preMeasure;

	public MasterSection(List<String> ps){
		this();
		setSection(ps);
	}

	public void setSection(List<String> ps){
		for(int i = 0; i < ps.size(); i++){
			String pv[] = splitEqual(ps.get(i));
			if(PREMEASURE.equals(pv[0])){
				setPreMeasure(pv[1]);
			}
		}
	}

	public List<String> getSection(){
		List<String> ps = new ArrayList<String>();
		ps.add(PREMEASURE + "=" + getPreMeasure());
		return ps;
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasPreMeasure()){
			sl.add(PREMEASURE + "=" + getPreMeasure());
		}

		StringBuilder sb = new StringBuilder();
		sb.append("MasterSection={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasPreMeasure();
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		MasterSection masterSection = (MasterSection)obj;
		return compare(this.getPreMeasure(), masterSection.getPreMeasure());
	}

	public MasterSection over(BaseObject obj, boolean fill, boolean has){
		MasterSection masterSection = ((MasterSection)obj).clone();
		if(masterSection.hasPreMeasure() && (fill || has == hasPreMeasure())){
			setPreMeasure(masterSection.getPreMeasure());
		}
		return this;
	}

	public MasterSection setDefault(){
		setPreMeasure(4);
		return this;
	}

	public static MasterSection getDefault(){
		MasterSection masterSection = new MasterSection();
		masterSection.setDefault();
		return masterSection;
	}

	public MasterSection delete(){
		deletePreMeasure();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasPreMeasure();
		return !has;
	}

	public MasterSection(){
		delete();
	}

	public Integer getPreMeasure(){
		return preMeasure;
	}

	public String getPreMeasureS(){
		return Integer.toString(getPreMeasure());
	}

	public void setPreMeasure(Integer preMeasure){
		this.preMeasure = adjustMaxMin(preMeasure, PREMEASURE_MAX, PREMEASURE_MIN);
	}

	public void setPreMeasure(String preMeasure){
		setPreMeasure(new Integer(preMeasure));
	}

	public void deletePreMeasure(){
		preMeasure = null;
	}

	public boolean hasPreMeasure(){
		return preMeasure != null;
	}

	public MasterSection clone(){
		return (MasterSection)super.clone();
	}

	public static MasterSection load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MasterSection)BaseObject.load(file);
	}

	public static MasterSection load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MasterSection)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MasterSection unzip(String file) throws IOException, ClassNotFoundException{
		return (MasterSection)BaseObject.unzip(file);
	}

	public static MasterSection unzip(File file) throws IOException, ClassNotFoundException{
		return (MasterSection)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
