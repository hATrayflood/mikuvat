package org.rayflood.mikuvat.io.midi;

import static org.rayflood.mikuvat.Utilities.getByteArrayString;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.SysexMessage;

import org.rayflood.mikuvat.io.BaseObject;

public class MidiSysex extends MidiData{
	private static final long serialVersionUID = -697221293558971797L;
	private int status;
	private byte[] data;

	protected MidiSysex(MidiMessage mm){
		setMidiEvent(mm);
	}

	public MidiSysex(SysexMessage sysex){
		setMidiEvent(sysex);
	}

	public MidiSysex(int status, byte[] data){
		setStatus(status);
		setData(data);
	}

	public SysexMessage getMidiEvent() throws InvalidMidiDataException{
		SysexMessage sysex = new SysexMessage();
		sysex.setMessage(getStatus(), getData(), getData().length);
		return sysex;
	}

	protected void setMidiEvent(MidiMessage mm){
		setMidiEvent((SysexMessage)mm);
	}

	public void setMidiEvent(SysexMessage sysex){
		setStatus(sysex.getStatus());
		setData(sysex.getData());
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		sl.add("Status=" + getStatus());
		sl.add("Data=" + getByteArrayString(getData()));

		StringBuilder sb = new StringBuilder();
		sb.append("MidiSysex={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		MidiSysex midiSysex = (MidiSysex)obj;
		return compare(getStatus(), midiSysex.getStatus());
	}

	public MidiSysex over(BaseObject obj, boolean fill, boolean has){
		MidiSysex midiSysex = ((MidiSysex)obj).clone();
		setStatus(midiSysex.getStatus());
		setData(midiSysex.getData());
		return this;
	}

	public MidiSysex setDefault(){
		return this;
	}

	public static MidiSysex getDefault(){
		MidiSysex midiSysex = new MidiSysex();
		midiSysex.setDefault();
		return midiSysex;
	}

	public MidiSysex delete(){
		setStatus(0);
		setData(new byte[]{});
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public MidiSysex(){
	}

	public int getStatus(){
		return status;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public byte[] getData(){
		return data;
	}

	public void setData(byte[] data){
		this.data = data;
	}

	public MidiSysex clone(){
		return (MidiSysex)super.clone();
	}

	public static MidiSysex load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiSysex)BaseObject.load(file);
	}

	public static MidiSysex load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiSysex)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiSysex unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiSysex)BaseObject.unzip(file);
	}

	public static MidiSysex unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiSysex)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
