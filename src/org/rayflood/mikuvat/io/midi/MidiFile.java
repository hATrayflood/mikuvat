package org.rayflood.mikuvat.io.midi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Track;

import org.rayflood.mikuvat.io.BaseObject;
import org.rayflood.mikuvat.io.InvalidBaseObjectException;

public class MidiFile extends BaseObject{
	private static final long serialVersionUID = 4239387350760199430L;

	private File midiFile;
	private Integer fileType;
	private Float divisionType;
	private Integer resolution;
	private MidiTrack conductor;
	private ArrayList<MidiTrack> tracks;

	public MidiFile(String midi) throws IOException, InvalidMidiDataException{
		this();
		readMidiFile(midi);
	}

	public MidiFile(File midi) throws IOException, InvalidMidiDataException{
		this();
		readMidiFile(midi);
	}

	public MidiFile(Sequence sq){
		this();
		setMidiSequence(sq);
	}

	public void readMidiFile(String midi) throws IOException, InvalidMidiDataException{
		readMidiFile(new File(midi));
	}

	public void readMidiFile(File midi) throws IOException, InvalidMidiDataException{
		setMidiFile(midi);
		setMidiSequence(MidiSystem.getSequence(getMidiFile()));
	}

	public void writeMidiFile() throws IOException, InvalidMidiDataException{
		writeMidiFile(getMidiFile());
	}

	public void writeMidiFile(String midi) throws IOException, InvalidMidiDataException{
		writeMidiFile(new File(midi));
	}

	public void writeMidiFile(File midi) throws IOException, InvalidMidiDataException{
		MidiSystem.write(getMidiSequence(), getFileType(), midi);
	}

	public void setMidiSequence(Sequence sq){
		setFileType(MidiSystem.getMidiFileTypes(sq)[0]);
		setDivisionType(sq.getDivisionType());
		setResolution(sq.getResolution());
		Track t[] = sq.getTracks();
		setConductor(t[0]);
		for(int i = 1; i < t.length; i++){
			addMidiTrack(new MidiTrack(t[i]));
		}
	}

	public Sequence getMidiSequence() throws InvalidMidiDataException{
		Sequence sq = new Sequence(getDivisionType(), getResolution());

		getConductor(sq.createTrack());
		for(int i = 0; i < getMidiTrackSize(); i++){
			MidiTrack mt = getMidiTrack(i);
			mt.getTrack(sq.createTrack());
		}

		return sq;
	}

	public int getMidiTrackSize(){
		return tracks.size();
	}

	public void addMidiTrack(MidiTrack mt){
		if(mt == null || mt.isDeleted()){
			return;
		}
		tracks.add(mt);
	}

	public void addMidiTrack(int index, MidiTrack mt){
		if(mt == null || mt.isDeleted()){
			return;
		}
		if(index < getMidiTrackSize()){
			tracks.add(index, mt);
		}
		else{
			addMidiTrack(mt);
		}
	}

	public void setMidiTrack(int index, MidiTrack mt){
		if(mt == null || mt.isDeleted()){
			removeMidiTrack(index);
			return;
		}
		if(index < getMidiTrackSize()){
			tracks.set(index, mt);
		}
		else{
			addMidiTrack(mt);
		}
	}

	public MidiTrack getMidiTrack(int index){
		if(index < getMidiTrackSize()){
			return tracks.get(index);
		}
		else{
			return new MidiTrack();
		}
	}

	public MidiTrack removeMidiTrack(int index){
		if(index < getMidiTrackSize()){
			return tracks.remove(index);
		}
		else{
			return null;
		}
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		if(hasMidiFile()){
			sl.add("midiFile=" + getMidiFileS());
		}
		if(hasFileType()){
			sl.add("FileType=" + getFileType());
		}
		if(hasDivisionType()){
			sl.add("DivisionType=" + getDivisionType());
		}
		if(hasResolution()){
			sl.add("Resolution=" + getResolution());
		}
		if(hasConductor()){
			sl.add("Conductor={" + getConductor().toString() + "}");
		}
		if(hasMidiTracks()){
			StringBuilder sb = new StringBuilder();
			sb.append(getMidiTrack(0).toString());
			for(int i = 1; i < getMidiTrackSize(); i++){
				sb.append("," + getMidiTrack(i).toString());
			}
			sl.add("Tracks=[" + sb.toString() + "]");
		}

		StringBuffer sb = new StringBuffer();
		sb.append("MidiFile={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate() throws InvalidBaseObjectException{
		boolean ready = true;
		ready &= hasFileType();
		ready &= hasDivisionType();
		ready &= hasResolution();
		ready &= hasConductor() && getConductor().validate();
		for(int i = 0; i < getMidiTrackSize(); i++){
			ready &= getMidiTrack(i).validate();
		}
		if(!ready){
			throw new InvalidBaseObjectException(this);
		}
		return ready;
	}

	public int compareTo(BaseObject obj){
		MidiFile midiFile = (MidiFile)obj;
		return compare(getMidiFile().getPath(), midiFile.getMidiFile().getPath());
	}

	public MidiFile over(BaseObject obj, boolean fill, boolean has){
		MidiFile midiFile = ((MidiFile)obj).clone();
		if(midiFile.hasFileType() && (fill || has == hasFileType())){
			setFileType(midiFile.getFileType());
		}
		if(midiFile.hasDivisionType() && (fill || has == hasDivisionType())){
			setDivisionType(midiFile.getDivisionType());
		}
		if(midiFile.hasResolution() && (fill || has == hasResolution())){
			setResolution(midiFile.getResolution());
		}
		if(midiFile.hasConductor()){
			MidiTrack conductor = hasConductor() ? getConductor() : new MidiTrack();
			setConductor(conductor.over(midiFile.getConductor(), fill, has));
		}
		for(int i = 0; i < midiFile.getMidiTracks().size(); i++){
			MidiTrack mt = getMidiTrack(i);
			setMidiTrack(i, mt.over(midiFile.getMidiTrack(i), fill, has));
		}
		if(midiFile.hasMidiFile() && (fill || has == hasMidiFile())){
			setMidiFile(midiFile.getMidiFile());
		}
		return this;
	}

	public MidiFile setDefault(){
		setFileType(1);
		setDivisionType(0.0f);
		setResolution(480);
		setConductor(new MidiTrack());
		deleteMidiTracks();
		addMidiTrack(MidiTrack.getDefault());
		deleteMidiFile();
		return this;
	}

	public static MidiFile getDefault(){
		MidiFile midiFile = new MidiFile();
		midiFile.setDefault();
		return midiFile;
	}

	public MidiFile delete(){
		deleteFileType();
		deleteDivisionType();
		deleteResolution();
		deleteConductor();
		deleteMidiTracks();
		deleteMidiFile();
		return this;
	}

	public boolean isDeleted(){
		boolean has = false;
		has |= hasFileType();
		has |= hasDivisionType();
		has |= hasResolution();
		has |= hasConductor();
		has |= hasMidiTracks();
		has |= hasMidiFile();
		return !has;
	}

	public MidiFile(){
		delete();
	}

	public File getMidiFile(){
		return midiFile;
	}

	public String getMidiFileS(){
		return getMidiFile().getPath();
	}

	public void setMidiFile(File midiFile){
		this.midiFile = midiFile;
	}

	public void setMidiFile(String midiFile){
		setMidiFile(new File(midiFile));
	}

	public void deleteMidiFile(){
		midiFile = null;
	}

	public boolean hasMidiFile(){
		return midiFile != null;
	}

	public Integer getFileType(){
		return fileType;
	}

	public void setFileType(Integer fileType){
		this.fileType = fileType;
	}

	public void deleteFileType(){
		fileType = null;
	}

	public boolean hasFileType(){
		return fileType != null;
	}

	public Float getDivisionType(){
		return divisionType;
	}

	public void setDivisionType(Float divisionType){
		this.divisionType = divisionType;
	}

	public void deleteDivisionType(){
		divisionType = null;
	}

	public boolean hasDivisionType(){
		return divisionType != null;
	}

	public Integer getResolution(){
		return resolution;
	}

	public void setResolution(Integer resolution){
		this.resolution = resolution;
	}

	public void deleteResolution(){
		resolution = null;
	}

	public boolean hasResolution(){
		return resolution != null;
	}

	public MidiTrack getConductor(){
		return conductor;
	}

	public void getConductor(Track t) throws InvalidMidiDataException{
		conductor.getTrack(t);
	}

	public void setConductor(MidiTrack conductor){
		if(conductor == null || conductor.isDeleted()){
			deleteConductor();
			return;
		}
		this.conductor = conductor;
	}

	public void setConductor(Track t){
		this.conductor = new MidiTrack(t);
	}

	public void deleteConductor(){
		conductor = null;
	}

	public boolean hasConductor(){
		return conductor != null;
	}

	public ArrayList<MidiTrack> getMidiTracks(){
		return tracks;
	}

	public void setMidiTracks(ArrayList<MidiTrack> tracks){
		if(tracks == null){
			deleteMidiTracks();
			return;
		}
		this.tracks = tracks;
	}

	public void deleteMidiTracks(){
		setMidiTracks(new ArrayList<MidiTrack>());
	}

	public boolean hasMidiTracks(){
		return !tracks.isEmpty();
	}

	public MidiFile clone(){
		return (MidiFile)super.clone();
	}

	public static MidiFile load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiFile)BaseObject.load(file);
	}

	public static MidiFile load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiFile)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiFile unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiFile)BaseObject.unzip(file);
	}

	public static MidiFile unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiFile)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
