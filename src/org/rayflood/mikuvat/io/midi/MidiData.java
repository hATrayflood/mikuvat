package org.rayflood.mikuvat.io.midi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;

import org.rayflood.mikuvat.io.BaseObject;

public abstract class MidiData extends BaseObject{
	private static final long serialVersionUID = 2907770619042609302L;

	public int compareTo(BaseObject obj){
		MidiData midiData = (MidiData)obj;
		return compare(getClass().getName(), midiData.getClass().getName());
	}

	public static MidiData newMidiData(MidiMessage mm){
		MidiData midiData = null;
		if(mm instanceof ShortMessage){
			midiData = new MidiShort(mm);
		}
		if(mm instanceof SysexMessage){
			midiData = new MidiSysex(mm);
		}
		if(mm instanceof MetaMessage){
			midiData = new MidiMeta(mm);
		}
		return midiData;
	}

	public abstract MidiMessage getMidiEvent() throws InvalidMidiDataException;

	protected abstract void setMidiEvent(MidiMessage mm);

	public MidiData clone(){
		return (MidiData)super.clone();
	}

	public static MidiData load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiData)BaseObject.load(file);
	}

	public static MidiData load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiData)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException {
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiData unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiData)BaseObject.unzip(file);
	}

	public static MidiData unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiData)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
