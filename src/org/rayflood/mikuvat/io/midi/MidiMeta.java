package org.rayflood.mikuvat.io.midi;

import static org.rayflood.mikuvat.Utilities.getByteArrayString;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiMessage;

import org.rayflood.mikuvat.io.BaseObject;

public class MidiMeta extends MidiData{
	private static final long serialVersionUID = 315321518296650973L;
	private int type;
	private byte[] data;

	protected MidiMeta(MidiMessage mm){
		setMidiEvent(mm);
	}

	public MidiMeta(MetaMessage meta){
		setMidiEvent(meta);
	}

	public MidiMeta(int type, byte[] data){
		setType(type);
		setData(data);
	}

	public MetaMessage getMidiEvent() throws InvalidMidiDataException{
		MetaMessage meta = new MetaMessage();
		meta.setMessage(getType(), getData(), getData().length);
		return meta;
	}

	protected void setMidiEvent(MidiMessage mm){
		setMidiEvent((MetaMessage)mm);
	}

	public void setMidiEvent(MetaMessage meta){
		setType(meta.getType());
		setData(meta.getData());
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		sl.add("Type=" + getType());
		sl.add("Data=" + getByteArrayString(getData()));

		StringBuilder sb = new StringBuilder();
		sb.append("MidiMeta={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		MidiMeta midiMeta = (MidiMeta)obj;
		return compare(getType(), midiMeta.getType());
	}

	public MidiMeta over(BaseObject obj, boolean fill, boolean has){
		MidiMeta midiMeta = ((MidiMeta)obj).clone();
		setType(midiMeta.getType());
		setData(midiMeta.getData());
		return this;
	}

	public MidiMeta setDefault(){
		return this;
	}

	public static MidiMeta getDefault(){
		MidiMeta midiMeta = new MidiMeta();
		midiMeta.setDefault();
		return midiMeta;
	}

	public MidiMeta delete(){
		setType(0);
		setData(new byte[]{});
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public MidiMeta(){
	}

	public int getType(){
		return type;
	}

	public void setType(int type){
		this.type = type;
	}

	public byte[] getData(){
		return data;
	}

	public void setData(byte[] data){
		this.data = data;
	}

	public MidiMeta clone(){
		return (MidiMeta)super.clone();
	}

	public static MidiMeta load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiMeta)BaseObject.load(file);
	}

	public static MidiMeta load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiMeta)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiMeta unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiMeta)BaseObject.unzip(file);
	}

	public static MidiMeta unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiMeta)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
