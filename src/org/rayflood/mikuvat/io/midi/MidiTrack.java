package org.rayflood.mikuvat.io.midi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Track;

import org.rayflood.mikuvat.io.BaseObject;

public class MidiTrack extends BaseObject{
	private static final long serialVersionUID = -1866741348212885469L;
	private TreeMap<Long, ArrayList<MidiData>> trackdata;

	public MidiTrack(Track t){
		this();
		setTrack(t);
	}

	public List<MidiEvent> getMidiEvent(long tick) throws InvalidMidiDataException {
		List<MidiData> list = getMidiData(tick);
		List<MidiEvent> mes = new ArrayList<MidiEvent>();
		for(int i = 0; i < list.size(); i++){
			mes.add(new MidiEvent(list.get(i).getMidiEvent(), tick));
		}
		return mes;
	}

	public List<MidiMessage> getMidiMessage(long tick) throws InvalidMidiDataException{
		List<MidiData> list = getMidiData(tick);
		List<MidiMessage> mms = new ArrayList<MidiMessage>();
		for(int i = 0; i < list.size(); i++){
			mms.add(list.get(i).getMidiEvent());
		}
		return mms;
	}

	public List<MidiData> getMidiData(long tick){
		return hasMidiData(tick) ? trackdata.get(tick) : new ArrayList<MidiData>();
	}

	public void addMidiEvent(MidiEvent event){
		addMidiData(event.getTick(), MidiData.newMidiData(event.getMessage()));
	}

	public void addMidiMessage(long tick, MidiMessage message){
		addMidiData(tick, MidiData.newMidiData(message));
	}

	public void addMidiData(long tick, MidiData midiData){
		ArrayList<MidiData> list = new ArrayList<MidiData>(getMidiData(tick));
		list.add(midiData);
		trackdata.put(tick, list);
	}

	public void setMidiMessage(long tick, List<MidiMessage> messages){
		List<MidiData> list = new ArrayList<MidiData>();
		for(int i = 0; i < messages.size(); i++){
			list.add(MidiData.newMidiData(messages.get(i)));
		}
		setMidiData(tick, list);
	}

	public void setMidiData(long tick, List<MidiData> list){
		trackdata.put(tick, new ArrayList<MidiData>(list));
	}

	public void addMidiMessage(long tick, List<MidiMessage> messages){
		for(int i = 0; i < messages.size(); i++){
			addMidiData(tick, MidiData.newMidiData(messages.get(i)));
		}
	}

	public void addMidiData(long tick, List<MidiData> list){
		for(int i = 0; i < list.size(); i++){
			addMidiData(tick, list.get(i));
		}
	}

	public Long[] getTicks(){
		return trackdata.keySet().toArray(new Long[0]);
	}

	public MidiData[] deleteMidiData(long tick){
		return trackdata.remove(tick).toArray(new MidiData[0]);
	}

	public boolean hasMidiData(long tick){
		return trackdata.containsKey(tick);
	}

	public void getTrack(Track t) throws InvalidMidiDataException{
		Long ticks[] = getTicks();
		for(int i = 0; i < ticks.length; i++){
			List<MidiEvent> mes = getMidiEvent(ticks[i]);
			for(int j = 0; j < mes.size(); j++){
				t.add(mes.get(j));
			}
		}
	}

	public void setTrack(Track t){
		for(int i = 0; i < t.size(); i++){
			addMidiEvent(t.get(i));
		}
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		Long ticks[] = getTicks();
		if(ticks.length > 0){
			for(int i = 0; i < ticks.length; i++){
				List<MidiData> mes = getMidiData(ticks[i]);
				if(mes.size() > 0){
					StringBuilder sb = new StringBuilder();
					sb.append(mes.get(0).toString());
					for(int j = 1; j < mes.size(); j++){
						sb.append("," + mes.get(j).toString());
					}
					sl.add(ticks[i] + "=[" + sb.toString() + "]");
				}
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append("MidiTrack={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		if(!(obj instanceof MidiTrack)){
			throw new ClassCastException();
		}
		return 0;
	}

	public MidiTrack over(BaseObject obj, boolean fill, boolean has){
		MidiTrack midiTrack = ((MidiTrack)obj).clone();
		Long ticks[] = midiTrack.getTicks();
		for(int i = 0; i < ticks.length; i++){
			long tick = ticks[i];
			if(fill || has == hasMidiData(tick)){
				List<MidiData> list = midiTrack.getMidiData(tick);
				for(int j = 0; j < list.size(); j++){
					addMidiData(tick, list.get(j));
				}
			}
		}
		return this;
	}

	public MidiTrack setDefault(){
		delete();
		return this;
	}

	public static MidiTrack getDefault(){
		MidiTrack midiTrack = new MidiTrack();
		midiTrack.setDefault();
		return midiTrack;
	}

	public MidiTrack delete(){
		trackdata = new TreeMap<Long, ArrayList<MidiData>>();
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public MidiTrack(){
		setDefault();
	}

	public MidiTrack clone(){
		return (MidiTrack)super.clone();
	}

	public static MidiTrack load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiTrack)BaseObject.load(file);
	}

	public static MidiTrack load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiTrack)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiTrack unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiTrack)BaseObject.unzip(file);
	}

	public static MidiTrack unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiTrack)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
