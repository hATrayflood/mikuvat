package org.rayflood.mikuvat.io.midi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;

import org.rayflood.mikuvat.io.BaseObject;

public class MidiShort extends MidiData{
	private static final long serialVersionUID = 7292836173063614828L;
	private int command;
	private int channel;
	private int data1;
	private int data2;

	protected MidiShort(MidiMessage mm){
		setMidiEvent(mm);
	}

	public MidiShort(ShortMessage shortm){
		setMidiEvent(shortm);
	}

	public MidiShort(int command, int channel, int data1, int data2){
		setCommand(command);
		setChannel(channel);
		setData1(data1);
		setData2(data2);
	}

	public ShortMessage getMidiEvent() throws InvalidMidiDataException{
		ShortMessage message = new ShortMessage();
		message.setMessage(getCommand(), getChannel(), getData1(), getData2());
		return message;
	}

	protected void setMidiEvent(MidiMessage mm){
		setMidiEvent((ShortMessage)mm);
	}

	public void setMidiEvent(ShortMessage shortm){
		setCommand(shortm.getCommand());
		setChannel(shortm.getChannel());
		setData1(shortm.getData1());
		setData2(shortm.getData2());
	}

	public String toString(){
		List<String> sl = new ArrayList<String>();
		sl.add("Command=" + getCommand());
		sl.add("Channel=" + getChannel());
		sl.add("Data1=" + getData1());
		sl.add("Data2=" + getData2());

		StringBuilder sb = new StringBuilder();
		sb.append("MidiShort={");
		if(!sl.isEmpty()){
			sb.append(sl.get(0));
			for(int i = 1; i < sl.size(); i++){
				sb.append("," + sl.get(i));
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean validate(){
		return true;
	}

	public int compareTo(BaseObject obj){
		MidiShort midiShort = (MidiShort)obj;
		int c = compare(this.getCommand(), midiShort.getCommand());
		if(c == 0){
			c = compare(this.getChannel(), midiShort.getChannel());
		}
		return c;
	}

	public MidiShort over(BaseObject obj, boolean fill, boolean has){
		MidiShort midiShort = ((MidiShort)obj).clone();
		setCommand(midiShort.getCommand());
		setChannel(midiShort.getChannel());
		setData1(midiShort.getData1());
		setData2(midiShort.getData2());
		return this;
	}

	public MidiShort setDefault(){
		return this;
	}

	public static MidiShort getDefault(){
		MidiShort midiShort = new MidiShort();
		midiShort.setDefault();
		return midiShort;
	}

	public MidiShort delete(){
		setCommand(0);
		setChannel(0);
		setData1(0);
		setData2(0);
		return this;
	}

	public boolean isDeleted(){
		return false;
	}

	public MidiShort(){
	}

	public int getCommand(){
		return command;
	}

	public void setCommand(int command){
		this.command = command;
	}

	public int getChannel(){
		return channel;
	}

	public void setChannel(int channel){
		this.channel = channel;
	}

	public int getData1(){
		return data1;
	}

	public void setData1(int data1){
		this.data1 = data1;
	}

	public int getData2(){
		return data2;
	}

	public void setData2(int data2){
		this.data2 = data2;
	}

	public MidiShort clone(){
		return (MidiShort)super.clone();
	}

	public static MidiShort load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiShort)BaseObject.load(file);
	}

	public static MidiShort load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return (MidiShort)BaseObject.load(file);
	}

	public void store(String file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public void store(File file) throws FileNotFoundException, IOException{
		super.store(file);
	}

	public static MidiShort unzip(String file) throws IOException, ClassNotFoundException{
		return (MidiShort)BaseObject.unzip(file);
	}

	public static MidiShort unzip(File file) throws IOException, ClassNotFoundException{
		return (MidiShort)BaseObject.unzip(file);
	}

	public void zip(String file) throws IOException{
		super.zip(file);
	}

	public void zip(File file) throws IOException{
		super.zip(file);
	}
}
