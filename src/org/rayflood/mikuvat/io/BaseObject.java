package org.rayflood.mikuvat.io;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * BaseObjectはデータを格納するオブジェクトのベースとなるクラスです。<br>
 * 標準でSerializable, Cloneableを実装しており、派生クラスで定義しなくても使用可能です。<br>
 * シリアライズによるインスタンスのファイル化もサポートしています。<br>
 * <br>
 * Serializableを実装するため、オブジェクト内のフィールドは全て直列化可能なオブジェクトを使ってください。<br>
 * Comparableは派生クラスでオーバーライドしてください。<br>
 * @see java.io.Serializable
 * @see java.lang.Cloneable
 * @see java.lang.Comparable
 */
public abstract class BaseObject implements Serializable, Cloneable, Comparable<BaseObject>{

	private static final long serialVersionUID = 5225438162616635387L;

	/**
	 * このクラスのデフォルト値をセットします。<br>
	 * 通常は、デフォルト値がセットされたオブジェクトを返すgetDefault()メソッドも用意します。<br>
	 * <pre>
	 * public SomeObject extends BaseObject{
	 *     public static SomeObject getDefault(){
	 *         SomeObject someObject = new SomeObject();
	 *         someObject.setDefault();
	 *         return someObject;
	 *     }
	 * }
	 * </pre>
	 * 派生クラスでそれぞれの内容にあったsetDefault()メソッドをオーバーライドしてください。<br>
	 * @return このクラスのデフォルト値がセットされたオブジェクト
	 */
	public abstract BaseObject setDefault();

	/**
	 * このオブジェクトの内容を完全に削除します。<br>
	 * <br>
	 * 派生クラスでそれぞれの内容にあったdelete()メソッドをオーバーライドしてください。<br>
	 * また、このメソッド実行後はisDeleted()メソッドがtrueになるようにしてください。<br>
	 * @see BaseObject#isDeleted()
	 * @return 内容が完全に削除されたこのオブジェクト
	 */
	public abstract BaseObject delete();

	/**
	 * オブジェクトの内容が論理的に正しいか調べます。<br>
	 * 通常は、オブジェクトをファイルに出力しても問題ないかのチェックに使います。<br>
	 * <br>
	 * 派生クラスでそれぞれの内容にあったvalidate()メソッドをオーバーライドしてください。<br>
	 * @return 内容が論理的に正しい場合はtrue、正しくない場合はfalse
	 */
	public abstract boolean validate() throws InvalidBaseObjectException;

	/**
	 * オブジェクトの内容が完全に削除されているか調べます。<br>
	 * 通常は、オブジェクトをファイルに出力する際に、省いても良いかのチェックに使います。<br>
	 * <br>
	 * 派生クラスでそれぞれの内容にあったisDeleted()メソッドをオーバーライドしてください。<br>
	 * @see BaseObject#delete()
	 * @return 削除されている場合はtrue、されていない場合はfalse
	 */
	public abstract boolean isDeleted();

	/**
	 * 指定されたオブジェクトでこのオブジェクトを上書きします。<br>
	 * ただし、指定されたオブジェクトのうち、値が何も設定されていないフィールドは上書きしません。<br>
	 * <br>
	 * このメソッドは通常、over()メソッドで使われるので、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @param obj 上書きするオブジェクト
	 * @return 上書きされたこのオブジェクト
	 * @throws ClassCastException 指定されたオブジェクトの型が、このオブジェクトと一致しない場合
	 */
	public BaseObject cover(BaseObject obj){
		return over(obj, true, false);
	}

	/**
	 * 指定されたオブジェクトでこのオブジェクトを上書きします。<br>
	 * 上書きする対象は、フィールドに<strong>何らかの値が設定されているもの</strong>のみです。値が設定されていないフィールドには上書きしません。<br>
	 * また、指定されたオブジェクトのうち、値が何も設定されていないフィールドは上書きしません。<br>
	 * <br>
	 * このメソッドは通常、over()メソッドで使われるので、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @param obj 上書きするオブジェクト
	 * @return 上書きされたこのオブジェクト
	 * @throws ClassCastException 指定されたオブジェクトの型が、このオブジェクトと一致しない場合
	 */
	public BaseObject heap(BaseObject obj){
		return over(obj, false, true);
	}

	/**
	 * 指定されたオブジェクトでこのオブジェクトを上書きします。<br>
	 * 上書きする対象は、フィールドに<strong>何も値が設定されていないもの</strong>のみです。値が設定されているフィールドには上書きしません。<br>
	 * また、指定されたオブジェクトのうち、値が何も設定されていないフィールドは上書きしません。<br>
	 * <br>
	 * このメソッドは通常、over()メソッドで使われるので、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @param obj 上書きするオブジェクト
	 * @return 上書きされたこのオブジェクト
	 * @throws ClassCastException 指定されたオブジェクトの型が、このオブジェクトと一致しない場合
	 */
	public BaseObject lay(BaseObject obj){
		return over(obj, false, false);
	}

	/**
	 * 指定されたオブジェクトでこのオブジェクトを上書きします。<br>
	 * ただし、指定されたオブジェクトのフィールドがnullの場合は上書きしません。<br>
	 * 派生クラスでそれぞれの内容にあったover()メソッドをオーバーライドしてください。<br>
	 * <pre>
	 * public SomeObject extends BaseObject{
	 *     String aString;
	 *     public SomeObject over(Object obj, boolean fill, boolean has){
	 *         SomeObject someObject = ((SomeObject)obj).clone();
	 *         if(someObject.hasAString() && (fill || has == hasAString())){
	 *             setAString(someObject.getAString());
	 *         }
	 *         return this;
	 *     }
	 * }
	 * </pre>
	 * @see BaseObject#cover(BaseObject)
	 * @see BaseObject#heap(BaseObject)
	 * @see BaseObject#lay(BaseObject)
	 * @param obj 上書きするオブジェクト
	 * @param fill trueは、このオブジェクトのフィールドの状態に関係なく上書きする。
	 * @param has trueは、このオブジェクトのフィールドがnullではない場合に上書きする。falseはnullの場合に上書きする。
	 * @return 上書きされたこのオブジェクト
	 * @throws ClassCastException 指定されたオブジェクトの型が、このオブジェクトと一致しない場合
	 */
	public abstract BaseObject over(BaseObject obj, boolean fill, boolean has);

	/**
	 * オブジェクトの文字列表現を返します。<br>
	 * 通常、toString()メソッドはこのオブジェクトを「テキストで表現する」文字列を返します。<br>
	 * <br>
	 * 派生クラスでそれぞれの内容にあったtoString()メソッドをオーバーライドしてください。<br>
	 * @see java.lang.Object#toString()
	 */
	@Override
	public abstract String toString();

	/**
	 * このオブジェクトと「等価」になるオブジェクトがあるかどうかを示します。<br>
	 * <br>
	 * このメソッドはお互いのオブジェクトをシリアライズしてバイト配列を取得し、そのバイト配列同士を比較します。<br>
	 * そのため、オブジェクト同士の、型を含めた完全一致チェックを行います。<br>
	 * @param obj 比較対象の参照オブジェクト
	 * @return 引数に指定されたオブジェクトとこのオブジェクトが等しい場合は true、そうでない場合は false
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @see java.io.Serializable
	 */
	@Override
	public boolean equals(Object obj){
		if(obj == null || !getClass().getName().equals(obj.getClass().getName())){
			return false;
		}
		try{
			return Arrays.equals(encodeBytes(), ((BaseObject)obj).encodeBytes());
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * このオブジェクトのコピーを作成して返します。<br>
	 * <br>
	 * 直列化してバイト配列を取得し、そのバイト配列から新たなインスタンスを作成するため、クラスに関係なく、常に「ディープコピー」が作成されます。<br>
	 * このメソッドの戻り値はObjectクラスになっており、アクセス権限もprotectedになっているため、使う場合は派生クラスでオーバーライドしてください。<br>
	 * <pre>
	 * public SomeObject extends BaseObject{
	 *     public SomeObject clone(){
	 *         return (SomeObject)super.clone();
	 *     }
	 * }
	 * </pre>
	 * @see BaseObject#encodeBytes()
	 * @see BaseObject#decodeBytes(byte[])
	 * @return このインスタンスの複製
	 * @see java.lang.Object#clone()
	 * @see java.io.Serializable
	 */
	@Override
	protected BaseObject clone(){
		try{
			return decodeBytes(encodeBytes());
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * このオブジェクトと指定されたオブジェクトの順序を比較します。<br>
	 * このオブジェクトが指定されたオブジェクトより小さい場合は負の整数、等しい場合はゼロ、大きい場合は正の整数を返します。<br>
	 * <br>
	 * 派生クラスでそれぞれの内容にあったcompareTo()メソッドをオーバーライドしてください。<br>
	 * @see BaseObject#compare(Comparable, Comparable)
	 * @param obj 比較対象の Object
	 * @return このオブジェクトが指定されたオブジェクトより小さい場合は負の整数、等しい場合はゼロ、大きい場合は正の整数
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public abstract int compareTo(BaseObject obj);

	/**
	 * 2つのオブジェクトの順序を比較します。<br>
	 * 一方のオブジェクトが他方のオブジェクトより小さい場合は負の整数、等しい場合はゼロ、大きい場合は正の整数を返します。<br>
	 * <br>
	 * オブジェクトがnullだった場合は、nullの方が小さいと判定します。両方nullの場合は等しいと判定します。<br>
	 * 引数のオブジェクトはパラメータ化されていないため、オブジェクトの型が合わない場合はClassCastExceptionが発生します。<br>
	 * このメソッドは通常、compareTo()メソッドで使われるので、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @see BaseObject#compareTo(BaseObject)
	 * @param o1 一方のオブジェクト
	 * @param o2 他方のオブジェクト
	 * @return o1がo2より小さい場合は負の整数、等しい場合はゼロ、大きい場合は正の整数
	 * @throws ClassCastException 指定されたオブジェクトの型が原因で、この Object と比較できない場合
	 */
	@SuppressWarnings("unchecked")
	protected static int compare(Comparable o1, Comparable o2){
		if(o1 == null && o2 == null){
			return 0;
		}
		if(o1 == null){
			return -1;
		}
		if(o2 == null){
			return 1;
		}
		return o1.compareTo(o2);
	}

	/**
	 * オブジェクトのコレクションで「全体順序付け」を行う比較関数です。<br>
	 * コンパレータ (Comparator) をソートメソッド (Collections.sort など) に渡すと、ソート順を正確に制御できます。<br>
	 * <br>
	 * このコンパレータはオブジェクトを「昇順」にソートします。比較はcompareTo()メソッドで行います。<br>
	 * compare()メソッドと同じく、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @see BaseObject#getComparatorR()
	 * @return 昇順コンパレータ
	 * @see java.util.Comparator
	 */
	public static Comparator<BaseObject> getComparator(){
		return new Comparator<BaseObject>(){
			public int compare(BaseObject o1, BaseObject o2){
				return o1.compareTo(o2);
			}
		};
	}

	/**
	 * オブジェクトのコレクションで「全体順序付け」を行う比較関数です。<br>
	 * コンパレータ (Comparator) をソートメソッド (Collections.sort など) に渡すと、ソート順を正確に制御できます。<br>
	 * <br>
	 * このコンパレータはオブジェクトを「降順」にソートします。比較はcompareTo()メソッドの戻り値 * -1 です。<br>
	 * compare()メソッドと同じく、特別な理由がない限り派生クラスでオーバーライドする必要はありません。<br>
	 * @see BaseObject#getComparator()
	 * @return 降順コンパレータ
	 * @see java.util.Comparator
	 */
	public static Comparator<BaseObject> getComparatorR(){
		return new Comparator<BaseObject>(){
			public int compare(BaseObject o1, BaseObject o2){
				return o1.compareTo(o2) * -1;
			}
		};
	}

	/**
	 * このオブジェクトを直列化し、指定された出力ストリームに書き込みます。<br>
	 * 出力ストリームは自動的に閉じられるため、再利用できません。<br>
	 * @see BaseObject#unserialize(InputStream)
	 * @param out 出力ストリーム
	 * @throws IOException
	 */
	private void serialize(OutputStream out) throws IOException{
		ObjectOutputStream oos = null;
		try{
			oos = new ObjectOutputStream(out);
			oos.writeObject(this);
		}
		finally{
			if(oos != null){
				oos.close();
			}
			else{
				out.close();
			}
		}
	}

	/**
	 * 指定された入力ストリームからオブジェクトを読み込みます。<br>
	 * 入力ストリームは自動的に閉じられるため、再利用できません。<br>
	 * @see BaseObject#serialize(OutputStream)
	 * @param in 入力ストリーム
	 * @return 読み込まれたオブジェクト
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static BaseObject unserialize(InputStream in) throws IOException, ClassNotFoundException{
		ObjectInputStream ois = null;
		try{
			ois = new ObjectInputStream(in);
			return (BaseObject)ois.readObject();
		}
		finally{
			if(ois != null){
				ois.close();
			}
			else{
				in.close();
			}
		}
	}

	/**
	 * このオブジェクトを直列化し、バイト配列に変換します。<br>
	 * @see BaseObject#decodeBytes(byte[])
	 * @return オブジェクトから直列化されたバイト配列
	 * @throws IOException
	 */
	protected byte[] encodeBytes() throws IOException{
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		serialize(byteOut);
		return byteOut.toByteArray();
	}

	/**
	 * オブジェクトを直列化したバイト配列を、元のオブジェクトに変換します。<br>
	 * @see BaseObject#encodeBytes()
	 * @param buf オブジェクトから変換されたバイト配列
	 * @return 読み込まれたオブジェクト
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static BaseObject decodeBytes(byte[] buf) throws IOException, ClassNotFoundException{
		return unserialize(new ByteArrayInputStream(buf));
	}

	/**
	 * このオブジェクトを直列化し、指定された出力ファイルに書き込みます。<br>
	 * アクセス権限がprotectedになっているため、使う場合は派生クラスでオーバーライドしてください。<br>
	 * store(String)メソッド、zip(File)メソッド、zip(String)メソッドについても同様です。<br>
	 * <pre>
	 * public SomeObject extends BaseObject{
	 *     public void store(File file) throws FileNotFoundException, IOException{
	 *         super.store(File file);
	 *     }
	 * }
	 * </pre>
	 * @see BaseObject#load(File)
	 * @param file 出力ファイル
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected void store(File file) throws FileNotFoundException, IOException{
		serialize(new BufferedOutputStream(new FileOutputStream(file)));
	}

	/**
	 * ファイル名をStringとして受ける以外はstore(File)と同じです。<br>
	 * @see BaseObject#store(File)
	 * @param file 出力ファイル
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected void store(String file) throws FileNotFoundException, IOException{
		store(new File(file));
	}

	/**
	 * 入力ファイルからオブジェクトを読み込みます。<br>
	 * このメソッドの戻り値はObjectクラスになっており、アクセス権限もprotectedになっているため、使う場合は派生クラスでオーバーライドしてください。<br>
	 * load(String)メソッド、unzip(File)メソッド、unzip(String)メソッドについても同様です。<br>
	 * <pre>
	 * public SomeObject extends BaseObject{
	 *     public static SomeObject load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
	 *         return (SomeObject)BaseObject.load(File file);
	 *     }
	 * }
	 * </pre>
	 * @see BaseObject#store(File)
	 * @param file 入力ファイル
	 * @return 読み込まれたオブジェクト
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static BaseObject load(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return unserialize(new BufferedInputStream(new FileInputStream(file)));
	}

	/**
	 * ファイル名をStringとして受ける以外はload(File)と同じです。<br>
	 * @see BaseObject#load(File)
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static BaseObject load(String file) throws FileNotFoundException, IOException, ClassNotFoundException{
		return load(new File(file));
	}

	/**
	 * このオブジェクトを直列化し、指定されたzipファイルに書き込みます。<br>
	 * zipエントリは1つだけで、クラス名がそのままエントリ名になります。<br>
	 * 派生クラスでの実装方法はstore(File)を参照してください。<br>
	 * @see BaseObject#store(File)
	 * @see BaseObject#unzip(File)
	 * @param zip zipファイル
	 * @throws IOException
	 */
	protected void zip(File zip) throws IOException{
		ZipOutputStream zos = null;
		try{
			zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zip)));
			zos.putNextEntry(new ZipEntry(getClass().getName()));
			serialize(zos);
		}
		finally{
			if(zos != null){
				zos.close();
			}
		}
	}

	/**
	 * ファイル名をStringとして受ける以外はzip(File)と同じです。<br>
	 * @see BaseObject#zip(File)
	 * @param zip zipファイル
	 * @throws IOException
	 */
	protected void zip(String zip) throws IOException{
		zip(new File(zip));
	}

	/**
	 * zipファイルからオブジェクトを読み込みます。<br>
	 * 最初のエントリのみ読み込み、オブジェクトを返した後、zipファイルを閉じます。<br>
	 * エントリ名とクラス名が一致しているかはチェックしません。<br>
	 * 派生クラスでの実装方法はload(File)を参照してください。<br>
	 * @see BaseObject#load(File)
	 * @see BaseObject#zip(File)
	 * @param zip zipファイル
	 * @return 読み込まれたオブジェクト
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static BaseObject unzip(File zip) throws IOException, ClassNotFoundException{
		ZipInputStream zis = null;
		try{
			zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zip)));
			zis.getNextEntry();
			return unserialize(zis);
		}
		finally{
			if(zis != null){
				zis.close();
			}
		}
	}

	/**
	 * ファイル名をStringとして受ける以外はunzip(File)と同じです。<br>
	 * @see BaseObject#unzip(File)
	 * @param zip zipファイル
	 * @return 読み込まれたオブジェクト
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static BaseObject unzip(String zip) throws IOException, ClassNotFoundException{
		return unzip(new File(zip));
	}

	/**
	 * このオブジェクトのJavaBeanテキスト表現を生成し、指定された出力ストリームに書き込みます。<br>
	 * 出力ストリームは自動的に閉じられるため、再利用できません。<br>
	 * @see BaseObject#decodeXML(InputStream)
	 * @param out 出力ストリーム
	 * @throws IOException
	 * @see java.beans.XMLEncoder
	 */
	private void encodeXML(OutputStream out) throws IOException{
		XMLEncoder e = null;
		try{
			e = new XMLEncoder(out);
			e.writeObject(this);
		}
		finally{
			if(e != null){
				e.close();
			}
			else{
				out.close();
			}
		}
	}

	/**
	 * 指定された入力ストリームからJavaBeanテキスト表現を取得し、オブジェクトを読み込みます。<br>
	 * 入力ストリームは自動的に閉じられるため、再利用できません。<br>
	 * @see BaseObject#encodeXML(OutputStream)
	 * @param in 入力ストリーム
	 * @return 読み込まれたオブジェクト
	 * @throws IOException
	 * @see java.beans.XMLDecoder
	 */
	private static BaseObject decodeXML(InputStream in) throws IOException{
		XMLDecoder d = null;
		try{
			d = new XMLDecoder(in);
			BaseObject result = (BaseObject)d.readObject();
			return result;
		}
		finally{
			if(d != null){
				d.close();
			}
			else{
				in.close();
			}
		}
	}

	/**
	 * XMLEncoderを使って、オブジェクトをxmlファイルに書き込みます。<br>
	 * <br>
	 * ※注：このメソッドはオブジェクトがJavaBeanの仕様に準じる必要があるため、必ずしもstore(File)メソッド、zip(File)メソッドの代替にはなり得ません。<br>
	 * 例えばBigDecimalはXMLEncoderで保存することが出来ません。<br>
	 * 詳細については、『The Swing Connection』の記事「<a target="_blank" href="http://java.sun.com/products/jfc/tsc/articles/persistence4">Using XMLEncoder</a>」を参照してください。<br>
	 * @see BaseObject#decodeXML(File)
	 * @param xml xmlファイル
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @see java.beans.XMLEncoder
	 */
	protected void encodeXML(File xml) throws FileNotFoundException, IOException{
		encodeXML(new BufferedOutputStream(new FileOutputStream(xml)));
	}

	/**
	 * ファイル名をStringとして受ける以外はencodeXML(File)と同じです。<br>
	 * @see BaseObject#encodeXML(File)
	 * @param xml xmlファイル
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected void encodeXML(String xml) throws FileNotFoundException, IOException{
		encodeXML(new File(xml));
	}

	/**
	 * XMLDecoderを使って、xmlファイルからオブジェクトを読み込みます。<br>
	 * <br>
	 * ※注：このメソッドはオブジェクトがJavaBeanの仕様に準じる必要があるため、必ずしもload(File)メソッド、unzip(File)メソッドの代替にはなり得ません。<br>
	 * 例えばBigDecimalはXMLEncoderで保存することが出来ません。<br>
	 * 詳細については、『The Swing Connection』の記事「<a target="_blank" href="http://java.sun.com/products/jfc/tsc/articles/persistence4">Using XMLEncoder</a>」を参照してください。<br>
	 * @see BaseObject#encodeXML(File)
	 * @param xml xmlファイル
	 * @return 読み込まれたオブジェクト
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @see java.beans.XMLDecoder
	 */
	protected static BaseObject decodeXML(File xml) throws FileNotFoundException, IOException{
		return decodeXML(new BufferedInputStream(new FileInputStream(xml)));
	}

	/**
	 * ファイル名をStringとして受ける以外はdecodeXML(File)と同じです。<br>
	 * @see BaseObject#decodeXML(File)
	 * @param xml xmlファイル
	 * @return 読み込まれたオブジェクト
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected static BaseObject decodeXML(String xml) throws FileNotFoundException, IOException{
		return decodeXML(new File(xml));
	}
}
