package org.rayflood.mikuvat.io;

public class InvalidBaseObjectException extends Exception{
	private static final long serialVersionUID = 5038596021277993480L;
	private BaseObject obj;

	public InvalidBaseObjectException(BaseObject obj){
		super();
		this.obj = obj;
	}

	public BaseObject getObj(){
		return obj;
	}

	public void setObj(BaseObject obj){
		this.obj = obj;
	}
}
